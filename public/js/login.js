function validateLoginForm()
{
	var messages = [];
	var msgString;

    var loginForm = document.getElementById("login-form");

    if(loginForm.username.value == ""){
    	messages.push("Please enter username");
    }

    if(loginForm.password.value == ""){
    	messages.push("Please enter password");
    }

    if(messages.length == 0)
    	return true;
    else{
    	msgString = "<section class=\"messages\" id=\"login-messages\">";
    	for(var i = 0;i < messages.length;i++){
    		msgString += "<p class='error'>" + messages[i] + "</p>";
    	}
        msgString += "</section>";
        document.getElementById("messages-container").innerHTML = msgString;
        return false;
    }
}

function clearErrorMessages()
{
	document.getElementById("messages-container").innerHTML = "";
}