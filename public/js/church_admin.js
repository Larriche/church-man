function validateChurchAdminRegis()
{
	var messages = [];
	var msgString;

    var churchAdminRegisForm = document.getElementById("church-admin-regis-form");

    if(churchAdminRegisForm.username.value == ""){
    	messages.push("Please enter username");
    }

    if(churchAdminRegisForm.password.value == ""){
    	messages.push("Please enter password");
    }

    if(messages.length == 0)
    	return true;
    else{
    	msgString = "<section class=\"messages\" id=\"church-admin-regis-messages\">";
    	for(var i = 0;i < messages.length;i++){
    		msgString += "<p class='error'>" + messages[i] + "</p>";
    	}
        msgString += "</section>";
        document.getElementById("messages-container").innerHTML = msgString;
        return false;
    }
}

function clearErrorMessages()
{
	document.getElementById("messages-container").innerHTML = "";
}