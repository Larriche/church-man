function initializeDialogComponents()
{
	var buttons = document.getElementsByTagName('a');

	for(var i = 0;i < buttons.length;i++){
		if(buttons[i].getAttribute('class') == "danger"){
			buttons[i].addEventListener('click',confirmDel)
		}
	}
}

function confirmDel(evnt)
{
	var dialog = document.getElementById('dialog-box');

	var yesBttn = document.getElementById('yes');
	var noBttn = document.getElementById('no');

	yesBttn.addEventListener('click',handleResponse);
	yesBttn.proceedTo = this.getAttribute('href');

	noBttn.addEventListener('click',handleResponse)

	dialog.style.display = "inline";
	evnt.preventDefault();
}

function handleResponse()
{
	if(this.getAttribute('id') == 'yes'){
		window.location.href = this.proceedTo;
	}

	document.getElementById('dialog-box').style.display = "none";
}

function closeMessageBox()
{
	var box = document.getElementById("messages-container");

	box.style.display = "none";
}