function ajaxRequest()
{
    try{ var request = new XMLHttpRequest(); }
    catch(e1){
        try{ request = new ActiveXObject("Msxml2.XMLHTTP");}
        catch(e2){
            try{ request = new ActiveXObject("Microsoft.XMLHTTP");}
            catch(e3){
                request = false;
            }
        }
    }
    return request;
}


function searchMember()
{
	var name = document.getElementById("new-member-name").value;

	var request =  ajaxRequest();
    var params = "name="+ name;
    request.open("POST","/public/ajax/hello.php",true);
    request.setRequestHeader("Content-type","application/x-www-form-urlencoded")
    

    request.onreadystatechange = function()
    {
        if(this.readyState == 4 && this.status == 200)
        {
            if(this.responseText != null)
            {
                document.getElementById("search-result").innerHTML = this.responseText;
            }
            else alert("Ajax error: no data received");
        }
    }

    request.send(params);


}

