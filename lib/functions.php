<?php
/**
 * Redirect to the given url
 * 
 * @return void
 */
function redirect($url)
{
	$url = "location:".$url;
	header($url);
}

/**
 * Determine whether the system admin is logged in
 * 
 * @return boolean 
 */
function sysAdminLoggedIn()
{
	if(isset($_SESSION['user_id']) && isset($_SESSION['user_role']) && $_SESSION['user_role'] == 'SYS_ADMIN')
		return true;
	return false;
}

/**
 * Determine whether a church admin is logged in
 * 
 * @return boolean
 */
function churchAdminLoggedIn()
{
    if(isset($_SESSION['user_id']) && isset($_SESSION['user_role']) && $_SESSION['user_role'] == 'CHURCH_ADMIN')
		return true;
	return false;	
}

/**
 * Determine whether a pastor is logged in
 * 
 * @return boolean
 */
function pastorLoggedIn()
{
	if(isset($_SESSION['user_id']) && isset($_SESSION['user_role']) && $_SESSION['user_role'] == 'PASTOR')
		return true;
	return false;
}

/**
 * Determiner whether a church member is logged in
 * 
 * @return boolean
 */
function memberLoggedIn()
{
	if(isset($_SESSION['user_id']) && isset($_SESSION['user_role']) && $_SESSION['user_role'] == 'MEMBER')
		return true;
	return false;
}

/**
 * A generic check for whether a user is logged in
 * 
 * @return boolean
 */
function userLoggedIn()
{
    if(isset($_SESSION['user_id']) && isset($_SESSION['user_role']))
        return true;
    return false;
}

/**
 * Log  out the user that is currently logged in
 * 
 * @return void
 */
function logoutUser()
{
    unset($_SESSION['user_id']);
    unset($_SESSION['user_role']);

}

/**
 * Sanitize user input
 * 
 * @return void
 */
function sanitizeInput($text)
{
    $text = trim($text);
    $text = strip_tags($text);
    $text = htmlentities($text);	
    return $text;
}

/**
 * Upload a photo to the server
 * 
 * @return void
 */
function uploadPhoto($file,$mime,$id)
{
    $save_url = "resources/uploads/profile_photos/".$id.".jpg";
    move_uploaded_file($file, $save_url);

    processImage($save_url,$mime);
}

/**
 * Do a bit of image processing on an uploaded image
 * 
 * @return void
 */
function processImage($saveUrl,$mime)
{
    $typeok = TRUE;

    switch($mime){
        case "image/gif":   $src = imagecreatefromgif($saveUrl); break;
        case "image/jpeg":  // Both regular and progressive jpegs
        case "image/pjpeg": $src = imagecreatefromjpeg($saveUrl); break;
        case "image/png":   $src = imagecreatefrompng($saveUrl); break;
        default:            $typeok = FALSE; break;
    }

    if($typeok){
        list($w, $h, $type, $attr) = getimagesize($saveUrl);

        // the size i want saved images to be
        $tw = 200;
        $th = 200;

        $tmp = imagecreatetruecolor($tw, $th);
        imagecopyresampled($tmp, $src, 0, 0, 0, 0, $tw, $th, $w, $h);
        imageconvolution($tmp, array(array(-1, -1, -1),
            array(-1, 16, -1), array(-1, -1, -1)), 8, 0);
        imagejpeg($tmp, $saveUrl);
        imagedestroy($tmp);
        imagedestroy($src);
    }
}

/**
 * Capitalise the given string
 * 
 * @return void
 */
function capitalise($str)
{
    $restOfLetters = substr($str, 1);
    return strtoupper($str[0]).strtolower($restOfLetters); 
}

/**
 * Persist notification strings
 * 
 * @return void
 */
function logNotifications($notifs)
{
    foreach($notifs as $notification){
        $_SESSION[$notification] = true;
    }
}

/**
 * Check whether a certain notification string exists
 * 
 * @return boolean
 */
function notificationExists($notif)
{
    if(isset($_SESSION[$notif]))
        return true;
    return false;
}

/**
 * Remove a notification string from the session
 * 
 * @return void
 */
function removeNotification($notif)
{
    unset($_SESSION[$notif]);
}

/**
 * Displays links with the appropriate parameters to implement pagination
 * 
 * @param int $count total number of items being paginated
 * @param int $offset number of items to display per page
 * @param int $numOfLinks the number of pagination links to display
 * @param int $currentPage current page in the pagination
 * @param string $linkBase the start of the url for each link
 * @return void
 */
function displayPaginatorLinks($count,$offset,$numOfLinks,$currentPage,$linkBase)
{
    // the number to display in the links, we initialise it to 1
    // and increment it as long as we have more links to generate
    $i = 1;

    // count of data items handled so far
    $curr = 1;

    // links output so far
    $linksSoFar = 0;

    if( ceil($count / $offset) < $numOfLinks){
        // if the supplied maximum number of links is more than neccessary for the
        // current data size,we reduce it to the number of links that are enough
        // to paginate all the data
        $numOfLinks = ceil($count / $offset);
    }

    if($currentPage > $numOfLinks){
        // if the current page number is more than the maximum number of links,we 
        // initialise i to that value
        $i = $currentPage;
        $curr = $offset * $currentPage + 1;
    }

    while(($curr  <= $count) && ($linksSoFar < $numOfLinks)) {
        // as long as there are more items and the links so far
        // has not reached the maximum,we output links to other
        // pages

        $link =  '<a ';

        if($i == $currentPage){
            // add a class for styling purposes if this is the current page
            $link .= 'class = "current-link" ';
        }

        $link .= $linkBase . $curr . '">' . $i ."</a>";
        
        // output the link
        echo $link;

        // increase number of links output so far
        $linksSoFar++;

        $i++; 
        $curr += $offset;
    }
}
?>