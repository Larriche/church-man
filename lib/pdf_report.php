<?php
class PDFReport
{
	protected $template;

	protected $htmlOutput;

	protected $pdfName;

	protected $data;

    protected $resources;

	public function __construct($template,$htmlOutput,$pdfName,$data,$resources)
	{
		$this->template = $template;
		$this->htmlOutput = 'storage/'.$htmlOutput;

		$this->pdfName = $pdfName;
		$this->data = $data;
		$this->resources = $resources;

		$this->moveResources();
		$this->createHTML();
	}

    // move all temporary resources usually images that the reports will need
    // into the framework's temporary storage folder and save them
    // with their mapped destination names
	public function moveResources()
	{
		foreach ($this->resources as $source => $destination) {
			$dest = 'storage/'.$destination;
			copy($source , $dest);
		}
	}

	public function createHTML()
	{
		$html =  file_get_contents($this->template);

		foreach($this->data as $placeholder => $replacement){
			$html = str_replace($placeholder, $replacement, $html);
		}
        
		$fileObj = fopen($this->htmlOutput,'w');
		fwrite($fileObj,$html);
		fclose($fileObj);
	}

	public function generatePDF()
	{
		$dompdf = new DOMPDF();

		$path = $_SERVER['DOCUMENT_ROOT'].'churchman/';
        $dompdf->set_option('chroot',realpath($path));   
        $dompdf->set_option('enable_css_float',true); 

		$dompdf->load_html_file($this->htmlOutput);
		$dompdf->render();

        $dompdf->set_paper("A4");
		$dompdf->stream($this->pdfName);

		$this->deleteResources();
	}

	public function deleteResources()
	{
		// delete temporary files used for pdf generation
		unlink($this->htmlOutput);

		foreach ($this->resources as $source => $dest) {
			unlink('storage/'.$dest);
		}
	}
}
?>