<?php
class ChurchAdmin extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Church Administrators controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the actions pertaining to church administrators
    | such as their registration and showing their dashboard page
    | 
    |
    */

    /**
     * Create a new ChurchAdmin controller instance.
     *
     * @return void
     */

	public function __construct()
	{
		// models this controller will be needing
		$this->models = ['User'];
        $this->loadModels();
	}

    /**
     * Shows the registration page for church admins to the user
     * This can only be accessed by system admins. 
     * It also process form validation messages and shows appropriate messages 
     * to the user
     * 
     * @return void
     */
	public function register()
	{
		if(!sysAdminLoggedIn()){
            redirect('/churchman');
        }
			
        $messages = [];

        if(notificationExists('registration_success')){
            $messages[] = "<p class='success'>Church administrator added successfully</p>";
            removeNotification('registration_success');
        }

        if(notificationExists('username_empty')){
        	$messages[] = "<p class='error'>Please enter a username</p>";
            removeNotification('username_empty');
        }

        if(notificationExists('username_taken')){
            $messages[] = "<p class='error'>Username already exists</p>";
            removeNotification('username_taken');
        }

        if(notificationExists('password_empty')){
        	$messages[] = "<p class='error'>Please enter a password</p>";
            removeNotification('password_empty');
        }

		$this->makeView('churchadmin/register',compact('messages'));
	}

    /**
     * Handles the form request to add a new church admin
     * 
     * @return void
     */
	public function process_registration()
	{
		if(isset($_POST['churchadmin_regis']))
		{
            // contains error notification strings
			$errors = [];

            // contain other notification strings
            $notifications = [];

			if(empty($_POST['username'])){
				$errors[] = "username_empty";
            }
            else{
                $username = $_POST['username'];
            }

			if(empty($_POST['password'])){
				$errors[] = "password_empty";
            }
            else{
                $password = password_hash($_POST['password'],PASSWORD_DEFAULT);
            }

            if(empty($errors)){
                if(User::userNameExists($username,'CHURCH_ADMIN')){
                    $errors[] = "username_taken";
                }
            }

            if(empty($errors)){
				$user = new User();
				$user->setUsername($username);
				$user->setPassword($password);
				$user->setRole('CHURCH_ADMIN');
				$user->save();

				$notifications[] = 'registration_success';
                logNotifications($notifications);
            }
            else{
            	logNotifications($errors);
            }
			
		}	

        redirect('/churchadmin/register');

	}

    /**
     * Displays dashboard for church admins
     * 
     * @return void
     */
	public function dashboard()
	{
		// dashboard for church admins
		$styles = ['churchadmin.css'];
		$this->makeView('churchadmin/dashboard',compact('styles'));
	}

    /**
     * Show all church admins
     * 
     * @return void
     */
    public function view()
    {
        $churchadmins = User::getAllByRole('CHURCH_ADMIN');
        $this->makeView('churchadmin/view' , compact('churchadmins'));
    }	

    /**
     * Shows form to update password of a church admin
     *
     *
     * @return void
     */
    public function update_password()
    {
        $data = func_get_args();

        if(count($data) > 0){
            $id = $data[0];
        }

        $messages = [];

        if(notificationExists('password_updated')){
            $messages[] = "<p class='success'>Password updated successfully</p>";
            removeNotification('password_updated');
        }

        if(notificationExists('password_empty')){
            $messages[] = "<p class='error'>Please enter password</p>";
            removeNotification('password_empty');
        }

        $this->makeView('churchadmin/admin_update_password',compact('id','messages'));
    }

    /**
     * Processes the form to update password of a churchadmin
     *
     *
     * @return void
     */
    public function process_password_update()
    {

        if(isset($_POST['update_password'])){
            $errors = [];
            $notifications = [];

            $id = $_POST['id'];

            if(empty($_POST['new_password'])){
                $errors[] = 'password_empty';
            }
            else{
                $password = $_POST['new_password'];
            }

            if(empty($errors)){
                $user = User::find($id);
                $user->setPassword(password_hash($password,PASSWORD_DEFAULT));
                $user->save();

                $notifications[] = 'password_updated';
                logNotifications($notifications);
            }
            else{
                $notifications += $errors;
                logNotifications($notifications);
            }

            redirect('/churchadmin/update_password/'.$id);

        }
    }

    public function delete()
    {
        if(!sysAdminLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }

        $data = func_get_args();

        if(isset($data[0]) && is_numeric($data[0])){
            $id = $data[0];
        }

        $pastor = User::find($id);
        $pastor->delete();

        redirect('/churchadmin/view');
    }
}
?>