<?php
class Announcements extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Announcements Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the creation,update and display of church announcements
    | It's main model used is the Announcements model and like all other controllers,
    | it requires that the User model is loaded
    |
    */


	/**
     * Create a new Announcements controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->models = ['Announcement','User'];
        $this->loadModels();
    }


    /**
     * Displays the page containing the form for adding new church announcements
     * It is only accessible to logged in church admins
     *
     * @return void
     */
	public function add_new()
	{
		// if church admin is not logged in , display the system error view rather
		if(!churchAdminLoggedIn()){
			redirect('/churchman');
		}

        // holds messages that are returned after server-side form processing
		$messages = [];

       
        // Check for all possible notifications associated with this form and add the 
        // appropriate message to be displayed to the user in each case
        // After that remove the notification in each case
		if(notificationExists('empty_title')){
			$messages[] = "<p class='error'>Please enter a title for the announcement</p>";
			removeNotification('empty_title');
		}

		if(notificationExists('empty_message')){
			$messages[] = "<p class='error'>Please enter announcement message</p>";
			removeNotification('empty_message');
		}

		if(notificationExists('announcement_added')){
			$messages[] = "<p class='success'>Announcement added successfully</p>";
			removeNotification('announcement_added');
		}

		if(notificationExists('empty_date')){
			$messages[] = "<p class='error'>Please enter expiry date for announcement</p>";
			removeNotification('empty_date');
		}

        // load the view template file for this controller
        $this->makeView('announcements/create',compact('messages'));
	}


    /**
     * Processes the request to add a new announcement
     *
     * @return void
     */
	public function add_announcement()
	{
		// if form is submitted
		if(isset($_POST['add_announcement'])){
			// holds errors
			$errors = [];

            // holds notifications whether they are errors or success notifications
			$notifications = [];

            
			$title = $_POST['title'];
			$message = $_POST['message'];

            if(empty($title)){
            	$errors[] = 'empty_title';
            }

			if(empty($message)){
				$errors[] = 'empty_message';
			}

            // if a full date has been specified 
			if($_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
                $date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
			}
			else{
				$errors[] = 'empty_date';
			}

            
            // if the errors array still has no element , we are good to go
			if(empty($errors)){
				$announcement = new Announcement();

                $announcement->setTitle(sanitizeInput($title));
				$announcement->setDate(date('Y-m-d'));
				$announcement->setMessage(sanitizeInput($message));
				$announcement->setExpiryDate($date);

				$announcement->save();

                // add the notification to signify successful processing
				$notifications[] = 'announcement_added';
			}
			else{
				// transfer all errors to the notifications array
				// we use two different arrays with obvious names to make the code feel 
				// more natural
				$notifications += $errors;
			}

            // persist notifications
			logNotifications($notifications);

			// go back to the form page
			redirect('/announcements/add_new');
		}
	}


    /**
     * View all announcements.Announcements are not displayed individually.
     * They are all shown at once with pagination
     *
     * @return void
     */
	public function view()
	{
		// get all announcements
		$announcements = Announcement::findAll();

        // we need to display only announcements whose expiry dates have not passed
        // so we are eliminating outdated ones
		for($i = 0;$i < count($announcements);$i++){
			$announcement = $announcements[$i];
			
			$date1 = new PrettyDate($announcement->getExpiryDate());
			$date2 = new PrettyDate(date('Y-m-d'));

            // check if the announcement's expiry date falls behind today's date
			if(PrettyDate::compare($date1,$date2) == -1){
			    $announcements[$i] = null;
			}
		}

		$announcementsCopy = $announcements;
		$announcements = [];

        // re-building the announcements array to eliminate items we set to null
		foreach($announcementsCopy as $copy){
			if($copy != null){
				$announcements[] = $copy;
			}
		}

		$this->makeView('announcements/view',compact('announcements'));
	}

    /**
     * Shows the page with a form to edit an announcement
     * Only accessible to church admins that are logged in
     *
     * @return void
     */
	public function edit()
	{
		// redirect to home page if church admin is not logged in
		if(!churchAdminLoggedIn()){
			redirect('/churchman');
		}

		// get all parameters passed in the url
		$data = func_get_args();

		// stores messages to be displayed to user after server-side form processing
		$messages = [];

        // we expect at least one parameter which is the id of the 
        // announcement
		if(count($data) > 0){
			$id = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}

        // Check for all possible notifications associated with this form and add the 
        // appropriate message to be displayed to the user in each case
        // After that remove the notification in each case
		if(notificationExists('empty_title')){
			$messages[] = "<p class='error'>Please enter a title for the announcement</p>";
			removeNotification('empty_title');
		}

		if(notificationExists('empty_message')){
			$messages[] = "<p class='error'>Please enter an announcement</p>";
			removeNotification('empty_message');
		}

		if(notificationExists('empty_date')){
			$messages[] = "<p class='error'>Please specify an expiry date for the announcement</p>";
			removeNotification('empty_date');
		}


		$announcement = Announcement::find($id);

		$this->makeView('announcements/edit',compact('announcement','messages'));
	}

    /**
     * Processes the request to update an announcement
     * Only accessible to church admins that are logged in
     *
     * @return void
     */
	public function update_announcement()
	{
		// check if form is submitted
		if(isset($_POST['update_announcement'])){
			// contains errors
		    $errors = [];

		    // contains notifications whether they are errors or success
			$notifications = [];

            $id = $_POST['id'];
			$title = $_POST['title'];
			$message = $_POST['message'];

            if(empty($title)){
            	$errors[] = 'empty_title';
            }

			if(empty($message)){
				$errors[] = 'empty_message';
			}

			// if a full date has been specified 
			if($_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
                $date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
			}
			else{
				$errors[] = 'empty_date';
			}
 
            // if there are no errors,let's do the updates
			if(empty($errors)){
				$announcement = Announcement::find($id);

                $announcement->setTitle(sanitizeInput($title));
				$announcement->setDate(date('Y-m-d'));
				$announcement->setMessage(sanitizeInput($message));
				$announcement->setExpiryDate($date);

				$announcement->save();

				// we are redirecting to the views section instead of 
				// back to the form so no notification of success is needed
				redirect('/announcements/view');
			}
			else{
				// load the notifications array with the error notifications and go
				// back to the form page
				$notifications += $errors;
				logNotifications($notifications);
			    redirect('/announcements/edit/'.$id);
			}
		}
	}

    /**
     * Handles the request to delete an announcement from the database
     *
     * @return void
     */
	public function delete()
	{
		if(!churchAdminLoggedIn()){
			redirect('/');
		}

		$data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$id = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}

		$announcement = Announcement::find($id);
		$announcement->delete();

		redirect('/announcements/view');
	}
}
?>