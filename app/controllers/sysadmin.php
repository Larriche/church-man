<?php
class SysAdmin extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | System admins controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the actions pertaining to system admins
    | Not much to do here for now
    | 
    |
    */

    /**
     * Create a new system admin controller instance.
     *
     * @return void
     */
	public function __construct()
	{
		$this->models = ['User'];
		$this->loadModels();
	}

    /**
     * Displays the dashboard for the system administrator
     *
     * @return void
     */
	public function dashboard()
	{
		$styles = ['sysadmin.css'];
		$this->makeView('sysadmin/dashboard',compact('styles'));
	}
}
?>