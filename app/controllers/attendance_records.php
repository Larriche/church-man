<?php 
class AttendanceRecords extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | AttendanceRecords Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the creation and update of attendance records
    | for church events
    | It requires the Attendance , Event and User models
    |
    */


	/**
     * Create a new AttendanceRecords controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->models = ['Attendance','User','Event'];
        $this->loadModels();
    }

    public function process_add_new()
    {
        if(isset($_POST['record_attendance'])){
            $errors = [];
            $notifications = [];

            $event = $_POST['event'];
            
            if($_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00")
                $date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];

            if($_POST['date_hour'] !== '00' && $_POST['date_minute'] !== '00')
                $date .= ' '.$_POST['date_hour'].':'.$_POST['date_minute'];
            
            if(empty($_POST['attendance'])){
                $errors[] = 'empty_attendance';
            }
            else{
                $attendance = $_POST['attendance'];              
            }

            if(empty($errors)){
                $attendanceRecord = new Attendance();
                $attendanceRecord->setEvent($event);
                $attendanceRecord->setDate($date);
                $attendanceRecord->setNumber($attendance);

                $attendanceRecord->save();
            }
                       
        }
    }

    public function edit()
    {
        $data = func_get_args();
        $messages = [];

        if(isset($data[0]) && is_numeric($data[0])){
            $attendance_id = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        $attendance = Attendance::find($attendance_id);

        if($attendance == null){
            $this->makeView('errors/system_error');
            return;
        }

        if(notificationExists('attendance_error')){
            $messages[] = "<p class='error'>Please enter attendance count</p>";
            removeNotification('attendance_error');
        }

        if(notificationExists('attendance_updated')){
            $messages[] = "<p class='success'>Attendance record updated successfully</p>";
            removeNotification('attendance_updated');
        }

        $this->makeView('attendance/edit',compact('attendance' , 'messages'));

    }

    public function process_edit()
    {
        if(isset($_POST['update_attendance'])){
            $errors = [];
            $notifications = [];

            if(empty($_POST['attendance'])){
                $errors[] = 'attendance_error';
            }
            else{
                $attendance = $_POST['attendance'];
            }

            if($_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
                $date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
            }
            else{
                $date = "0000-00-00";
            }

            if($_POST['date_hour'] !== '00' && $_POST['date_minute'] !== '00'){
                $date .= ' '.$_POST['date_hour'].':'.$_POST['date_minute'];
            }

            if(!empty($_POST['note'])){
                $note = $_POST['note'];
            }
            else{
                $note = "";
            }

            $eventId = $_POST['event_id'];

            if(empty($errors)){
                $attendanceRecord = Attendance::find($eventId);
                $attendanceRecord->setEvent($eventId);
                $attendanceRecord->setDate($date);
                $attendanceRecord->setNumber($attendance);
                $attendanceRecord->setNote($note);

        
                $attendanceRecord->save();

                $notifications[] = "attendance_updated";
            }

            else{
                $notifications += $errors;               
            }

            logNotifications($notifications);
            redirect('/attendance_records/edit/'.$eventId);
            
        }
    }
}