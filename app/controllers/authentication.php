<?php
class Authentication extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users,updating of login credential and 
    | log out actions
    |
    */

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */

	public function __construct()
	{
		// load the models that this controller will make use of
		$this->models = ['User'];
		$this->loadModels();
	}

    /**
     * Shows the login page to the user 
     * It also process form validation messages and shows appropriate messages 
     * to the user
     * 
     * @return void
     */

	public function login()
	{
		$data = func_get_args();

		// the first argument is the role of the user
		if(isset($data[0])){
			$role = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}
        
        // messages from form validation to be passed to the user
        $messages = [];

        // check whether a password has just been updated by checking whether 
        // 'password_updated' is set in the sessions array
		if(notificationExists('password_updated')){
			$messages[] = "<p class='success'>Password updated.Login to continue</p>";
			removeNotification("password_updated");
		}

		// check to see if there is a notification of an invalid password
		if(notificationExists('invalid_password')){
			$messages[] = "<p class='error'>Username/Password is invalid.</p>";
			removeNotification("invalid_password");
		}

		// check to see if there is a notification of empty username
		if(notificationExists('username_empty')){
			$messages[] = "<p class='error'>Please enter username</p>";
			removeNotification('username_empty');
		}

		// check to see if there is a notification of empty password
		if(notificationExists('password_empty')){
			$messages[] = "<p class='error'>Please enter password</p>";
			removeNotification('password_empty');
		}

        // check if there is a notification of user account being inactive
        if(notificationExists('user_inactive')){
        	$messages[] = "<p class='error'>Please your account has been de-activated.</p>"
        	              ."<p class='error'>Please contact church administrators</p>";
        	              
        	removeNotification('user_inactive');
        }

		// display view associated with this method
		$this->makeView('auth/login',compact('role','styles','messages'));
	}
 
    /**
     * Processes login for a user
     *
     * @return void
     */

	public function process_login()
	{
		// mapping of links to redirect to after logging in based on user roles
		$whereTo = ["sys_admin"=>"/sysadmin/dashboard",
		            "church_admin"=>"/churchadmin/dashboard",
		            "pastor"=>"/pastors/dashboard",
		            "member"=>"/members/dashboard"
		            ];


		$username = $_POST['username'];
		$password = $_POST['password'];
		$role = $_POST['role'];

        $errors = [];

        if(empty($username))
        	$errors[] = "username_empty";

        if(empty($password))
        	$errors[] = "password_empty";

        if(empty($errors)){
			// check whether the entered credentials match that of a registered user
	        if(User::validLogin($username,$password,$role)){
	        	$user = User::findbyUsername($username,$role);

	        	if(!$user->isActive()){
	        		$errors[] = 'user_inactive';
	        	}
	        }
	        else{
	        	$errors[] = "invalid_password";
	        }
		}

		if(empty($errors)){
			session_regenerate_id();
			
			$_SESSION['user_id'] = $user->getId();
	        $_SESSION['user_role'] = $user->getRole();

	        session_write_close();
	        redirect($whereTo[$role]);
		}
		else{
		    // we had problems with how the form was filled so we pass
            // error notification into the sessions array
            logNotifications($errors);

            redirect('/authentication/login/'.$role.'/');	
		}
        
        
	}

    /**
     * Shows the password update page to the user 
     * It also process form validation messages and shows appropriate messages 
     * to the user
     * 
     * @return void
     */

	public function password_update()
	{
		$styles = ['authentication.css'];

		$messages = [];

        // check if there is a notification that the current password form field
        // was empty and add appropriate error message into messages to be displayed
		if(notificationExists('current_password_empty')){
            $messages[] = "<p class='error'>Please enter current password</p>";
            removeNotification('current_password_empty');
		}

        // check if there is a notification that the new password form field was empty
        // and add appropriate error message into messages to be displayed
        if(notificationExists('new_password_empty')){
        	$messages[] = "<p class='error'>Please enter new password</p>";
        	removeNotification('new_password_empty');
        }

        // check if there is a notification that the password entered in the current 
        // password form field didn't match that of the currently logged in user
        if(notificationExists('invalid_password')){
        	$messages[] = "<p class='error'>Invalid Current Password</p>";
        	removeNotification('invalid_password');
        }

		$this->makeView('auth/update_password',compact('messages','styles'));
	}

    
    /**
     * Handles password update form submissions for user currently logged in 
     * 
     * @return void
     */
	public function process_password_update()
	{
		if(isset($_POST['update_password'])){
			$user_id = $_SESSION['user_id'];
	    	$user = User::find($user_id);

	    	$errors = [];         // stores error notification strings
	    	$notifications = [];  // stores general notification strings
            
            if(empty($_POST['current_password'])){
            	$errors[] = "current_password_empty";
            }

            if(empty($_POST['new_password'])){
            	$errors[] = "new_password_empty";
            }
            
            if(empty($errors)){
            	// check if the user login credentials are valid
		    	if(!User::validLogin($user->getUsername(),$_POST['current_password'],$user->getRole()))
		    		$errors[] = "invalid_password";
		    }


	    	if(empty($errors)){
	    		// everything is ok so we proceed to update password

	    	    $newPassword = sanitizeInput($_POST['new_password']);

	    	    $user->setPassword(password_hash($newPassword,PASSWORD_DEFAULT));

	    	    // commit changes 
	    	    $user->save();

	    	    // add a notification of successful change
	    	    $notifications[] = 'password_updated';

                // logout user and redirect to login page
                logoutUser();

                logNotifications($notifications);
	    	    redirect('/authentication/login/church_admin');
	    	}
            else{
            	// we had problems with how the form was filled so we go back to
            	// the page with the various error notification variables set
            	$notifications += $errors;

            	logNotifications($notifications);

            	redirect('/authentication/password_update');
            }
		}
	}

    /**
     * Log out the user
     * 
     * @return void
     */
	public function logout()
	{
		logoutUser();
		redirect('/home/index');
	}
}