<?php
class Pledges extends controller
{
    /*
    |--------------------------------------------------------------------------
    | Pledges controller
    |--------------------------------------------------------------------------
    |
    | This controller handles actions pertaining to pledges including their
    | and their payments
    | 
    |
    */

	/**
     * Create a new Pledge controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->models = ['Pledge','PledgePayment', 'Project' , 'User','UserProfile'];
        $this->loadModels();
    }

    /**
     * View information about a given pledge
     *
     * @return void
     */
	public function view()
	{
        $data = func_get_args();

        if(isset($data[0]) && is_numeric($data[0])){
            $id = $data[0];
        }

        // only church admins,pastors or the pledgebook's owner can view
        // the pledges of a member and payment details
        if(!churchAdminLoggedIn() && !memberLoggedIn() && !pastorLoggedIn() ){
            $this->makeView('errors/system_error');
            return;
        }

		$styles = ['members.css'];       
        $pledge = Pledge::find($id);
        $pledgePayments = $pledge->getPayments();

        if(memberLoggedIn()){
            if($pledge->getPayer() != $_SESSION['user_id']){
                $this->makeView('errors/system_error');
                return;
            }
        }
        

        if(churchAdminLoggedIn()){
            $messages = [];

            if(notificationExists('empty_amount')){
                $messages[] = "<p class='error'>Please enter payment amount</p>";
                removeNotification('empty_amount');
            }

            if(notificationExists('empty_date')){
                $messages[] = "<p class='error'>Please enter payment date</p>";
                removeNotification('empty_date');
            }

            if(notificationExists('payment_recorded')){
                $messages[] = "<p class='success'>Payment recorded successfully</p>";
                removeNotification('payment_recorded');
            }

            $this->makeView('pledges/view',compact('pledge' , 'pledgePayments' , 'styles' , 'messages'));
        }

        if(memberLoggedIn()){
            $this->makeView('pledges/view',compact('pledge','pledgePayments'));
        }
        
	}

    /**
     * Process form to record a new pledge
     *
     * @return void
     */
    public function record_payment()
    {
        global $connection;


        if(isset($_POST['make_payment'])){
            $errors = [];
            $notifications = [];

            $pledge_id = $_POST['pledge_id'];   
            $amount = $_POST['amount'];
            $projectId = $_POST['project_id'];

            if(empty($amount))
                $errors[] = 'empty_amount';

            if($_POST['pay_day'] != "00" && $_POST['pay_month'] != "00" && $_POST['pay_year'] != "00"){
                $date = $_POST['pay_year'] .'-'.$_POST['pay_month'].'-'.$_POST['pay_day'];
            }
            else{
                $errors[] = 'empty_date';
            }

            if(empty($errors)){
                $connection->beginTransaction();

                $pledge = Pledge::find($pledge_id);
                $pledge->setAmountPaid((float)$pledge->getAmountPaid() + $amount);

                $pledge->save();

                $pledgePayment = new PledgePayment();

                $pledgePayment->setPledgeId($pledge->getId());
                $pledgePayment->setDate($date);
                $pledgePayment->setAmount($amount);

                $pledgePayment->save();

                Project::supplyFunds($projectId,$amount);

                $connection->commit();

                $notifications[] = 'payment_recorded';
            }
            else{
                $notifications += $errors;
            }

            logNotifications($notifications);
            redirect('/pledges/view/'.$pledge_id);

        }
    }

    /**
     * Show form to edit a pledge
     *
     * @return void
     */
    public function edit()
    {
        $data = func_get_args();
        $messages = [];

        if(isset($data[0])){
            $pledgeId = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        if(notificationExists('empty_amount')){
            $messages[] = "<p class='error'>Please enter amount pledged</p>";
            removeNotification('empty_amount');
        }

        if(notificationExists('pledge_updated')){
            $messages[] = "<p class='success'>Pledge details updated successfully</p>";
            removeNotification('pledge_updated');
        }

        $pledge = Pledge::find($pledgeId);
        $projects = Project::findAll();

        $this->makeView('pledges/edit',compact('pledge' , 'projects' , 'messages'));
    }

    /**
     * Process form to edit a pledge
     *
     * @return void
     */
    public function process_edit()
    {
        if(isset($_POST['update_pledge'])){
            $errors = [];
            $notifications = [];

            if(empty($_POST['amount'])){
                $errors[] = 'empty_amount';
            }
            else{
                $amount = $_POST['amount'];
            }
            
            if($_POST['pledge_day'] != "00" && $_POST['pledge_month'] != "00" && $_POST['pledge_year'] != "00"){
                $date = $_POST['pledge_year'] .'-'.$_POST['pledge_month'].'-'.$_POST['pledge_day'];
            }
            else{
                $date = "0000-00-00";
            }

            $projectId = $_POST['project_id'];
            $pledgeId = $_POST['pledge_id'];
        }

        if(empty($errors)){
            $pledge = Pledge::find($pledgeId);
            $pledge->setPledgedAmount($amount);
            $pledge->setDate($date);
            $pledge->setProject($projectId);
            $pledge->save();

            $notifications[] = 'pledge_updated';
        }
        else{
            $notifications += $errors;
        }

        logNotifications($notifications);
        redirect('/pledges/edit/'.$pledgeId);
    }   

    /**
     * Show form to edit payment
     *
     * @return void
     */
    public function edit_payment()
    {
        $data = func_get_args();
        $messages = [];

        if(isset($data[0])){
            $paymentId = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        if(notificationExists('empty_amount')){
            $messages[] = "<p class='error'>Please enter amount</p>";
            removeNotification('empty_amount');
        }

        if(notificationExists('payment_updated')){
            $messages[] = "<p class='success'>Payment updated successfully</p>";
            removeNotification('payment_updated');
        }
 
        $payment = PledgePayment::find($paymentId);

        $this->makeView('pledges/edit_payment',compact('payment' , 'messages'));
    }

    /**
     * Process form to edit payment
     *
     * @return void
     */
    public function process_payment_edit()
    {
        if(isset($_POST['update_payment'])){
            $errors = [];
            $notifications = [];

            if(empty($_POST['amount'])){
                $errors[] = 'empty_amount';
            }
            else{
                $amount = $_POST['amount'];
            }

            if($_POST['pay_day'] != "00" && $_POST['pay_month'] != "00" && $_POST['pay_year'] != "00"){
                $date = $_POST['pay_year'] .'-'.$_POST['pay_month'].'-'.$_POST['pay_day'];
            }
            else{
                $date = "0000-00-00";
            }

            $paymentId = $_POST['payment_id'];

            if(empty($errors)){
                $payment = PledgePayment::find($paymentId);
                $payment->setAmount($amount);
                $payment->setDate($date);
                $payment->save();

                $notifications[] = 'payment_updated';
            }
            else{
                $notifications += $errors;
            }

            logNotifications($notifications);
            redirect('/pledges/edit_payment/'.$paymentId);
        }
    }

    public function delete()
    {
        if(!churchAdminLoggedIn()){
            redirect('/');
        }

        $data = func_get_args();
        
        if(isset($data[0]) && is_numeric($data[0])){
            $id = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        $pledge = Pledge::find($id);
        $pledge->delete();

        redirect('/members/pledgebook/'.$pledge->getPayer());
    }
}
?>