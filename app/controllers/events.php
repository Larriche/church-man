<?php 
class Events extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Church events controller
    |--------------------------------------------------------------------------
    |
    | This controller handles actions pertaining to church events such as their
    | creation,update,recording of attendance and displaying attening units
    | Makes use of the Event,Group,Attendance,User and UserProfile models
    | 
    |
    */

	/**
     * Create a new Events controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->models = ['Event','Group','Attendance','User','UserProfile'];
        $this->loadModels();
    }

    /**
     * Display page with form for creating a new church event
     * Only accessible to church admins
     *
     * @return void
     */
	public function create()
	{
        if(!churchAdminLoggedIn()){
            redirect('/churchman');
        }

        // contains feedback messages to be displayed after server-side form processing
        $messages = [];

        
        // check for the various notifications,add the appropriate messages to the array
        // of messages and remove the notifications
		if(notificationExists('empty_name')){
            $messages[] = "<p class='error'>Please enter event name</p>";
            removeNotification('empty_name');
        }

        if(notificationExists('empty_description')){
            $messages[] = "<p class='error'>Please enter a description for the event</p>";
            removeNotification('empty_description');
        }

        if(notificationExists('event_created')){
            $messages[] = "<p class='success'>Event created successfully</p>";
            removeNotification('event_created');
        }

        // load html view located in 'app/views/events/create.phpt'
		$this->makeView("events/create",compact('messages'));
	}

    /**
     * Process the form for adding a new church event
     *
     * @return void
     */
    public function add_event()
    {
        // check if form is submitted
        if(isset($_POST['add_event'])){
            // contains error notification strings
            $errors = [];

            // contains other notification strings
            $notifications = [];

            $eventName = $_POST['event_name'];
            $type = $_POST['type'];
            $description = $_POST['description'];

            if(empty($eventName)){
                $errors[] = 'empty_name';
            }

            if(empty($description)){
                $errors[] = 'empty_description';
            }

            // check if full date has been specified 
            if($_POST['start_day'] != "00" && $_POST['start_month'] != "00" && $_POST['start_year'] != "00"){
                $startDate = $_POST['start_year'] .'-'.$_POST['start_month'].'-'.$_POST['start_day'];
            }
            else{
                $startDate = "0000-00-00";
            }

            // check if full date has been specified
            if($_POST['end_day'] != "00" && $_POST['end_month'] != "00" && $_POST['end_year'] != "00"){
                $closeDate = $_POST['end_year'] .'-'.$_POST['end_month'].'-'.$_POST['end_day'];
            }
            else{
                $closeDate = "0000-00-00";
            }

            if(empty($errors)){
                $event = new Event();

                $event->setName($eventName);
                $event->setDateStarted($startDate);
                $event->setDateCompleted($closeDate);
                $event->setDescription($description);
                $event->setEventType($type);

                $event->save();

                $notifications[] = 'event_created';
                logNotifications($notifications);
            }
            else{
               logNotifications($errors);
            }
            
            redirect('/events/create');
        }
    }


    /**
     * Display page for viewing all events
     *
     * @return void
     */
    public function view_all()
    {
        // fetch all events
        $events = Event::findAll();
       
        if(memberLoggedIn()){
            // do some filtering to show only the events that concern 
            // a member either as an individual or because he is part
            // of a group
            $eventsCopy  = $events;
            $events = [];

            foreach($eventsCopy as $event){
                if($event->getEventType() == 'GROUP'){
                    // if it is a group event,get IDs of all attending groups
                    $groups = $event->getAttendance();
                    
                    foreach($groups as $group){
                        $groupObj = Group::find($group);
                        
                        // if the member logged in is part of the current group,
                        // add the event to the events to be shown and break out
                        // to prevent unnecessary testing of other groups
                        if($groupObj->searchMember($_SESSION['user_id'])){
                            $events[] = $event;
                            break;
                        }
                    }
                }

                elseif($event->getEventType() == 'INDIVIDUALS'){
                    // if it is an individuals event, get the IDs of all attending
                    // church members and check whether the logged in member is among them
                    $attending = $event->getAttendance();
                    
                    if(count($attending) > 0 && in_array($_SESSION['user_id'],$attending)){
                        $events[] = $event;
                    }
                }

                else{
                    // general church events
                    $events[] = $event;
                }
            }
        }
        
        $this->makeView('events/view_all', compact('events'));
        
    }

    /**
     * Display page for viewing a church event with given id
     *
     * @return void
     */
    public function view()
    {
        // Get params passed to this controller method
        $data = func_get_args();
        
        // we  expect at least one argument or url parameter which happens to be the id
        // of the event
        if(isset($data[0]) && is_numeric($data[0])){
            $event = Event::find($data[0]);
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        // feedback messages after form processing
        $messages = [];

        // check whether there is a notification of no church member's name specified 
        // in the form to add attending members
        if(notificationExists('name_empty')){
            $messages[] = "<p class='error'>Please enter name of attending church member</p>";
            removeNotification('name_empty');
        }

        // check whether there is a notification of an attending church member's name
        // not found in the church records
        if(notificationExists('unknown_member')){
            $messages[] = "<p class='error'>Church member not found</p>";
            removeNotification('unknown_member');
        }

        // check whether there is a notification of a successful add of an attending member
        if(notificationExists('attending_member_added')){
            $messages[] = "<p class='success'>Attending member added successfully</p>";
            removeNotification('attending_member_added');
        }

        // check whether there is a notification of group already existing in attending list
        if(notificationExists('attending_group_exists')){
            $messages[] = "<p class='error'>Group already exists in attending list</p>";
            removeNotification('attending_group_exists');
        }

        // check whether there is a notification of member already existing in attending list
        if(notificationExists('attending_member_exists')){
            $messages[] = "<p class='error'>Member already exists in attending list</p>";
            removeNotification('attending_member_exists');
        }

        // check whether there is a notification of a successful add of an attending group
        if(notificationExists('attending_group_added')){
            $messages[] = "<p class'success'>Attending group added successfully</p>";
            removeNotification('attending_group_added');
        }

        // Javascript files to be loaded with this view
        $scripts = ['canvasjs.min.js','event.js'];

        $this->makeView('events/view' , compact('event' , 'scripts' , 'messages'));
    }


    /**
     * Handles the form request to add a group to list of groups attending an event
     *
     * @return void
     */
    public function add_attending_group()
    {
        if(isset($_POST['add_group'])){
            $group = sanitizeInput($_POST['group']);
            $eventId = $_POST['event_id'];
            $groupId = $_POST['group'];
            
            $event = Event::find($eventId);

            if(in_array($groupId,$event->getAttending())){
               $errors = ['attending_group_exists'];
            }

            if(empty($errors)){
                $event->addAttending($groupId);
                $event->save(); 

                $notifications = ['attending_group_added'];
                logNotifications($notifications);            
            }
            else{
                logNotifications($errors);
            }
            
            redirect('/events/view/'.$eventId);         
        }
    }

    /**
     * Handles the form request to add a member to list of members attending an event
     *
     * @return void
     */
    public function add_attending_member()
    {
        if(isset($_POST['add_member'])){
            // error notification strings
            $errors = [];

            // other notification strings
            $notifications = [];

            $memberName = sanitizeInput($_POST['member_name']);
            $eventId = $_POST['event_id'];

            $member = UserProfile::findByFullname($memberName);
            $event = Event::find($eventId);
           
            if(empty($memberName)){
                $errors[] = 'name_empty';
            }
            else{
                if($member == null){
                    $errors[] = 'unknown_member';
                }
                else{
                    if(in_array($member->getUserId(),$event->getAttending())){
                        $errors[] = 'attending_member_exists';
                    }
                }
            }

            if(empty($errors)){
                // form is filled correctly so we proceed
                $event->addAttending($member->getUserId());

                $event->save();

                $notifications[] = 'attending_member_added';
                logNotifications($notifications);
            }
            else{
                logNotifications($errors);
            }

            redirect('/events/view/'.$eventId);

        }
    }

    /**
     * Show information about financial transactions  pertaining to a sepcified
     * church event and also shows a form to record new finances
     *
     * @return void
     */
    public function finances()
    {
        if(!churchAdminLoggedIn() && !pastorLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }
        $data = func_get_args();

        if(count($data) > 0 && is_numeric($data[0])){
            $eventId = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        $event = Event::find($eventId);

        $messages = [];

        if(notificationExists('empty_amount')){
            $messages[] = "<p class='error'>Please enter amount</p>";
            removeNotification('empty_amount');
        }

        if(notificationExists('empty_description')){
            $messages[] = "<p class='error'>Please enter a short description</p>";
            removeNotification('empty_description');
        }

        if(notificationExists('transaction_recorded')){
            $messages[] = "<p class='success'>Transaction recorded successfully</p>";
            removeNotification('transaction_recorded');
        }

        if($event == null){
            $this->makeView('errors/system_error');
            return;
        }

        $this->makeView('events/finances',compact('event','messages'));
    }

    /**
     * Handles request to record a financial transaction for a church event
     *
     * @return void
     */
    public function record_finance()
    {
        if(isset($_POST['add_transaction'])){
            $errors = [];
            $notifications = [];

            if(empty($_POST['amount'])){
                $errors[] = 'empty_amount';
            }
            else{
                $amount = $_POST['amount'];
            }

            if($_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
                $date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
            }
            else{
                $date = "0000-00-00";
            }

            if(empty($_POST['description'])){
                $errors[] = 'empty_description';
            }
            else{
                $description = $_POST['description'];
            }

            $type = $_POST['type'];
            $eventId = $_POST['event_id'];

            if(empty($errors)){
                $event = Event::find($eventId);

                $event->recordTransaction($type,$amount,$date,$description);

                $notifications[] = "transaction_recorded";
            }
            else{
                $notifications += $errors;
                logNotifications($notifications);
                redirect('/events/finances/'.$eventId);
            }

        }
    }

    /**
     * Show attendance records  for  a specified
     * church event and also shows a form to record new attendance
     *
     * @return void
     */
    public function attendance()
    {
        if(!churchAdminLoggedIn() && !pastorLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }
        $data = func_get_args();

        if(count($data) > 0 && is_numeric($data[0])){
            $eventId = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        $messages =  [];

        if(notificationExists('empty_attendance')){
            $messages[] = "<p class='error'>Please enter attendance</p>";
            removeNotification('empty_attendance');
        }

        if(notificationExists('attendance_recorded')){
            $messages[] = "<p class='success'>Attendance recorded successfully</p>";
            removeNotification('attendance_recorded');
        }

        $event = Event::find($eventId);

        if($event == null){
            $this->makeView('errors/system_error');
            return;
        }

        $this->makeView('events/attendance',compact('event','messages'));
    }

     /**
     * 
     * Processes form request to record new attendance for a church event
     *
     * @return void
     */
    public function record_attendance()
    {
        if(isset($_POST['record_attendance'])){
            $errors = [];
            $notifications = [];

            if(empty($_POST['attendance'])){
                $errors[] = 'empty_attendance';
            }
            else{
                $attendance = $_POST['attendance'];
            }

            if($_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
                $date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
            }
            else{
                $date = "0000-00-00";
            }

            if($_POST['date_hour'] !== '00' && $_POST['date_minute'] !== '00'){
                $date .= ' '.$_POST['date_hour'].':'.$_POST['date_minute'];
            }

            if(!empty($_POST['note'])){
                $note = $_POST['note'];
            }
            else{
                $note = "";
            }

            $eventId = $_POST['event_id'];

            if(empty($errors)){
                $attendanceRecord = new Attendance();
                $attendanceRecord->setEvent($eventId);
                $attendanceRecord->setDate($date);
                $attendanceRecord->setNumber($attendance);
                $attendanceRecord->setNote($note);

                $attendanceRecord->save();

                $notifications[] = "attendance_recorded";
            }
            else{
                $notifications += $errors;
            }
            
            logNotifications($notifications);
            redirect('/events/attendance/'.$eventId);

        }
    }

    /**
     * Show the form to edit a specified church event
     * 
     *
     * @return void
     */
    public function edit()
    {
        $data = func_get_args();

        $messages = [];

        if(notificationExists('empty_name')){
            $messages[] = "<p class='error'>Please enter event name</p>";
            removeNotification('empty_name');
        }

        if(notificationExists('empty_description')){
            $messages[] = "<p class='error'>Please enter a description for the event</p>";
            removeNotification('empty_description');
        }

        if(notificationExists('event_updated')){
            $messages[] = "<p class='success'>Event updated successfully</p>";
            removeNotification('event_updated');
        }

        if(isset($data[0]) && is_numeric($data[0])){
            $event = Event::find($data[0]);
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        $this->makeView('events/edit',compact('event','messages'));
    }

    /**
     * Processes form request to edit a church event
     * 
     * @return void
     */
    public function process_edit()
    {
        if(isset($_POST['process_event'])){
            $errors = [];
            $notifications = [];

            if(empty($_POST['event_name'])){
                $errors[] = 'empty_name';
            }
            else{
                $eventName = $_POST['event_name'];
            }

            if(empty($_POST['description'])){
                $errors[] = 'empty_descriptionr';
            }
            else{
                $description = $_POST['description'];
            }

            if($_POST['start_day'] != "00" && $_POST['start_month'] != "00" && $_POST['start_year'] != "00"){
                $startDate = $_POST['start_year'] .'-'.$_POST['start_month'].'-'.$_POST['start_day'];
            }
            else{
                $startDate = "0000-00-00";
            }

            if($_POST['end_day'] != "00" && $_POST['end_month'] != "00" && $_POST['end_year'] != "00"){
                $closeDate = $_POST['end_year'] .'-'.$_POST['end_month'].'-'.$_POST['end_day'];
            }
            else{
                $closeDate = "0000-00-00";
            }
            
            $type = $_POST['type'];
            $event = Event::find($_POST['event_id']);

            if(empty($errors)){
                $event->setName($eventName);
                $event->setDateStarted($startDate);
                $event->setDateCompleted($closeDate);
                $event->setDescription($description);
                $event->setEventType($type);

                $event->save();

                $notifications[] = 'event_updated';
            }
            else{
                $notifications += $errors;
            }
            
            logNotifications($notifications);
            redirect('/events/edit/'.$event->getId());
        }
    }

    public function delete()
    {
        if(!churchAdminLoggedIn()){
            redirect('/');
        }

        $data = func_get_args();

        if(isset($data[0]) && is_numeric($data[0])){
            $id = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        $event = Event::find($id);
        $event->delete();

        redirect('/events/view_all');
    }
}
?>