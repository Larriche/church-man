<?php

class Home extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Home controller
    |--------------------------------------------------------------------------
    |
    | This controller is the default controller
    | 
    |
    */

    /**
     * Create a new home controller instance.
     *
     * @return void
     */
	public function __construct()
	{
        $this->models = ['User'];
		$this->loadModels();
	}
	
	/**
     * Displays the home page
     *
     * @return void
     */
	public function index()
	{
	    $this->makeView('home/index',compact('styles'));
	}
}