<?php

class Pastors extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Pastors controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the actions pertaining to pastors in the church
    | including their registration,profile setup and dashboard views
    | 
    |
    */

    /**
     * Create a new Pastors controller instance.
     *
     * @return void
     */
   
	public function __construct()
	{
		$this->models = ['User','Event'];
        $this->loadModels();
	}

    
    /**
     * Shows the form for registering a new user with role pastor
     *
     * @return void
     */
	public function register()
	{
		$messages = [];

		if(notificationExists('username_empty')){
			$messages[] = "<p class='error'>Please enter username</p>";
			removeNotification('username_empty');
		}

		if(notificationExists('password_empty')){
			$messages[] = "<p class='error'>Please enter password</p>";
			removeNotification('password_empty');
		}

		if(notificationExists('username_exists')){
			$messages[] = "<p class='error'>Username exists</p>";
			removeNotification('username_exists');
		}

		if(notificationExists('pastor_added')){
			$messages[] = "<p class='success'>Pastor added successfully</p>";
			removeNotification('pastor_added');
		}

		$this->makeView('pastors/register',compact('messages'));
	}

    /**
     * Processes form to register a pastor
     *
     * @return void
     */
	public function process_registration()
	{
		$errors = [];
		$notifications = [];
        
        $role = 'PASTOR';

		if(empty($_POST['username'])){
			$errors[] = 'username_empty';
		}
		else{
			$username = sanitizeInput($_POST['username']);
		}

		if(empty($_POST['password'])){
			$errors[] = 'password_empty';
		}
		else{
			$password = sanitizeInput($_POST['password']);
		}

		if(User::userNameExists($username,$role)){
			$errors[] = 'username_exists';
		}

		if(empty($errors)){
			$pastor = new User();

			$pastor->setUsername($username);
			$pastor->setPassword(password_hash($password,PASSWORD_DEFAULT));
			$pastor->setRole($role);

			$pastor->save();

			$notifications[] = 'pastor_added';
		}
		else{
			$notifications += $errors;
		}

		logNotifications($notifications);
		redirect('/pastors/register');
	}

    /**
     * Shows all registered pastors
     *
     * @return void
     */
	public function view()
	{
		$pastors = User::getAllByRole('PASTOR');

		$this->makeView('pastors/view',compact('pastors'));
	}

    /**
     * Dashboard for when a pastor is logged in
     *
     * @return void
     */
	public function dashboard()
	{
		if(!pastorLoggedIn()){
			$this->makeView('errors/system_error');
			return;
		}

		$this->makeView('pastors/dashboard');
	}

	/**
     * Shows form to update password of a pastor
     *
     *
     * @return void
     */
    public function update_password()
    {
        $data = func_get_args();

        if(count($data) > 0){
            $id = $data[0];
        }

        $messages = [];

        if(notificationExists('password_updated')){
            $messages[] = "<p class='success'>Password updated successfully</p>";
            removeNotification('password_updated');
        }

        if(notificationExists('password_empty')){
            $messages[] = "<p class='error'>Please enter password</p>";
            removeNotification('password_empty');
        }

        $this->makeView('pastors/update_password',compact('id','messages'));
    }

    /**
     * Processes the form to update password of a pastor
     *
     *
     * @return void
     */
    public function process_password_update()
    {

        if(isset($_POST['update_password'])){
            $errors = [];
            $notifications = [];

            $id = $_POST['id'];

            if(empty($_POST['new_password'])){
                $errors[] = 'password_empty';
            }
            else{
                $password = $_POST['new_password'];
            }

            if(empty($errors)){
                $user = User::find($id);
                $user->setPassword(password_hash($password,PASSWORD_DEFAULT));
                $user->save();

                $notifications[] = 'password_updated';
                logNotifications($notifications);
            }
            else{
                $notifications += $errors;
                logNotifications($notifications);
            }

            redirect('/pastors/update_password/'.$id);

        }
    }

	public function delete()
	{
		if(!churchAdminLoggedIn()){
			$this->makeView('errors/system_error');
			return;
		}

		$data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$id = $data[0];
		}

		$pastor = User::find($id);
		$pastor->delete();

		redirect('/pastors/view');
	}

}