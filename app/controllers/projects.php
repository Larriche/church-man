<?php
class Projects extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Projects controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the actions pertaining to church projects
    | such as their creation,edit and recording of pledges and donations 
    | 
    |
    */


    /**
     * Create a new projects controller instance.
     *
     * @return void
     */
	public function __construct()
	{
		// load the models this controller needs
		$this->models = ['Project','Pledge','User','UserProfile'];
		$this->loadModels();
	}


    /**
     * Displays form for creation of a new church project
     *
     * @return void
     */
	public function create()
	{
		$styles = ['projects.css'];

		$data = func_get_args();

		$messages = [];

		if(notificationExists('project_created')){
			$messages[] = "<p class='success'>Project added successfully</p>";
            removeNotification('project_created');
        }

		if(notificationExists('name_empty')){
			$messages[] = "<p class='error'>Please enter a name for the project</p>";
            removeNotification('name_empty');
        }

		if(notificationExists('description_empty')){
			$messages[] = "<p class='error'>Please enter a description for the project</p>";
            removeNotification('description_empty');
        }

		$this->makeView('projects/create',compact('styles' , 'messages'));
	}

    /**
     * View all projects the church has undertaken
     *
     * @return void
     */
	public function view_all()
	{
        $projects = Project::findAll();

		$this->makeView('projects/view_all',compact('projects'));
	}

    /**
     * View all projects the church has completed
     *
     * @return void
     */
    public function view_completed()
    {
        $projects = Project::getCompleted();

        $this->makeView('projects/view_completed',compact('projects'));
    }

    /**
     * View all projects the church is currently undergoing
     *
     * @return void
     */
    public function view_uncompleted()
    {
        $projects = Project::getUncompleted();

        $this->makeView('projects/view_active' , compact('projects'));
    }

    /**
     * View project whose id has been passed as the first argument 
     *
     * @return void
     */
	public function view()
	{
		$data = func_get_args();
		if(isset($data[0]) && is_numeric($data[0])){
            $id = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        $messages = [];

        if(notificationExists('pledge_added')){
            $messages[] = "<p class='success'>Pledge made successfully</p>";
            removeNotification('pledge_added');
        }

        if(notificationExists('empty_amount')){
            $messages[] = "<p class='error'>Please enter amount</p>";
            removeNotification('empty_amount');
        }

		$styles = ['projects.css'];

		$project = Project::find($id);

		$this->makeView('projects/view',compact('project' , 'styles' , 'messages'));

	}

    /**
     * Handles the form submission for creating a new project
     *
     * @return void
     */
    public function process_create()
    {
    	if(isset($_POST['create_project'])){

    		$errors = [];
            $notifications = [];

    		if(empty($_POST['project_name'])){
    			$errors[] = 'name_empty';
            }

    		if(empty($_POST['description'])){
    		    $errors[] = 'description_empty';
            }

            if($_POST['start_day'] != "00" && $_POST['start_month'] != "00" && $_POST['start_year'] != "00"){
                $startDate = $_POST['start_year'] .'-'.$_POST['start_month'].'-'.$_POST['start_day'];
            }
            else{
                $startDate = "0000-00-00";
            }

    		if(empty($errors)){
    		    $project = new Project();

	    		$project->setName(sanitizeInput($_POST['project_name']));

	    		$project->setDescription(sanitizeInput($_POST['description']));

	    		$project->setDateCommenced($startDate);

	    		$project->setEstimatedCost(sanitizeInput($_POST['estimated_cost']));

	    		$project->save();

	    		$notifications[] = 'project_created';
                logNotifications($notifications);
    		}
    		else{
    	        logNotifications($errors);		
    		}

            redirect('/projects/create');  		
    	}
    }

    /**
     * View the pledges made concerning a project
     *
     * @return void
     */
    public function pledges()
    {
        if(!churchAdminLoggedIn() && !memberLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }

        $data = func_get_args();

        if(count($data) > 0 && is_numeric($data[0])){
            $projectId = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        $project = Project::find($projectId);
        
        $this->makeView('projects/pledges',compact('project'));
    }

    /**
     * Shows the form for editing a project
     *
     * @return void
     */
    public function edit()
    {
        if(!churchAdminLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }

        $data = func_get_args();
        $messages = [];

        if(isset($data[0]) && is_numeric($data[0])){
            $id = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        $project = Project::find($id);

        if(notificationExists('project_updated')){
            $messages[] = "<p class='success'>Project has been updated successfully</p>";
            removeNotification('project_updated');
        }


        $this->makeView('projects/edit',compact('project','messages'));       
    }

    /**
     * Processes the form for editing a project
     *
     * @return void
     */
    public function process_edit()
    {
        if(isset($_POST['edit_project'])){

            $errors = [];
            $notifications = [];

            if(empty($_POST['project_name'])){
                $errors[] = 'name_empty';
            }

            if(empty($_POST['description'])){
                $errors[] = 'description_empty';
            }

            if($_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
                $date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
            }

            $project_id = $_POST['project_id'];

            if(empty($errors)){
                $project = Project::find($project_id);

                $project->setName(sanitizeInput($_POST['project_name']));

                $project->setDescription(sanitizeInput($_POST['description']));

                $project->setDateCommenced($date);

                $project->setEstimatedCost(sanitizeInput($_POST['estimated_cost']));

                $project->save();

                $notifications[] = 'project_updated';
            }
            else{
                $notifications += $errors;      
            }

            logNotifications($notifications);
            redirect('/projects/edit/'.$project->getId());
        }
    }

    /**
     * Change the status of the project whose id is passed to
     * completed
     *
     * @return void
     */

    public function mark_completed()
    {
        $data = func_get_args();

        if(isset($data[0]) && is_numeric($data[0])){
            $id = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
        }

        $project = Project::find($id);
        $project->setStatus('COMPLETED');
        $project->save();

        redirect('/projects/view_all');
    }

    /**
     * Change the status of the project whose id is passed
     * to uncompleted
     * 
     * @return void
     */
    public function mark_uncompleted()
    {
        $data = func_get_args();

        if(isset($data[0]) && is_numeric($data[0])){
            $id = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
        }

        $project = Project::find($id);
        $project->setStatus('UNCOMPLETED');
        $project->save();

        redirect('/projects/view_all');
    }
}
?>