<?php
class Reports extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Reports controller
    |--------------------------------------------------------------------------
    |
    | This controller handles actions pertaining to generation and display of 
    | PDF reports 
    |
    */

	public function __construct()
	{
		$this->models = ['UserProfile','Tithe'];
		$this->loadModels();
	}

    /**
     * Generate a PDF of all members of the church
     * 
     * @return void
     */
	public function all_members()
	{
		$dompdf = new DOMPDF();

		$html = file_get_contents('resources/reports/all_members.html');
		$replacement = "";

		$members = UserProfile::findAll();

		$count = 1;

		foreach($members as $member){
			$dateObj = new PrettyDate($member->getDateJoined());

			$date = $dateObj->getReadable();

			$replacement .= "<tr><td>".$count++."</td><td style='width: 250px'>".$member->getFullname()
			   ."</td><td style='width: 150px'>".$date."</td>"
			   ."<td style='width: 100px'>".$member->getCellPhone()."</td></tr>";
		}

		$html = str_replace("MEMBERS", $replacement, $html);


		$dompdf->load_html($html);
		$dompdf->render();

		$dompdf->stream("hello.pdf");
	}

    /**
     * Generate a PDF report of a church member
     * @return void
     */
	public function member()
	{
		$args = func_get_args();

		if(isset($args[0]) && is_numeric($args[0])){
			$id = $args[0];
		}
        
		$member = UserProfile::find($id);

        $template = 'resources/report_templates/member.html';
        $htmlOutput = 'member_temp'.$member->getUserId().'.html';
        $pdfName = $member->getFullname().'.pdf';
        $resources = [$member->getImageUrl() => 'member_temp'.$member->getUserId().'.jpg'];

        
        $birthDate = new PrettyDate($member->getDateOfBirth());
        $dateJoined = new PrettyDate($member->getDateJoined());
        
        $data = ["MEMBER_PIC" => 'member_temp'.$member->getUserId().'.jpg',
                 "FULLNAME" => $member->getTitle().' '.$member->getFullname(),
                 "DATE_OF_BIRTH" => $birthDate->getReadable(),
                 "OCCUPATION" => $member->getOccupation(),
                 "MARITAL_STATUS" => $member->getMaritalStatus(),
                 "SPOUSE" => $member->getSpouse(),
                 "LOCATION" => $member->getHouseAddress(),
                 "CITY" => $member->getCity(),
                 "CELL_PHONE" => $member->getCellPhone(),
                 "HOME_PHONE" => $member->getHomePhone(),
                 "WORK_PHONE" => $member->getWorkPhone(),
                 "EMAIL" => $member->getEmail(),
                 "EMERGENCY" => $member->getEmergencyContact(),
                 "DATE_JOINED" => $dateJoined->getReadable(),
                 "POSITION" => $member->getChurchPosition()
                ];	

		$pdfObj = new PDFReport($template,$htmlOutput,$pdfName,$data,$resources);
        $pdfObj->generatePDF();

	}

    /**
     * Generate a PDF report of the tithing information of a church member
     * @return void
     */
	public function tithebook()
	{
		$args = func_get_args();

		if(isset($args[0]) && is_numeric($args[0])){
			$payer_id = $args[0];
		}

		$tithes = Tithe::getMemberTitheRecords($payer_id);
		$member = UserProfile::find($payer_id);

        $tithesBlob = "";

        foreach($tithes as $tithe){
        	$titheDate = new PrettyDate($tithe->getDate());
        	$tithesBlob .= "<tr><td>".$titheDate->getReadable()."</td><td>Gh&#162;"
        	  .number_format($tithe->getAmount(),2,'.',',')."p</td></tr>";
        }

        $data = ['TITHES'=>$tithesBlob,
                 'MEMBER_PIC'=>'member_temp'.$member->getUserId().'.jpg',
                 'MEMBER_NAME'=>$member->getTitle().$member->getFullname()];

		$template = 'resources/report_templates/tithebook.html';
		$htmlOutput = 'tithe_temp'.$payer_id.'.html';
		$pdfName = $member->getFullname()."_tithebook.pdf";
		$resources = [$member->getImageUrl() => 'member_temp'.$member->getUserId().'.jpg'];

		$pdfObj = new PDFReport($template,$htmlOutput,$pdfName,$data,$resources);
        $pdfObj->generatePDF();
	}
}
?>