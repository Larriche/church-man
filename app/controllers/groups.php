<?php
class Groups extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Church Groups controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the actions pertaining to church groups
    | such as their creation and addition of new group members
    | 
    |
    */


    /**
     * Create a new groups controller instance.
     *
     * @return void
     */
	public function __construct()
	{
		// load models needed by this controller
        $this->models = ['Group' , 'GroupMember' , 'GroupRequest' ,'UserProfile','User'];
        $this->loadModels();

	}

    /**
     * Displays form for creating a new group.
     * Only accessible to church admins
     *
     * @return void
     */
	public function create()
	{
		if(!churchAdminLoggedIn() && !pastorLoggedIn()){
			redirect('/home/index');
		}
		
		$messages = [];

		if(notificationExists("group_created")){
			$messages[] = "<p class='success'>Group added successfully</p>";
			removeNotification("group_created");
		}

		if(notificationExists("empty_group_name")){
			$messages[] = "<p class='error'>Please enter group name</p>";
			removeNotification("empty_group_name");
		}

		if(notificationExists("empty_description")){
			$messages[] = "<p class='error'>Please enter description</p>";
			removeNotification("empty_description");
		}

		$this->makeView('groups/create',compact('styles' , 'messages'));
	}

    /**
     * Processes the form for creating a new church group
     *
     * @return void
     */
	public function process_create()
	{
		if(isset($_POST['create_group'])){

			$errors = [];
			$notifications = [];

			if(empty($_POST['group_name'])){
				$errors[] = "empty_group_name";
			}

			if(empty($_POST['description'])){
				$errors[] = "empty_description";
			}

			if($_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
                $date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
            }
            else{
                $date = "0000-00-00";
            }
            
            if(empty($errors)){
            	// we are good to go

            	$group = new Group();

	            $group->setName(sanitizeInput($_POST['group_name']));

	            $group->setDescription(sanitizeInput($_POST['description']));

	            $group->setDateCreated($date);

	            $group->setType($_POST['privacy']);

	            $group->save();

	            $notifications[] = 'group_created';
            }
            else{
            	// we had problems with how the form was filled so we pass
                // error notificaton strings as session variables
	            $notifications += $errors;
	
            }
            
            logNotifications($notifications);
            redirect('/groups/create');
            
		}
	}

    /**
     * View all church groups
     * 
     *
     * @return void
     */
	public function view_all()
	{
        $groups = Group::findAll();

        $activeGroups = [];
    	$inactiveGroups = [];

    	foreach($groups as $group){
    		if($group->getStatus() == 'ACTIVE'){
    			$activeGroups[] = $group;
    		}
    		else{
    			$inactiveGroups[] = $group;
    		}
    	}

        if(churchAdminLoggedIn() || pastorLoggedIn()){
		    $this->makeView('groups/view_all',compact('activeGroups','inactiveGroups'));
		}

		if(memberLoggedIn()){
			$memberGroups = Group::getMemberGroups($_SESSION['user_id']);
			$this->makeView('groups/members/view_all',compact('memberGroups','activeGroups'));
		}
	}

    /**
     * View a single group whose id is passed as the argument
     * 
     *
     * @return void
     */
	public function view()
	{
		$scripts = ['groups.js'];
        
        $data = func_get_args();
        $id = $data[0];

        $messages = [];

		$group = Group::find($id);

		$members = $group->getMembers();

        if(notificationExists('member_added')){
        	$messages[] = "<p class = 'success'>Member has been added to the group</p>";
        	removeNotification('member_added');
        }

		if(notificationExists('name_empty')){
			$messages[] = "<p class ='error'>Please enter member's name</p>";
			removeNotification('name_empty');
		}

		if(notificationExists('member_exists')){
			$messages[] = "<p class='error'>Member is already a group member</p>";
			removeNotification('member_exists');
		}

		if(notificationExists('member_not_found')){
			$messages[] = "<p class='error'>Church member not found</p>";
			removeNotification('member_not_found');
		}

		if(notificationExists('join_requested')){
			$messages[] = "<p class='success'>Your membership request has been made.</p>";
			removeNotification('join_requested'); 
		}


		$this->makeView('/groups/view',compact('styles' ,'scripts' , 'group' , 'members','messages'));
	}

    /**
     * Handles the form submission for the addition of a member to the group
     * 
     *
     * @return void
     */
	public function add_member()
	{
		if(isset($_POST['add_group_member'])){
			$errors = [];
			$notifications = [];

			$groupId = $_POST['group_id'];
			$userId;
			$role = $_POST['role'];


			if(empty($_POST['name'])){
				$errors[] = 'name_empty';
			}
			else{
				$name = explode(' ',$_POST['name']);

			    $firstname = $name[0];
			    $lastname =  $name[count($name) - 1];

			    $others = "";

			    for($i = 1;$i < count($name) - 1;$i++)
				$others .= $name[$i] . " ";

				$dbman = new DBSelectManager();
				$rows = $dbman->select('*')->from('user_profiles')->where('firstname','othernames','lastname')
				      ->match($firstname,$others,$lastname)->getRows();

				if(count($rows) > 0){
				    $userRow = $rows[0];
				    $userId = $userRow['user_id'];

				    if(GroupMember::memberExists($groupId,$userId)){
					    $errors[] = 'member_exists';
				    }
				}
				else{
					$errors[] = "member_not_found";
				}

			}

			if($_POST['joined_day'] != "00" && $_POST['joined_month'] != "00" && $_POST['joined_year'] != "00"){
                $date = $_POST['joined_year'] .'-'.$_POST['joined_month'].'-'.$_POST['joined_day'];
            }
            else{
                $date = "0000-00-00";
            }
			
			if(empty($errors)){
				$groupMember = new GroupMember();
	            $groupMember->setGroupId($groupId);
	            $groupMember->setUserId($userId);
	            $groupMember->setDateJoined($date);
	            $groupMember->setRole($role);

	            $groupMember->save();
	            $notifications[] = "member_added";
			}
			else {
			    $notifications += $errors; 
			}

	        logNotifications($notifications);
			redirect('/groups/view/'.$groupId);

		}
	}

     /**
     * Handles a request to be a member of a church group
     * 
     *
     * @return void
     */
	public function join_requests()
	{
		$data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$id = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}

		$joinRequest = new GroupRequest();

		$joinRequest->setUserId($_SESSION['user_id']);
		$joinRequest->setGroupId($id);

		$joinRequest->save();

        $notifications = ['join_requested'];
        logNotifications($notifications);

		redirect('/groups/view/'.$id);
	}

    /**
     * Shows the requests for membership for a specified group
     * 
     *
     * @return void
     */
	public function requests()
	{
	    $data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$id = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}

		$group = Group::find($id);

		// this action is only allowed for leaders of that group

	    $this->makeView('groups/requests',compact('group'));
	}

     /**
     * Handles an action to approve a group join request
     * 
     *
     * @return void
     */
	public function approve_request()
	{
	    $data = func_get_args();

        // we expect the id of the request as a parameter
		if(isset($data[0]) && is_numeric($data[0])){
			$id = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}

		$request  = GroupRequest::find($id);	
        
        
		$groupMember = new GroupMember();
        $groupMember->setGroupId($request->getGroupId());
        $groupMember->setUserId($request->getUserId());
        $groupMember->setDateJoined(date('Y-m-d'));
        $groupMember->setRole('MEMBER');

        $groupMember->save();

        $request->delete();

        redirect('/groups/requests/'.$request->getGroupId());
	}

    /**
     * Shows the form for editing a group
     * 
     *
     * @return void
     */
	public function edit()
	{
		if(!churchAdminLoggedIn()){
			$this->makeView('errors/system_error');
			return;
		}

		$data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$id = $data[0];
		}
		else{
			$this->makeView('error/system_error');
			return;
		}

		$group = Group::find($id);

		$messages = [];

		if(notificationExists('name_empty')){
			$messages[] = "<p class='error'>Please enter name of group</p>";
			removeNotification('name_empty');
		}

		if(notificationExists('description_empty')){
			$messages[] = "<p class='error'>Please enter description for the group</p>";
			removeNotification('description_empty');
		}

		if(notificationExists('group_updated')){
			$messages[] = "<p class='success'>Group has been updated successfully</p>";
			removeNotification('group_updated');
		}

		$this->makeView('groups/edit',compact('messages','group'));
	}

     /**
     * Processes the form for creating a new church group
     * 
     *
     * @return void
     */
	public function process_edit()
	{
		if(isset($_POST['edit_group'])){
			$errors = [];
			$notifications = [];

			if(empty($_POST['group_name'])){
				$errors[] = 'name_empty';
			}
            else{
            	$name = $_POST['group_name'];
            }

            if(empty($_POST['description'])){
            	$errors[] = 'description_empty';
            }
            else{
            	$description = $_POST['description'];
            }

            if($_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
                $date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
            }
            else{
            	$date = "0000-00-00";
            }

            $id = $_POST['group_id'];
            $privacyType = $_POST['privacy'];

            if(empty($errors)){
            	$group = Group::find($id);

            	$group->setName($name);
            	$group->setDescription($description);
            	$group->setDateCreated($date);
            	$group->setType($privacyType);

            	$group->save();

            	$notifications[] = 'group_updated';
            }
            else{
            	$notifications += $errors;
            }

            logNotifications($notifications);
            redirect('/groups/edit/'.$id);

		}
	}

    /**
     * Carries out the action to make a group inactive
     *
     * @return void
     */
	public function dissolve()
	{
		$data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$groupId = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}

		$group = Group::find($groupId);
		$group->setStatus('CLOSED');
		$group->setDateDissolved(date('Y-m-d'));
		$group->save();

		redirect('/groups/view_all');
	}

    /**
     * Carries out the action to make a dissolved group active again
     *
     * @return void
     */
	public function activate()
	{
		$data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$groupId = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}

		$group = Group::find($groupId);
		$group->setStatus('ACTIVE');
		$group->save();

		redirect('/groups/view_all');
	}

    /**
     * Carries out the action to remove group with the given id from the database
     *
     * @return void
     */
	public function delete()
	{
		$data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$groupId = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}

		$group = Group::find($groupId);
		$group->delete();

		redirect('/groups/view_all');
	}
}
?>