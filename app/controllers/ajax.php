<?php
class Ajax extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Ajax Requests Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles  ajax url requests
    | 
    |
    */

	public function __construct()
    {
        $this->models = ['UserProfile','User'];
        $this->loadModels();
    }

    /**
     * Generates a list of member names that match a given string
     * It is used for auto completing fields that require member names
     *
     * @return void
     */
	public function members()
	{
		if($_GET['name']){
			$searchStr = $_GET['name'];
		}else{
			$searchStr = '';
		}
	
		$members = UserProfile::findWhereNameLike($searchStr);
		$names = [];

		foreach($members as $member){
			$names[] = array('label' => $member->getFullname());
		}

		# echo the json data back to the html web page
		echo json_encode($names);
	}

}
?>