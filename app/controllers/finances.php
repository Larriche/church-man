<?php
class Finances extends Controller
{
	/*
    |--------------------------------------------------------------------------
    | Church Finances controller
    |--------------------------------------------------------------------------
    |
    | This controller handles actions pertaining to church events such as their
    | creation,update,recording of attendance and displaying attening units
    | Makes use of the Event,Group,Attendance,User and UserProfile models
    | 
    |
    */

	/**
     * Create a new Finances controller instance.
     *
     * @return void
     */
	public function __construct()
	{
		$this->models = ['User','UserProfile','Offering','Tithe','FinancialRecord'];
		$this->loadModels();
	}

    
	/**
     * Shows information about the offerings collected by the church
     * Also shows the form for recording new offerings
     *
     * @return void
     */
	public function offerings()
	{
		$scripts = ['canvasjs.min.js'];
		$offerings = Offering::findAll();

		$messages = [];

		if(notificationExists('amount_empty')){
			$messages[] = "<p class='error'>Please enter amount</p>";
			removeNotification('amount_empty');
		}

		if(notificationExists('date_empty')){
			$messages[] = "<p class='error'>Please specify date for offering</p>";
			removeNotification('date_empty');
		}

		if(notificationExists('offering_recorded')){
			$messages[] = "<p class='success'>Offering recorded successfully</p>";
			removeNotification('offering_recorded');
		}

		$this->makeView('finances/offerings',compact('scripts','offerings','messages'));
	}

    /**
     * Handles form request to add a new offering record to the database
     *
     * @return void
     */
	public function record_offering()
	{
		$errors = [];
		$notifications = [];

		if(isset($_POST['add_offering'])){
			if(empty($_POST['amount'])){
				$errors[] = 'amount_empty';
			}
			else{
				$amount = $_POST['amount'];
			}

			if( $_POST['date_day'] == "00" && $_POST['date_month'] == "00" && $_POST['date_year'] == "00"){
				$errors[] = 'date_empty';
			}
			else{
				$date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
			}

			if(empty($errors)){
				$offering = new Offering();
				$offering->setAmount($amount);
				$offering->setDate($date);
				$offering->save();

				$notifications[] = 'offering_recorded';
			}
			else{
				$notifications += $errors;
			}

			logNotifications($notifications);
			redirect('/finances/offerings');
		}
	}

    /**
     * Shows form for editing a church offering record 
     *
     * @return void
     */
	public function edit_offering()
	{
		if(!churchAdminLoggedIn()){
			$this->makeView('errors/system_error');
			return;
		}

		$data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$id = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}

		$offering = Offering::find($id);
		$messages = [];

		if(notificationExists('empty_amount')){
			$messages[] = "<p class='error'>Please enter amount</p>";
			removeNotification('empty_amount');
		}

		if(notificationExists('empty_date')){
			$messages[] = "<p class='success'>Please specify date</p>";
			removeNotification('empty_date');
		}

		if(notificationExists('offering_updated')){
			$messages[] = "<p class='success'>Offering updated successfully</p>";
			removeNotification('offering_updated');
		}


		$this->makeView('finances/edit_offering',compact('offering','messages'));
	}

    /**
     * Processes form request to edit a church offering record
     *
     * @return void
     */
	public function process_edit_offering()
	{
		if(isset($_POST['edit_offering'])){
			$errors = [];
			$notifications = [];

			if(empty($_POST['amount'])){
				$errors[] = 'empty_amount';
			}
			else{
				$amount = $_POST['amount'];
			}

			if( $_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
				$date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
			}
			else{
				$errors[] = 'empty_date';
			}

			$offeringId = $_POST['offering_id'];

			if(empty($errors)){
				$offering = Offering::find($offeringId);

				$offering->setAmount($amount);
				$offering->setDate($date);

				$offering->save();

				$notifications[] = 'offering_updated';
			}
			else{
				$notifications += $errors;
			}
            
            logNotifications($notifications);
            redirect('/finances/edit_offering/'.$offeringId);

		}
	}

    /**
     * Shows information about the tithe records of the church
     * Also shows the form for recording tithes given by visitors
     * with no membership info in church records
     *
     * @return void
     */
	public function tithes()
	{
		
		$tithes = Tithe::getTotalsByDay();
		$anonTithes = Tithe::getAnonymousTithes();
		$monthlyTithes = Tithe::getTotalsByMonth();

		$scripts = ['canvasjs.min.js'];

		$messages = [];

		if(notificationExists('tithe_recorded')){
			$messages[] = "<p class='success'>Tithe recorded successfully</p>";
			removeNotification('tithe_recorded');
		}

		if(notificationExists('empty_amount')){
			$messages[] = "<p class='error'>Please enter tithe amount</p>";
			removeNotification('empty_amount');
		}

		if(notificationExists('empty_date')){
			$messages[] = "<p class='error'>Please specify date</p>";
			removeNotification('empty_date');
		}

		$this->makeView('finances/tithes',compact('tithes','scripts','monthlyTithes','messages','anonTithes'));
	}

    /**
     * Handles form request to add a new tithe paid by non - members to the database
     *
     * @return void
     */
	public function record_tithes()
	{
		if(isset($_POST['record_tithe'])){
			$errors = [];
			$notifications = [];

			if(empty($_POST['amount'])){
				$errors[] = 'empty_amount';
			}
			else{
				$amount = $_POST['amount'];
			}

			if($_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
                $date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
			}
			else{
				$errors[] = 'empty_date';
			}

			if(empty($errors)){
				$tithe = new Tithe();

				$tithe->setAmount($amount);
				$tithe->setDate($date);
				$tithe->setPayer(NULL);

				$tithe->save();

				$notifications[] = 'tithe_recorded';
			}
			else{
				$notifications += $errors;
			}

			logNotifications($notifications);
			redirect('/finances/tithes');
		}
	}

    /**
     * Shows the form to edit a tithe record
     *
     * @return void
     */
	public function edit_tithe()
	{
		if(!churchAdminLoggedIn()){
			$this->makeView('errors/system_error');
			return;
		}

		$data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$id = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}

        $tithe = Tithe::find($id);
        $messages = [];

        if(notificationExists('empty_amount')){
        	$messages[] = "<p class='error'>Please enter amount</p>";
        	removeNotification('empty_amount');
        }

        if(notificationExists('empty_date')){
        	$messages[] = "<p class='error'>Please specify date</p>";
        	removeNotification('empty_amount');
        }

        if(notificationExists('tithe_updated')){
        	$messages[] = "<p class='success'>Tithe record has been updated successfully</p>";
        	removeNotification('tithe_updated');
        }


		$this->makeView('finances/edit_tithe',compact('tithe','messages'));
	}

    /**
     * Handles the request to edit a tithe record
     * @return void
     */
	function process_edit_tithe()
	{
		if(isset($_POST['edit_tithe'])){
			$errors = [];
			$notifications = [];

			if(empty($_POST['amount'])){
				$errors[] = 'empty_amount';
			}
			else{
				$amount = $_POST['amount'];
			}

			if( $_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
				$date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
			}
			else{
				$errors[] = 'empty_date';
			}

			$titheId = $_POST['tithe_id'];

			if(empty($errors)){
				$tithe = Tithe::find($titheId);

				$tithe->setAmount($amount);
				$tithe->setDate($date);

				$tithe->save();

				$notifications[] = 'tithe_updated';
			}
			else{
				$notifications += $errors;
			}
            
            logNotifications($notifications);
            redirect('/finances/edit_tithe/'.$titheId);

		}
	}
		
    /**
     * Shows graphed information about the church's incomes and expenditures
     *
     * @return void
     */
	public function totals()
	{
		$income = FinancialRecord::getRecordsByMonth('INCOME');
		$expenses = FinancialRecord::getRecordsByMonth('EXPENSE');
        $scripts = ['canvasjs.min.js'];
         
		$this->makeView('finances/totals',compact('income','expenses','scripts'));
	}

    /**
     * Shows form for recording financial transactions of the church
     *
     * @return void
     */
	public function record_new()
	{
		if(!churchAdminLoggedIn()){
			$this->makeView('errors/system_error');
			return;
		}

		$messages = [];

		if(notificationExists('empty_amount')){
			$messages[] = "<p class='error'>Please enter amount</p>";
			removeNotification('empty_amount');
		}

		if(notificationExists('transaction_recorded')){
			$messages[] = "<p class='success'>Record saved successfully</p>";
			removeNotification('transaction_recorded');
		}

		$this->makeView('finances/record_new',compact('messages'));
	}

    /**
     * Record a new financial transaction by the church either an income or an
     * expenditure
     *
     * @return void
     */
	public function add_new()
	{
		$errors = [];
		$notifications = [];

		if(isset($_POST['record_transaction'])){
			if(empty($_POST['amount'])){
				$errors[] = 'empty_amount';
			}
			else{
				$amount = $_POST['amount'];
			}

			if($_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
                $date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
			}
			else{
				$date = "0000-00-00";
			}

			$type = $_POST['type'];
			$description = $_POST['description'];

			if(empty($errors)){
				$financialRecord = new FinancialRecord();

				$financialRecord->setAmount($amount);
				$financialRecord->setDateRecorded($date);
				$financialRecord->setType($type);
				$financialRecord->setDescription($description);

				$financialRecord->save();

				$notifications[] = 'transaction_recorded';
			}
			else{
				$notifications += $errors;
			}

			logNotifications($notifications);
			redirect('/finances/record_new');
		}
	}
    
    /**
     * Shows detailed information about the incomes and expenditures of the
     * church
     *
     * @return void
     */
	public function details()
	{
		$records = FinancialRecord::findAll();

        $income = [];
        $expenses = [];

        foreach($records as $record){
        	if($record->getType() == 'INCOME'){
        		$income[] = $record;
        	}
        	else if($record->getType() == 'EXPENSE'){
        		$expenses[] = $record;
        	}
        }

		$this->makeView('finances/details',compact('income' , 'expenses'));
	}

	public function edit_others()
	{
		if(!churchAdminLoggedIn()){
			redirect('/');
		}
        
        $data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$id = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}

		$messages = [];

		if(notificationExists('empty_amount')){
			$messages[] = "<p class='error'>Please enter amount</p>";
			removeNotification('empty_amount');
		}

		if(notificationExists('record_updated')){
			$messages[] = "<p class='success'>Record updated successfully</p>";
			removeNotification('record_updated');
		}

		$financialRecord = FinancialRecord::find($id);
		
		$this->makeView('finances/edit_others' , compact('financialRecord' , 'messages'));
	}

	public function process_edit_others()
	{
		if(isset($_POST['process_edit'])){
			$errors = [];
			$notifications = [];

			if(empty($_POST['amount'])){
				$errors[] = 'empty_amount';
			}
			else{
				$amount = $_POST['amount'];
			}

			if( $_POST['date_day'] != "00" && $_POST['date_month'] != "00" && $_POST['date_year'] != "00"){
				$date = $_POST['date_year'] .'-'.$_POST['date_month'].'-'.$_POST['date_day'];
			}
			else{
				$date = "0000-00-00";
			}

			$recordId = $_POST['financialRecord_id'];

			if(empty($errors)){
				$record = FinancialRecord::find($recordId);

				$record->setAmount($amount);
				$record->setDateRecorded($date);

				$record->save();

				$notifications[] = 'record_updated';
			}
			else{
				$notifications += $errors;
			}
            
            logNotifications($notifications);
            redirect('/finances/edit_others/'.$recordId);

		}
	}

	public function delete_others()
	{
		if(!churchAdminLoggedIn()){
			redirect('/');
		}

		$data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$id = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
			return;
		}

		$record = FinancialRecord::find($id);
		$record->delete();

		redirect('/finances/details');
	}

	public function delete_tithe()
	{
		if(!churchAdminLoggedIn()){
			redirect('/');
		}

		$data = func_get_args();

		if(isset($data[0]) && is_numeric($data[0])){
			$id = $data[0];
		}

		$tithe = Tithe::find($id);
		$tithe->delete();

		redirect('/finances/tithes');
	}
}
?>