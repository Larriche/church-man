<?php
class Members extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Church Members controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the actions pertaining to church members
    | including their registration,profile setup and view
    | 
    |
    */

    /**
     * Create a new Members controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->models = ['User','UserProfile','Child','Tithe','Pledge','Project'];
        $this->loadModels();
    }

    /**
     * Displays the member registration page
     *
     * @return void
     */
    public function register()
    {
        // if a church admin is not logged in,we have no business doing here
        if(!ChurchAdminLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }

        // Get all arguments passed to this controller method
        $data = func_get_args();

        // Messages to be shown to user based on queued notifications
        $messages = [];

        // did the admin forget to enter a username for the member being registered?
        if(notificationExists('username_empty')){
            $messages[] = "<p class='error'>Please enter a username</p>";
            removeNotification('username_empty');
        }

        // did the admin forget to enter a password for the member being registered?
        if(notificationExists('password_empty')){
            $messages[] = "<p class='error'>Please enter a password</p>";
            removeNotification('password_empty');
        }
        
        // is the username already in use?
        if(notificationExists('username_taken')){
            $messages[] = "<p class='error'>Username is already taken.Please choose another one.</p>";
            removeNotification('username_taken');
        }

        // render member register page
    	$this->makeView('members/register',compact('messages'));
    }	

    /**
     * Handles the form request to register a new church member
     *
     * @return void
     */
    public function process_registration()
    {
        if(isset($_POST['member_regis']))
        {
            $errors = [];
            $notifications = [];

            $username = $_POST['username'];
            $password = $_POST['password'];

            if(empty($username)){
                $errors[] = "username_empty";
            }

            if(empty($password)){
                $errors[] = "password_empty";
            }

            if(empty($errors)){
                if(User::userNameExists($username,'MEMBER')){
                    $errors[] = "username_taken";
                }
            }

            if(empty($errors)){
                $password = password_hash($password,PASSWORD_DEFAULT);

                $user = new User();
                $user->setUsername(sanitizeInput($username));
                $user->setPassword(sanitizeInput($password));
                $user->setRole('MEMBER');
                $user->save();
                
                redirect('/members/setup_profile/'.$user->getId());
            }
            else{
                $notifications += $errors;

                logNotifications($notifications);
                redirect('/members/register');
            }

        }
    }

    /**
     * Displays page where a church admin or members themselves can set up
     * church member profiles
     *
     * @return void
     */
    public function setup_profile()
    {
        // if neither a church admin nor a member is logged in
        // we have no business here
        if(!churchAdminLoggedIn() && !memberLoggedIn()){
            $this->make('errors/system_error');
            return;
        }


        $data = func_get_args();

        if(churchAdminLoggedIn()){
            if(count($data) > 0 && is_numeric($data[0])){
                $user_id = $data[0];
            }
            else{
                $this->makeView('errors/system_error'); 
                return;              
            }

        }
        else{
            $user_id = $_SESSION['user_id'];
        }

        $styles = ['churchadmin.css'];
        $messages = [];
        
        if(notificationExists('profile_saved')){
            $messages[] = "<p class='success'>Profile has been saved</p>";
            removeNotification('profile_saved');
        }

        if(notificationExists('no_name_entered')){
            $messages[] = "<p class='error'>Please enter member's name</p>";
            removeNotification('no_name_entered');
        }

        if(notificationExists('father_not_member')){
            $messages[] = "<p class='error'>The member's father is not a registered member of the church</p>";
            removeNotification('father_not_member');
        }

        if(notificationExists('mother_not_member')){
            $messages[] = "<p class='error'>The member's mother is not a registered member of the church</p>";
            removeNotification('mother_not_member');
        }

        if(notificationExists('spouse_not_member')){
            $messages[] = "<p class='error'>The member's spouse is not a member of the church</p>";
            removeNotification('spouse_not_member');
        }


               

        $this->makeView('members/profile_setup',compact('user_id','messages'));
    }

    /**
     * Handles the form submission to set up profile of a church member
     * Here,it's not mandatory to fill all form fields so no strict check is made
     * for empty fields
     *
     *
     * @return void
     */
    public function process_profile_setup()
    {
        if(!churchAdminLoggedIn() && !memberLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }

        if(isset($_POST['profile_submit']))
        {
            $errors = [];
            $notifications = [];

            $file = $_FILES['profile_photo']['tmp_name'];
            $mime = $_FILES['profile_photo']['type'];
            $user_id = $_POST['user_id'];

            if(empty($_POST['firstname']) && empty($_POST['other_names']) && empty($_POST['surname'])){
                $errors[] = 'no_name_entered';
            }

            if(!empty($_POST['father'])){
                $father = UserProfile::findByFullname($_POST['father']);

                if($father == null){
                    $errors[] = "father_not_member";
                }
                else{
                    $fatherId = $father->getUserId();
                } 
            }
            

            if(!empty($_POST['mother'])){
                $mother = UserProfile::findByFullname($_POST['mother']);

                if($mother == null){
                    $errors[] = "mother_not_member";
                }
                else{
                    $motherId = $mother->getUserId();
                }
            }

            if(!empty($_POST['spouse'])){
                $spouse = UserProfile::findByFullname($_POST['spouse']);

                if($spouse == null){
                    $errors[] = "spouse_not_member";
                }
            }
            
            if(empty($errors)){
                if($file){
                    uploadPhoto($file,$mime,$user_id);
                }

                $profile = new UserProfile();

                $profile->setFirstname(sanitizeInput($_POST['firstname']));

                $profile->setOthernames(sanitizeInput($_POST['other_names']));

                $profile->setLastname(sanitizeInput($_POST['surname']));

                $profile->setTitle(sanitizeInput($_POST['member_title']));

                $profile->setGender($_POST['gender']);

                $profile->setMaritalStatus($_POST['marital_status']);

                $birthDay = $_POST['birth_day'];
                $birthMonth = $_POST['birth_month'];
                $birthYear = $_POST['birth_year'];

                if($birthDay != 0 && $birthMonth != 0 && $birthYear != 0){
                    $birthdate = $birthYear . "-" . $birthMonth . "-" . $birthDay;
                    $profile->setDateOfBirth($birthdate);
                }


                $joinedDay = $_POST['joined_day'];
                $joinedMonth = $_POST['joined_month'];
                $joinedYear = $_POST['joined_year'];

                if($joinedDay != 0 && $joinedMonth != 0 && $joinedYear != 0){
                    $joineddate = $joinedYear . "-" . $joinedMonth . "-" . $joinedDay;
                    $profile->setDateJoined($joineddate);
                }
                $profile->setSpouse(sanitizeInput($_POST['spouse']));

                $profile->setFather(sanitizeInput($_POST['father']));

                $profile->setMother(sanitizeInput($_POST['mother']));

                $profile->setOccupation(sanitizeInput($_POST['occupation']));

                $profile->setChurchPosition(sanitizeInput($_POST['position']));

                $profile->setHomePhone(sanitizeInput($_POST['home_phone']));

                $profile->setWorkPhone(sanitizeInput($_POST['work_phone']));

                $profile->setCellPhone(sanitizeInput($_POST['cell_phone']));

                $profile->setEmergencyContact(sanitizeInput($_POST['emergency_phone']));

                $profile->setEmail(sanitizeInput($_POST['email']));

                $profile->setHouseAddress(sanitizeInput($_POST['house_address']));

                $profile->setCity(sanitizeInput($_POST['city']));

                $profile->setUserId($_POST['user_id']);

                $profile->setDateLastEdited(date('Y-m-d'));

                $profile->setImageUrl("http://churchman.dev/resources/uploads/profile_photos/".$profile->getUserId().'.jpg');
        
                $profile->save();

                $notifications[] = 'profile_saved';
            }
            else{
                $notifications += $errors;
            }
            logNotifications($notifications);

            redirect('/members/setup_profile/'.$_POST['user_id']);

        }
    }


    /**
     * Shows the form for editing church member profiles
     *
     * @return void
     */
    public function edit()
    {
        $data = func_get_args();

        if(count($data) > 0 && is_numeric($data[0])){
            $user_id = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        if(!churchAdminLoggedIn() && !memberLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }
        else{
            if(memberLoggedIn() && ($user_id != $_SESSION['user_id'])){
                $this->makeView('errors/system_error');
                return;
            }
        }

        $member = UserProfile::find($user_id);

        if($member  == null){
            $this->makeView('errors/system_error');
            return;
        }

        $messages = [];

        if(notificationExists('no_name_entered')){
            $messages[] = "<p class='error'>Please enter member's name</p>";
            removeNotification('no_name_entered');
        }

        if(notificationExists('father_not_member')){
            $messages[] = "<p class='error'>The member's father is not a registered member of the church</p>";
            removeNotification('father_not_member');
        }

        if(notificationExists('mother_not_member')){
            $messages[] = "<p class='error'>The member's mother is not a registered member of the church</p>";
            removeNotification('mother_not_member');
        }

        if(notificationExists('spouse_not_member')){
            $messages[] = "<p class='error'>The member's spouse is not a member of the church</p>";
            removeNotification('spouse_not_member');
        }


        $this->makeView('members/edit' , compact('user_id','user','member', 'messages'));
    }

    /**
     * Handles the form submission to edit the profile of a church member
     * Here,it's not mandatory to fill all form fields so no strict check is made
     * for empty fields
     *
     *
     * @return void
     */
    public function process_edit()
    {
        if(isset($_POST['member_update'])){
            
            $id = $_POST['user_id'];

            $notifications = [];
            $errors = [];

            if(!churchAdminLoggedIn() && !memberLoggedIn()){
                $this->makeView('errors/system_error');
                return;
            }
            else{
                if(memberLoggedIn() && ($id != $_SESSION['user_id'])){
                    $this->makeView('errors/system_error');
                    return;
                }
            }

            if(empty($_POST['firstname']) && empty($_POST['other_names']) && empty($_POST['surname'])){
                $errors[] = 'no_name_entered';
            }

            if(!empty($_POST['father'])){
                $father = UserProfile::findByFullname($_POST['father']);

                if($father == null){
                    $errors[] = "father_not_member";
                }
                else{
                    $fatherId = $father->getUserId();
                } 
            }
            

            if(!empty($_POST['mother'])){
                $mother = UserProfile::findByFullname($_POST['mother']);

                if($mother == null){
                    $errors[] = "mother_not_member";
                }
                else{
                    $motherId = $mother->getUserId();
                }
            }

            if(!empty($_POST['spouse'])){
                $spouse = UserProfile::findByFullname($_POST['spouse']);

                if($spouse == null){
                    $errors[] = "spouse_not_member";
                }
            }

            if(empty($errors)){
                
                $profile = UserProfile::find($id);

                $profile->setFirstname(sanitizeInput($_POST['firstname']));

                $profile->setOthernames(sanitizeInput($_POST['other_names']));

                $profile->setLastname(sanitizeInput($_POST['surname']));

                $profile->setTitle(sanitizeInput($_POST['member_title']));

                $profile->setGender($_POST['gender']);

                $profile->setMaritalStatus($_POST['marital_status']);


                $birthDay = $_POST['birth_day'];
                $birthMonth = $_POST['birth_month'];
                $birthYear = $_POST['birth_year'];

                if($birthDay != 0 && $birthMonth != 0 && $birthYear != 0){
                    $birthdate = $birthYear . "-" . $birthMonth . "-" . $birthDay;
                    $profile->setDateOfBirth($birthdate);
                }


                $joinedDay = $_POST['joined_day'];
                $joinedMonth = $_POST['joined_month'];
                $joinedYear = $_POST['joined_year'];

                if($joinedDay != 0 && $joinedMonth != 0 && $joinedYear != 0){
                    $joineddate = $joinedYear . "-" . $joinedMonth . "-" . $joinedDay;
                    $profile->setDateJoined($joineddate);
                }

                $profile->setFather($fatherId);

                $profile->setMother($motherId);

                $profile->setSpouse(sanitizeInput($_POST['spouse']));

                $profile->setOccupation(sanitizeInput($_POST['occupation']));

                $profile->setChurchPosition(sanitizeInput($_POST['position']));

                $profile->setHomePhone(sanitizeInput($_POST['home_phone']));

                $profile->setWorkPhone(sanitizeInput($_POST['work_phone']));

                $profile->setCellPhone(sanitizeInput($_POST['cell_phone']));

                $profile->setEmergencyContact(sanitizeInput($_POST['emergency_phone']));

                $profile->setEmail(sanitizeInput($_POST['email']));

                $profile->setHouseAddress(sanitizeInput($_POST['house_address']));

                $profile->setCity(sanitizeInput($_POST['city']));

                $profile->setDateLastEdited(date('Y-m-d'));

                $file = $_FILES['profile_photo']['tmp_name'];
                $mime = $_FILES['profile_photo']['type'];
                 
                if($file)
                    uploadPhoto($file,$mime,$id);
        
                $profile->save();

                redirect('/members/view/'.$id);

            }
            else{
                $notifications += $errors;
                logNotifications($notifications);
                redirect('/members/edit/'.$id);
            }
        }
    }


    /**
     * View all active church members
     *
     *
     * @return void
     */
    public function view_all()
    {
        // only church admins and pastors are allowed to view member information
        if(!churchAdminLoggedIn() && !pastorLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }
        
        $offset = 10;

        $data = func_get_args();
        if(isset($data[0])){
            if(is_numeric($data[0])){
                $start = $data[0];
            }
            else{
                $this->makeView('errors/system_error');
                return;
            }
        }
        else{
            $start = 1;
        }

        $count = UserProfile::getPopulation();
        $memberProfiles = UserProfile::findByStatus('ACTIVE',$start - 1,$offset);


        $styles = ['members.css'];
        $this->makeView('members/view_all',compact('memberProfiles','styles','count','start','offset'));

    }

    
    /**
     * View a single member whose id is passed
     *
     * @param int id the id of the church member to be viewed
     * @return void
     */
    public function view()
    {
        $data = func_get_args();

        if(isset($data[0]) && is_numeric($data[0])){
            $id = $data[0];
        }

        if(!churchAdminLoggedIn() && !memberLoggedIn() && !pastorLoggedIn() ){
            $this->makeView('errors/system_error');
            return;
        }

        if(memberLoggedIn()){
            $loggedInMember = User::find($_SESSION['user_id']);

            if($id != $loggedInMember->getId()){
                $this->makeView('errors/system_error');
                return;
            }
        }

        $member = UserProfile::find($id);
        
        $styles = ['members.css'];

        $this->makeView('members/view' , compact( 'member' , 'styles'));
    }

    /**
     * View the tithe payment records of member whose id is passed
     *
     * 
     * @return void
     */
    public function tithebook()
    {
        $data = func_get_args();

        if(count($data) > 0 && is_numeric($data[0])){
            $payer_id = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        // only church admins,pastors or the pledgebook's owner can view
        // the pledges of a member and payment details
        if(!churchAdminLoggedIn() && !pastorLoggedIn() && !memberLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }
        else{
            if(memberLoggedIn() && $_SESSION['user_id'] != $payer_id){
                $this->makeView('errors/system_error');
                return;
            }
        }

        $styles = ['members.css'];
        
        $member = UserProfile::find($payer_id);
        $messages = [];

        $tithes = Tithe::getMemberTitheRecords($payer_id);

        if(churchAdminLoggedIn()){
            if(notificationExists('empty_amount')){
                $messages[] = "<p class='error'>Please enter an amount</p>";
                removeNotification('empty_amount');
            }

            if(notificationExists('empty_date')){
                $messages[] = "<p class='error'>Please enter date of payment</p>";
                removeNotification('empty_date');
            }

            if(notificationExists('tithe_recorded')){
                $messages[] = "<p class='success'>Tithe payment recorded successfully</p>";
                removeNotification('tithe_recorded');
            }

            $this->makeView('members/tithebook',compact('styles','member', 'tithes', 
                'messages'));
        }
        else if(memberLoggedIn()){
            $this->makeView('members/tithebook',compact('styles','member','tithes'));
        }
    }

     /**
     * Processes the form for recording the tithes of a member
     *
     *
     * @return void
     */
    public function record_tithe()
    {
        if(isset($_POST['tithe_add'])){
            $errors = [];
            $notifications = [];

            if(empty($_POST['amount'])){
                $errors[] = 'empty_amount';
            }

            if($_POST['pay_day'] != "00" && $_POST['pay_month'] != "00" && $_POST['pay_year'] != "00"){
                $date = $_POST['pay_year'] .'-'.$_POST['pay_month'].'-'.$_POST['pay_day'];
            }
            else{
                $errors[] = 'empty_date';
            }

            if(empty($errors)){
                $tithe = new Tithe();

                $tithe->setPayer($_POST['payer_id']);
                $tithe->setAmount($_POST['amount']);
                $tithe->setDate($date);
                
                $tithe->save();

                $notifications[] = "tithe_recorded";
            }
            else{
                $notifications += $errors;
            }
        }
        logNotifications($notifications);
        redirect('/members/tithebook/'.$_POST['payer_id']);

    }

     /**
     * Shows the pledges of a church member
     *
     *
     * @return void
     */
    public function pledgebook()
    {
        $data = func_get_args();

        if(count($data) > 0 && is_numeric($data[0])){
            $id = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
        }

        // only church admins,pastors or the pledgebook's owner can view
        // the pledges of a member and payment details
        if(!churchAdminLoggedIn() && !pastorLoggedIn() &&!memberLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }
        else{
            if(memberLoggedIn() && $_SESSION['user_id'] != $id){
                $this->makeView('errors/system_error');
                return;
            }
        }

        $styles = ['members.css'];
        $member = UserProfile::find($id);

        $pledges = Pledge::getMemberPledges($id);
        $projects = Project::findAll();

        $messages = [];

        if(notificationExists('empty_amount')){
            $messages[] = "<p class='error'>Please enter an amount</p>";
            removeNotification('empty_amount');
        }

        if(notificationExists('empty_date')){
            $messages[] = "<p class='error'>Please enter date pledge was made</p>";
            removeNotification('empty_date');
        }

        if(notificationExists('pledge_recorded')){
            $messages[] = "<p class='success'>New pledge recorded successfully</p>";
            removeNotification('pledge_recorded');
        }

        $this->makeView('members/pledgebook',compact('styles','member', 'pledges' , 'projects' , 'messages'));
    }

    /**
     * Processes the form submission to record a new pledge by member
     * when it is being carried out by a church admin
     *
     *
     * @return void
     */
    public function add_pledge()
    {
        $errors = [];
        $notifications = [];

        if(isset($_POST['add_pledge'])){
            if(empty($_POST['amount'])){
                $errors[] = 'empty_amount';
            }

            if($_POST['pay_day'] != "00" && $_POST['pay_month'] != "00" && $_POST['pay_year'] != "00"){
                $date = $_POST['pay_year'] .'-'.$_POST['pay_month'].'-'.$_POST['pay_day'];
            }
            else{
                $errors[] = 'empty_date';
            }


            if(empty($errors)){
                $pledge = new Pledge();

                $pledge->setPledgedAmount(sanitizeInput($_POST['amount']));
                $pledge->setDate($date);
                $pledge->setPayer($_POST['member_id']);
                $pledge->setProject($_POST['project']);

                $pledge->save();
                $notifications[] = "pledge_recorded";
            }
            else{
                $notifications += $errors;
            }

            logNotifications($notifications);
            redirect('/members/pledgebook/'.$_POST['member_id']);
        }
    }

    /**
     * Processes the form submission by a church member to pledge to a project
     *
     *
     * @return void
     */
    public function make_pledge()
    {
        if(isset($_POST['make_pledge'])){
            $errors = [];
            $notifications = [];

            if(empty($_POST['amount'])){
                $errors[] = 'empty_amount';
            }

            $member_id = $_POST['member_id'];
            $project_id = $_POST['project_id'];

            if(empty($errors)){
                $pledge = new Pledge();

                $pledge->setPledgedAmount(sanitizeInput($_POST['amount']));
                $pledge->setDate(date('Y-m-d'));
                $pledge->setPayer($member_id);
                $pledge->setProject($project_id);

                $pledge->save();
                $notifications[] = 'pledge_added';
            }
            else{
                $notifications += $errors;
            }

            logNotifications($notifications);
            redirect('/projects/view/'.$project_id);
        }
    }

    /**
     * Shows form to update password of church member
     *
     *
     * @return void
     */
    public function update_password()
    {
        $data = func_get_args();

        if(count($data) > 0){
            $id = $data[0];
        }

        $messages = [];

        if(notificationExists('password_updated')){
            $messages[] = "<p class='success'>Password updated successfully</p>";
            removeNotification('password_updated');
        }

        if(notificationExists('password_empty')){
            $messages[] = "<p class='error'>Please enter password</p>";
            removeNotification('password_empty');
        }

        $this->makeView('members/update_password',compact('id','messages'));
    }

    /**
     * Processes the form to update password of church member
     *
     *
     * @return void
     */
    public function process_password_update()
    {

        if(isset($_POST['update_password'])){
            $errors = [];
            $notifications = [];

            $id = $_POST['id'];

            if(empty($_POST['new_password'])){
                $errors[] = 'password_empty';
            }
            else{
                $password = $_POST['new_password'];
            }

            if(empty($errors)){
                $user = User::find($id);
                $user->setPassword(password_hash($password,PASSWORD_DEFAULT));
                $user->save();

                $notifications[] = 'password_updated';
                logNotifications($notifications);
            }
            else{
                $notifications += $errors;
                logNotifications($notifications);
            }

            redirect('/members/update_password/'.$id);

        }
    }

    
    /**
     * Shows the dashboard for logged in church members
     *
     * @return void
     */
    public function dashboard()
    {
        if(!memberLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }

        $this->makeView('members/dashboard');
    }

    /**
     * Do a soft delete on a church member
     * Member will no longer appear in searches and profile
     * but his data still exists and his name is still
     * associated with his pledges and other dealings
     *
     * @return void
     */
    public function delete()
    {
        if(!churchAdminLoggedIn()){
            redirect('/');
        }

        $data = func_get_args();

        if(isset($data[0]) && is_numeric($data[0])){
            $id = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        $member = User::find($id);
        $member->setStatus('INACTIVE');
        $member->save();

        redirect('/members/view_all');
    }

    /**
     *  View church members with de-activated accounts
     */
    public function inactive()
    {
        // only church admins and pastors are allowed to view member information
        if(!churchAdminLoggedIn() && !pastorLoggedIn()){
            $this->makeView('errors/system_error');
            return;
        }
        
        $offset = 3;

        $data = func_get_args();
        if(isset($data[0])){
            if(is_numeric($data[0])){
                $start = $data[0];
            }
            else{
                $this->makeView('errors/system_error');
                return;
            }
        }
        else{
            $start = 1;
        }

        $count = UserProfile::getPopulation();
        $memberProfiles = UserProfile::findByStatus('INACTIVE',$start - 1,$offset);
        $this->makeView('members/inactive',compact('memberProfiles','start','offset' ,'count'));
    }
    
    public function activate()
    {
        if(!churchAdminLoggedIn()){
            redirect('/');
        }

        $data = func_get_args();

        if(isset($data[0]) && is_numeric($data[0])){
            $id = $data[0];
        }
        else{
            $this->makeView('errors/system_error');
            return;
        }

        $member = User::find($id);
        $member->setStatus('INACTIVE');
        $member->save();

        redirect('/members/view_all');
    }

    public function search()
    {
        $name = $_POST['name'];

        $members = UserProfile::findWhereNameLike($name);

        $this->makeView('members/search_results' , compact('members'));
    }
}
?>