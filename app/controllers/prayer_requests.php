<?php
class PrayerRequests extends Controller
{
	 /*
    |--------------------------------------------------------------------------
    | Prayer Requests controller
    |--------------------------------------------------------------------------
    |
    | This controller handles actions pertaining to prayer requests including their
    | 
    |
    */

	/**
     * Create a new prayer request controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    	$this->models = ['User','UserProfile','Prayer'];
        $this->loadModels();
    }

    /**
     * Show form for making a prayer request
     *
     * @return void
     */
	public function create()
	{
		$messages = [];

		if(notificationExists('message_empty')){
			$messages[] = "<p class='error'>Please enter prayer request</p>";
			removeNotification('message_empty');
		}

		if(notificationExists('prayer_added')){
			$messages[] = "<p class='success'>Your prayer request has been made</p>";
			removeNotification('prayer_added');
		}

		$this->makeView('prayers/create',compact('messages'));
	}

    /**
     * Process creation of prayer request
     *
     * @return void
     */
	public function process_creation()
	{
		$errors = [];
		$notifications = [];

		if(isset($_POST['create_prayer'])){
			$userId = $_POST['user_id'];

		
			if(empty($_POST['message'])){
				$errors[] = 'message_empty';
			}
			else{
				$message = $_POST['message'];
			}

			if(empty($errors)){
				$prayer = new Prayer();

				$prayer->setUserId($userId);
				$prayer->setMessage($message);
				$prayer->setDateMade(date('Y-m-d'));
				$prayer->save();

				$notifications[] = 'prayer_added';
			}
			else{
				$notifications += $errors;
			}
		}
        
        logNotifications($notifications);
		redirect('/prayer_requests/create');
	}

    /**
     * Show all prayer requests by logged in member or all church
     * prayer requests when a pastor is logged in
     *
     * @return void
     */
	public function view_all()
	{
		if(!memberLoggedIn() && !pastorLoggedIn()){
			$this->makeView('errors/system_errors');
			return;
		}

		if(memberLoggedIn()){
			$user_id = $_SESSION['user_id'];
			$prayers = Prayer::getMemberPrayerRequests($user_id);

			$this->makeView('prayers/member/view_all',compact('prayers'));
		}
		else{
			$activePrayers = Prayer::getActive();
			$inactivePrayers = Prayer::getInActive();

			$this->makeView('prayers/pastors/view_all' , compact('activePrayers' , 'inactivePrayers'));
		}
	}

    /**
     * View a prayer request item
     *
     * @return void
     */
	public function view()
	{
		$data = func_get_args();

		if(isset($data[0])){
			$id = $data[0];
		}
		else{
			$this->makeView('errors/system_error');
		}

		$prayer = Prayer::find($id);

		$this->makeView('prayers/view',compact('prayer'));
	}
}
?>