<?php
class Prayer
{
	protected $id;

	protected $userId;

	protected $message;

	protected $dateMade;

	protected $status;

	protected $loadedFromTable = false;

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setUserId($id)
	{
		$this->userId = $id;
	}

	public function getUserId()
	{
		return $this->userId;
	}

	public function setMessage($message)
	{
		$this->message = $message;
	}

	public function getMessage()
	{
		return $this->message;
	}

	public function setDateMade($date)
	{
		$this->dateMade = $date;
	}

	public function getDateMade()
	{
		return $this->dateMade;
	}

	public function setStatus($status)
	{
		$this->status = $status;
	}

	public function getStatus()
	{
		return $this->status;
	}

	public function save()
	{
		if($this->loadedFromTable){
			//
		}
		else{
			$this->status = 'ACTIVE';
			$dbman = new DBInsertManager();
			$dbman->into('prayer_requests')->insert($this->message,$this->dateMade,$this->status,
				$this->userId)->fields('description','date_made','status','member_id')
			    ->execute();
		}
	}

	public static function createFromRow($row)
	{
		$prayer = new Prayer();

		$prayer->setId($row['id']);
		$prayer->setMessage($row['description']);
		$prayer->setDateMade($row['date_made']);
		$prayer->setStatus($row['status']);
		$prayer->setUserId($row['member_id']);

		return $prayer;
	}

	public static function find($id)
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('prayer_requests')->where('id')->match($id)
		   ->getRows();

		if(count($rows) > 0){
			return Prayer::createFromRow($rows[0]);
		}
		else{
			return null;
		}
	}

	public static function getActive()
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('prayer_requests')->where('status')->match('ACTIVE')
		   ->getRows();

		$prayers = [];

		if(count($rows)){
			foreach($rows as $row){
				$prayers[] = Prayer::createFromRow($row);
			}
		}

		return $prayers;
	}

	public static function getInActive()
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('prayer_requests')->where('status')->match('INACTIVE')
		   ->getRows();

		$prayers = [];

		if(count($rows)){
			foreach($rows as $row){
				$prayers[] = Prayer::createFromRow($row);
			}
		}

		return $prayers;
	}

	public static function getMemberPrayerRequests($id)
	{
		$prayers = [];

		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('prayer_requests')->where('member_id')
		  ->match($id)->getRows();

		if(count($rows) > 0){
			foreach($rows as $row){
				$prayers[] = Prayer::createFromRow($row);
			}
		}

		return $prayers;
	}
}
?>