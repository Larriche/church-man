<?php
class GroupRequest
{
    /*
    |--------------------------------------------------------------------------
    | Church Group Request Model
    |--------------------------------------------------------------------------
    |
    | This class represents a model of a request to join a church group
    |
    */

    // the id of this group request
	protected $id;

    // id of the church member requesting membership
	protected $userId;

    // the id of the group 
	protected $groupId;

    // determine whether this model was created using existing data
	protected $loadedFromTable = false;

    /**
     * Set the id of this group join request
     *
     * @param int the id of this request
     * @return void
     */
	public function setId($id)
	{
		$this->id = $id;
	}
    
    /**
     * Get the id of this group join request
     *
     * @return int the id
     */
	public function getId()
	{
		return $this->id;
	}

    /**
     * Set the id of the user requesting the membership
     * 
     * @param int the id
     * @return void
     */
	public function setUserId($id)
	{
		$this->userId = $id;
	}

    /**
     * Get the id of the user who made this group join request
     *
     * @return int the id of the user
     */
	public function getUserId()
	{
		return $this->userId;
	}
    
    /**
     * Set the id of the group associated with this membership request
     *
     * @param int the id
     * @return void
     */
	public function setGroupId($id)
	{
		$this->groupId = $id;
	}
 
    /**
     * Get the id of the group associated with this request
     *
     * @return int the id of the group
     */
	public function getGroupId()
	{
		return $this->groupId;
	}
    
    /**
     * Commit changes in this group membership request to the database
     *
     * @return void
     */
	public function save()
	{
		if($this->loadedFromTable){
			$dbman = new DBUpdateManager();

			$dbman->in('group_join_requests')->update('group_id' , 'user_id')
			      ->with($this->groupId , $this->userId)
			      ->where('id')->match($this->id)->execute();
		}
		else{
			$dbman = new DBInsertManager();
		    $dbman->into('group_join_requests')->insert($_SESSION['user_id'],$this->groupId)->fields('user_id','group_id')
		      ->execute();
		}
	}

    /**
     * Delete this group join request from the database
     *
     * @return void
     */
	public function delete()
	{
		$dbman = new DBDeleteManager();
		$dbman->from('group_join_requests')->where('id')->match($this->id)->delete();
	}
   
    /**
     * Create a group join request model from a given row of data from the database
     *
     * @param array the row of data
     * @return void
     */
	public static function createFromRow($row)
	{
		$request = new GroupRequest;

        $request->loadedFromTable = true;
		$request->setId($row['id']);
		$request->setUserId($row['user_id']);
		$request->setGroupId($row['group_id']);

		return $request;
	}

    /**
     * Get the group join request with the given id
     *
     * @param int the id
     * @return GroupRequest the group join request with the given id
     */
	public static function find($id)
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('group_join_requests')->where('id')
		   ->match($id)->getRows();

		if(count($rows) > 0){
			return GroupRequest::createFromRow($rows[0]);
		}
		else{
			return null;
		}
	}

}
?>