<?php
class PledgePayment
{
	protected $id;

	protected $pledgeId;

	protected $date;

	protected $amount;

    protected $loadedFromTable = false;

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setPledgeId($id)
	{
		$this->pledgeId = $id;
	}

	public function getPledgeId()
	{
		return $this->pledgeId;
	}

	public function setDate($date)
	{
		$this->date = $date;
	}

	public function getDate()
	{
		return $this->date;
	}

	public function setAmount($amount)
	{
		$this->amount = $amount;
	}

	public function getAmount()
	{
		return $this->amount;
	}

	public function save()
	{
		if($this->loadedFromTable){
			$dbman = new DBUpdateManager();

			$dbman->in('pledge_payments')->update('pledge_id', 'date_paid', 'amount')
			  ->with($this->pledge_id,$this->date,$this->amount)->where('id')
			  ->match($this->id)->execute();
		}
		else{
			$dbman = new DBInsertManager();

			$dbman->into("pledge_payments")->insert($this->pledge_id,$this->date,$this->amount)
			      ->fields('pledge_id', 'date_paid', 'amount')->execute();
		}
	}

	public static function createFromRow($row)
	{
		$pledgePayment = new PledgePayment();

        $pledgePayment->loadedFromTable = true;
		$pledgePayment->setId($row['id']);
		$pledgePayment->setPledgeId($row['pledge_id']);
		$pledgePayment->setAmount($row['amount']);
		$pledgePayment->setDate($row['date_paid']);

		return $pledgePayment;
	}

	public static function find($id)
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('pledge_payments')->where('id')->match($id)->getRows();

		if(count($rows) > 0){
			return PledgePayment::createFromRow($rows[0]);
		}

		return null;
	}
}