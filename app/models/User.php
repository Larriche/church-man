<?php
class User
{
	protected $id;

	protected $username;

	protected $password;

	protected $role;

	protected $lastUpdated;

    protected $loadedFromTable = false;

    protected $status;


    public function setUsername($name)
    {
    	$this->username = $name;
    }

    public function setPassword($password)
    {
    	$this->password = $password;
    }

    public function setRole($role)
    {
    	$this->role = $role;
    }

    public function setLastUpdate($date)
    {
    	$this->last_updated = $date;
    }

    public function setId($id)
    {
    	$this->id = $id;
    }

    public function getUsername()
    {
    	return $this->username;
    }

    public function getPasword()
    {
    	return $this->password;
    }

    public function getLastUpdated()
    {
    	return $this->lastUpdated;
    }

    public function getRole()
    {
    	return $this->role;
    }

    public function getId()
    {
    	return $this->id;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function isActive()
    {
        return ($this->status == 'ACTIVE');
    }

    public function save()
    {
        $this->lastUpdated = date('Y-m-d h-m-s');
        
        if($this->loadedFromTable)
        {
            // we are saving changes for a user retrieved from
            // the database so we do an UPDATE
            
            $dbman = new DBUpdateManager();
            $dbman->in('users')->update('username','password','date_last_edited','status')->
            with($this->username,$this->password,$this->lastUpdated,$this->status)->
            where('id')->match($this->id)->execute();
        }
        else{
            $dbman = new DBInsertManager();

            $this->status = 'ACTIVE';

            $dbman->into('users')->insert($this->username,$this->password,$this->role,$this->lastUpdated,$this->status)
                  ->fields('username','password','role','date_last_edited','status')->execute();

            // set the user id with the auto field given by the database
            $this->setId($dbman->getLastInsertId());

        }      
    }

    public function delete()
    {
        $dbman = new DBDeleteManager();
        $dbman->from('users')->where('id')->match($this->id)->delete();
    }

    public static function find($id)
    {
    	$dbman = new DBSelectManager();
        $rows = $dbman->select('*')->from('users')->where('id')->match($id)->getRows();
        $userRow = $rows[0];
        
        $user = User::createUserFromRow($userRow);
        $user->loadedFromTable = True;
    	return $user;   
    }

    public static function findByUsername($username,$role)
    {
    	$dbman = new DBSelectManager();
    	$rows = $dbman->select('*')->from('users')->where('username','role')->match($username,$role)->getRows();
    	$userRow = $rows[0];
    	$user = User::createUserFromRow($userRow);
    	return $user;
    }

    public static function createUserFromRow($userRow)
    {
    	$user = new User();

        $user->loadedFromTable = true;

        $user->setId($userRow['id']);
        $user->setUsername($userRow['username']);
        $user->setPassword($userRow['password']);
        $user->setRole($userRow['role']);
        $user->setLastUpdate($userRow['date_last_edited']);
        $user->setStatus($userRow['status']);

        return $user;
    }

    public static function validLogin($username,$password,$role)
    {
    	$dbman = new DBSelectManager();
    	$rows = $dbman->select('*')->from('users')->where('username','role')
    	         ->match($username,$role)->getRows();

        if(count($rows) > 0){
            $savedPassword = $rows[0]['password'];
            if(password_verify($password,$savedPassword))
            	return true;
            else
            	return false;
        } 
        return false;
    }

    public static function userNameExists($username,$role){
        $dbman = new DBSelectManager();

        $rows = $dbman->select('*')->from('users')->where('username','role')->match($username,$role)->getRows();

        if(count($rows) > 0){
            return true;
        }
        
        return false;
    }

    public static function getAllByRole($role)
    {
        $dbman = new DBSelectManager();
        $rows = $dbman->select("*")->from('users')->where('role')->match($role)->getRows();
        $userObjects = [];

        foreach($rows as $row){
            $userObjects[] = User::createUserFromRow($row);
        }

        return $userObjects;
    }

    public static function readableRole($role)
    {
        switch($role)
        {
            case 'SYS_ADMIN':
                return "system administrator";

            case 'CHURCH_ADMIN':
                return "church administrator";

            case 'PASTOR':
                return "pastor";

            case 'MEMBER':
                return "church member";
        }
    }

}
?>