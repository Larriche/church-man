<?php
class Event
{
    /*
    |--------------------------------------------------------------------------
    | Church Event model
    |--------------------------------------------------------------------------
    |
    | This class represents a Church event.Through this class,you interact with 
    | the events table and other dependent tables to get data about a church 
    | event
    |
    */

    // the id of this church event
	protected $id;

    // the name of this church event
	protected $name;

    // the description for this church event
	protected $description;

    // the date this event was started
	protected $dateStarted;

    // the date this event was completed
	protected $dateCompleted;

    // the type of event
	protected $eventType;

    // current list of people to attend this event
    protected $currentAttending;

    // a new list of people to attend this event
    protected $newAttending;

    // current attendance record for this event
    protected $currentAttendanceRecords = [];
    
    // new attendance records
    protected $newAttendanceRecord;
    

    /**
     * Set the id for the event instance using data from database
     *
     * @param the id
     * @return void
     */
    public function setId($id)
    {
    	$this->id = $id;
    }

    /**
     * Get the id of the event instance using data from database
     *
     * @return int the id of this event instance
     */
    public function getId()
    {
    	return $this->id;
    }

    /**
     * Set the name for the event instance 
     *
     * @param string the name
     * @return void
     */
    public function setName($name)
    {
    	$this->name = $name;
    }
   
    /**
     * Get the name for this event instance
     *
     * @return string the name of the event instance
     */
    public function getName()
    {
    	return $this->name;
    }

    /**
     * Set the description for this church event
     *
     * @param string the description
     * @return void
     */
    public function setDescription($description)
    {
    	$this->description = $description;
    }

    /**
     * Get the description for this church event
     *
     * @return string the description for this event
     */
    public function getDescription()
    {
    	return $this->description;
    }

    /**
     * Set the date this event was started
     *
     * @param string the date
     * @return void
     */
    public function setDateStarted($date)
    {
    	$this->dateStarted = $date;
    }

    /**
     * Get the date this event was started
     *
     * @return string the date this event was started
     */
    public function getDateStarted()
    {
        return $this->dateStarted;
    }

    /**
     * Set the date this event was completed
     *
     * @param string the date
     * @return void
     */
    public function setDateCompleted($date)
    {

        $this->dateCompleted = $date;
    }

    /**
     * Get the date this event was completed
     *
     * @return string the date as gotten from database
     */
    public function getDateCompleted()
    {
    	return $this->dateCompleted;
    }

    /**
     * Set the event type for this event
     *
     * @param string the event type
     * @return void
     */
    public function setEventType($type)
    {
    	$this->eventType = $type;
    }

    /**
     * Get the event type for this event
     *
     * @return string the type of this event
     */
    public function getEventType()
    {
    	return $this->eventType;
    }

    /**
     * Get a list of the individuals or members to attend this event
     *
     * @return array the list of groups or individuals to attend this event
     */
    public function getAttendance()
    {
        return $this->currentAttending;
    }

    /**
     * Add a new group or individual to attend this event
     *
     * @return void
     */
    public function addAttending($id)
    {
        $this->newAttending[] = $id;
    }

    /**
     * Get the attendance records for this event
     *
     * @return array a collection of attendance record objects
     */
    public function getAttendanceRecord()
    {
        return $this->currentAttendanceRecords;
    }

   
    /**
     * Record a financial transaction pertaining to this event
     *
     * @return void
     */
    public function recordTransaction($type,$amount,$date,$description)
    {
        $dbman = new DBInsertManager();
        
    
        $dbman->into('event_finances')->insert($this->id,$amount,$type,$date,$description)->
              fields('event_id','amount','type','date_recorded','description')->execute();
    }

   
    /**
     * Get all incomes gotten from this event
     *
     * @return array rows of income as obtained from database
     */
    public function getIncome()
    {

        $dbman = new DBSelectManager();

        $incomes = $dbman->select('*')->from('event_finances')->where('event_id','type')->match($this->id,'INCOME')
           ->getRows();

        return $incomes;
    }

    /**
     * Get all expenses made during this event
     *
     * @return array rows of expenses as obtained from database
     */
    public function getExpenses()
    {
        $dbman = new DBSelectManager();

        $expenses = $incomes = $dbman->select('*')->from('event_finances')->where('event_id','type')->match($this->id,'EXPENSE')
           ->getRows();

        return $expenses;
    }

    /**
     * Persist changes in the properties of this event to the database
     *
     * @return void
     */
    public function save()
    {
        if($this->loadedFromTable){
            $dbman = new DBUpdateManager();

            $dbman->in('events')->update('event_name','description','date_of_commencement','date_ended',
                'event_type')->with($this->name,$this->description,$this->dateStarted,$this->dateCompleted,
                $this->eventType)->where('id')->match($this->id)->execute();
        }
        else{
            $dbman = new DBInsertManager();

            $dbman->into('events')->insert($this->name,$this->description,$this->dateStarted,$this->dateCompleted,
                $this->eventType)->fields('event_name','description','date_of_commencement','date_ended','event_type')
                ->execute();
        }

        if(count($this->newAttending) > 0){
            $dbman = new DBInsertManager;

            if($this->eventType == "GROUP"){
                foreach($this->newAttending as $attending){
                    $dbman->into('groups_to_events')->insert($this->id,$attending)->fields('event_id','group_id')
                    ->execute();
                }
            }

            if($this->eventType == 'INDIVIDUALS'){
                foreach($this->newAttending as $attending){
                    $dbman->into('members_to_events')->insert($this->id,$attending)->fields('event_id','member_id')
                    ->execute();
                }
            }
        }
    }

    /**
     * Delete this event from the database
     *
     * @return void
     */
    public function delete()
    {
        $dbman = new DBDeleteManager();
        $dbman->from('events')->where('id')->match($this->id)->delete();
    } 

    /**
     * Create a church event model object from a row obtained from the database
     *
     * @return Event a church event model instance
     */
    public static function createFromRow($row)
    {
        $event = new Event();

        $event->loadedFromTable = true;

        $event->setId($row['id']);
        $event->setName($row['event_name']);
        $event->setDescription($row['description']);
        $event->setDateStarted($row['date_of_commencement']);
        $event->setDateCompleted($row['date_ended']);
        $event->setEventType($row['event_type']);

        if($event->getEventType() == 'GROUP'){
            $dbman = new DBSelectManager();

            $rows = $dbman->select('group_id')->from('groups_to_events')->where('event_id')->match($event->getId())
                       ->getRows();

            foreach($rows as $attendRow){
                $event->currentAttending[] = $attendRow['group_id'];
            }

        }
        else if($event->getEventType() == 'INDIVIDUALS'){
            $dbman = new DBSelectManager();

            $rows = $dbman->select('member_id')->from('members_to_events')->where('event_id')->match($event->getId())
                        ->getRows();

            foreach($rows as $attendRow){
                $event->currentAttending[] = $attendRow['member_id'];
            }
        }

        $dbman = new DBSelectManager();

        $rows = $dbman->select('*')->from('attendance')->where('event_id')->match($event->getId())
                    ->getRows();

        foreach($rows as $row){
            $event->currentAttendanceRecords[] = Attendance::createFromRow($row);
        }

        return $event;
    }

    /**
     * Get the event with specified id
     *
     * @return Event event with specified id
     */
    public static function find($id)
    {
        $dbman =  new DBSelectManager();

        $rows = $dbman->select("*")->from("events")->where("id")->match($id)->getRows();

        if(count($rows) > 0){
            $event = Event::createFromRow($rows[0]);

            return $event;
        }

        return null;
    }

    /**
     * Get all events from the database
     *
     * @return array a collection of all events
     */
    public static function findAll()
    {
        $dbman = new DBSelectManager();

        $eventObjs = [];
        $rows = $dbman->select('*')->from('events')->getRows();

        if(count($rows) > 0){
            foreach($rows as $row)
                $eventObjs[] = Event::createFromRow($row);
        }

        return $eventObjs;

    }
}
?>