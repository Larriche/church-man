<?php
class Offering
{
	/*
    |--------------------------------------------------------------------------
    | Church Offering Model
    |--------------------------------------------------------------------------
    |
    | This class represents a model of a recording of church  offerings
    |
    */

    // the id of this offering recording
	protected $id;

    // the amount of the offering
	protected $amount;

    // the date the offering was recorded
	protected $date;

    // whether this offering model was created using data from the database
	protected $loadedFromTable = false;


    /**
     * Set the id of this church offering record
     *
     * @param int the id
     * @return void
     */
	public function setId($id)
	{
		$this->id = $id;
	}

    /**
     * Get the id of this church offering model
     *
     * @return int the id of this church offering record model
     */
	public function getId()
	{
		return $this->id;
	}

    /**
     * Set the amount of this church offering model
     *
     * @param double the amount of this church offering 
     * @return void
     */
	public function setAmount($amount)
	{
		$this->amount = $amount;
	}
    
    /**
     * Get the amount of this church offering
     *
     * @return double the amount
     */
	public function getAmount()
	{
		return $this->amount;
	}

    /**
     * Set the date this offering was recorded
     * 
     * @param string the date
     * @return void
     */
	public function setDate($date)
	{
		$this->date = $date;
	}

    /**
     * Get the date this offering was recorded
     * 
     * @return string the date this offering was recorded
     */
	public function getDate()
	{
		return $this->date;
	}

    /**
     * Commit changes in this model's properties to the database
     *
     * @return void
     */
	public function save()
	{
		if($this->loadedFromTable){
			$dbman = new DBUpdateManager();

			$dbman->in('offerings')->update('amount','date_paid')->with($this->amount,$this->date)
			  ->where('id')->match($this->id)->execute();
		}
		else{
			$dbman = new DBInsertManager();

			$dbman->into('offerings')->insert($this->amount,$this->date)->fields('amount','date_paid')
			    ->execute();
		}
	}

   /**
    * Create a offering model obejct using data loaded from the database
    *
    * @param array the row of data
    * @return void
    */
	public static function createFromRow($row)
	{
		$offering = new Offering;
		$offering->loadedFromTable = true;

		$offering->setId($row['id']);
		$offering->setAmount($row['amount']);
		$offering->setDate($row['date_paid']);

		return $offering;
	}

    /**
     * Get the church offering record with the given id
     *
     * @param int the id
     * @return Offering  the church offering record associated with this id
     */
	public static function find($id)
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('offerings')->where('id')->match($id)->getRows();

		if(count($rows) > 0){
			return Offering::createFromRow($rows[0]);
		}
		else{
			return null;
		}
	}
    
    /**
     * Get all offering recordings
     *
     * @return array a collection of all offering records as Offering models
     */
	public static function findAll()
	{
		$offerings = [];

		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('offerings')->orderBy('date_paid','DESC')->getRows();

		if(count($rows) > 0){
			foreach($rows as $row){
				$offerings[] = Offering::createFromRow($row);
			}
		}

		return $offerings;
	}

}