<?php
class FinancialRecord
{
    /*
    |--------------------------------------------------------------------------
    | Financial Record model
    |--------------------------------------------------------------------------
    |
    | This class represents a model of a record of a financial transaction by the
    | church be it income or expenditure
    |
    */

    // the id of this financial record
    protected $id;

    // the monetary amount involved
    protected $amount;

    // type of record whether income or expenditure
    protected $type;

    // date this transaction was recorded
    protected $dateRecorded;

    // the description associated with this record
    protected $description;

    // whether this record model was loaded from the database
    // or it is new
    protected $loadedFromTable = false;

    /**
     * Set the id for this financial record
     * 
     * @param int the id 
     * @return void
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Get the id of this financial record
     *
     * @return int $id the id of this financial record
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *  Set the amount involved in this record
     *
     * @param double amount the amount 
     * @return void
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    /**
     * Get the monetary amount involved with this record
     *
     * @return double the amount involved
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set the type for this financial record
     *
     * @param string the type of this financial record
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Get the type of this financial record
     *
     * @return string type the type of this financial record
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the date this financial record was made
     *
     * @param string the date
     * @return void
     */
    public function setDateRecorded($date)
    {
        $this->dateRecorded =  $date;
    }

    /**
     * Get this financial record was made
     *
     * @return string the date this recording was made
     */
    public function getDateRecorded()
    {
        return $this->dateRecorded;
    }

    /**
     * Set the desccription for this financial record
     *
     * @param string the description
     * @return void
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get the description for this financial record
     *
     * @return string description
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Commit changes in the properties of this model to database
     *
     * @return Announcement the announcement object
     */
    public function save()
    {
        if($this->loadedFromTable){
            // existing model so we do an update
            $dbman = new DBUpdateManager();

            $dbman->in('church_finances')->update('amount','type','date_recorded','description')
              ->with($this->amount,$this->type,$this->dateRecorded,
                $this->description)->where('id')->match($this->id)->execute();
        }
        else{
            // we are saving a new record
            $dbman = new DBInsertManager();

            $dbman->into('church_finances')->insert($this->amount,$this->type,$this->dateRecorded,
                $this->description)->fields('amount','type','date_recorded','description')
                ->execute();
        }
    }

    /**
     * Delete this financial record from the database
     *
     * @return void
     */
    public function delete()
    {
        $dbman = new DBDeleteManager();
        $dbman->from('church_finances')->where('id')->match($this->id)->delete();
    }

    /**
     * Create a new financial record model object using a row of data from
     * the database
     *
     * @return FinancialRecord $record the record
     */
    public static function createFromRow($row)
    {
        $record = new FinancialRecord();

        $record->loadedFromTable = true;
        $record->setId($row['id']);
        $record->setDateRecorded($row['date_recorded']);
        $record->setAmount($row['amount']);
        $record->setDescription($row['description']);
        $record->setType($row['type']);

        return $record;
    }

    /**
     * Get financial records by month as an associative array
     *
     * @return array $records
     */
    public static function getRecordsByMonth($type)
    {
        $dbman = new DBSelectManager();

        $rows = $dbman->select('*')->from('church_finances')->where('type')->match($type)->getRows();

        $months = ['Jan','Feb','Mar','Apr','May','Jun','July','Aug','Sept','Oct','Nov','Dec'];

        $records = [];

        foreach($months as $month){
        	$records[$month] = 0;
        }

        foreach($months as $month){
        	foreach($rows as $row){
        		$date = new PrettyDate($row['date_recorded']);

        		if($date->getMonthName() == $month && $date->getYear() == date('Y')){
        			$records[$month] += (float)$row['amount'];
        		}
        	}
        }

        return $records;
    }

    /**
     * Get the financial record represented by the given id
     *
     * @param int id the id of this financial record
     * @return FinancialRecord the financial record
     */
    public static function find($id)
    {
        $dbman = new DBSelectManager();

        $rows = $dbman->select('*')->from('church_finances')->where('id')->match($id)
                ->getRows();

        if(count($rows)){
            return FinancialRecord::createFromRow($rows[0]);
        }
        else{
            return null;
        }
    }

    /**
     * Get all the financial records
     *
     * @return array $records all church financial records as model objects 
     */ 
    public static function findAll()
    {
        $dbman = new DBSelectManager();
        $records = [];

        $rows = $dbman->select('*')->from('church_finances')->orderBy('date_recorded','DESC')->getRows();

        if(count($rows) > 0){
            foreach($rows as $row){
                $records[] = FinancialRecord::createFromRow($row);
            }
        }

        return $records;
    }
}
?>