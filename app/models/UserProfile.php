<?php
require('app/models/Child.php');

class UserProfile 
{
	protected $userId;

	protected $firstname;

	protected $lastname;

	protected $othernames;

	protected $fullname;

	protected $title;

	protected $gender;

	protected $maritalStatus;

	protected $dateOfBirth;

	protected $dateJoined;

	protected $dateDeparted;

	protected $spouse;

	protected $father;

	protected $mother;

	protected $occupation;

	protected $churchPosition;

	protected $workPhone;

	protected $homePhone;

	protected $cellPhone;

	protected $emergencyContact;

	protected $email;

	protected $houseAddress;

	protected $city;
    
    protected $dateLastEdited;

    protected $objectType;

    protected $imageUrl;

    protected $children = [];

    protected $loadedFromTable = false;

    protected $status;

    public function __construct()
    {
    	$this->setChildren();
    }

    public function setUserId($id)
    { 
    	$this->userId = $id;
    }
    public function getUserId()
    {
    	return $this->userId;
    }

	public function setFirstname($name)
	{
		$this->firstname = $name;
	}

	public function getFirstname()
	{
		return $this->firstname;
	}

	public function setLastname($lastname)
	{
		$this->lastname = $lastname;
	}

	public function getLastname()
	{
		return $this->lastname;
	}

	public function setOthernames($othername)
	{
		$this->othernames = $othername;
	}

	public function getOthernames()
	{
		return $this->othernames;
	}

	public function getFullname()
	{
        return $this->firstname . " " . $this->othernames . " " .$this->lastname;
	}

	public function setTitle($title)
	{
		$this->title =  $title;
	}

	public function getTitle()
	{
		return $this->title;
	}

	public function setGender($gender)
	{
		$this->gender = $gender;
	}

	public function getGender()
	{
		if($this->gender == "NOT SET YET")
		    return "Not Set Yet";
		else
			return capitalise($this->gender);
	}

	public function setMaritalStatus($status)
	{
		$this->maritalStatus = $status;
	}

	public function getMaritalStatus()
	{
		if($this->maritalStatus == "NOT SET YET")
		    return "Not Set Yet";
		else
			return capitalise($this->maritalStatus);
	}

	public function setDateOfBirth($date)
	{
		$this->dateOfBirth = $date;
	}

	public function getDateOfBirth()
	{
		return $this->dateOfBirth;
	}

	public function setDateJoined($date)
	{
		$this->dateJoined = $date;
	}

	public function getDateJoined()
	{
		return $this->dateJoined;
	}

	public function setDateDeparted($date)
	{
		$this->dateDeparted = $date;
	}

	public function getDateDeparted()
	{
		return $this->dateDeparted;
	}

	public function setSpouse($spouse)
	{
		$this->spouse = $spouse;
	}

	public function getSpouse()
	{
		return $this->spouse == ""? "Not Set Yet" : $this->spouse;
	}

	public function setFather($father)
	{
		$this->father = $father;
	}

	public function getFather()
	{
		return $this->father == "" ? null : $this->father;
	}

	public function setMother($mother)
	{
		$this->mother = $mother;
	}

	public function getMother()
	{
		return $this->mother == "" ? null : $this->mother;
	}

	public function setOccupation($occupation)
	{
		$this->occupation = $occupation;
	}

	public function getOccupation()
	{
		return $this->occupation == "" ? "Not Set Yet" : $this->occupation;
	}

	public function setChurchPosition($position)
	{
		$this->churchPosition = $position;
	}

	public function getChurchPosition()
	{
		return $this->churchPosition == "" ? "None" : $this->churchPosition;
	}

	public function setWorkPhone($phone)
	{
		$this->workPhone = $phone;
	}

	public function getWorkPhone()
	{
		return $this->workPhone == "" ? "Not Set Yet" : $this->workPhone;
	}

	public function setHomePhone($phone)
	{
		$this->homePhone = $phone;
	}

	public function getHomePhone()
	{
		return $this->homePhone  == "" ? "Not Set Yet" : $this->homePhone;;
	}

	public function setCellPhone($phone)
	{
		$this->cellPhone = $phone;
	}

	public function getCellPhone()
	{
		return $this->cellPhone == "" ? "Not Set Yet" : $this->cellPhone;
	}

	public function setEmergencyContact($phone)
	{
		$this->emergencyContact = $phone;
	}

	public function getEmergencyContact()
	{
		return $this->emergencyContact == "" ? "Not Set Yet" : $this->emergencyContact;
	}

	public function setEmail($email)
	{
		$this->email = $email;
	}

	public function getEmail()
	{
		return $this->email == "" ? "Not Set Yet" : $this->email;
	}

	public function setHouseAddress($address)
	{
		$this->houseAddress = $address;
	}

	public function getHouseAddress()
	{
		return $this->houseAddress == "" ? "Not Set Yet" : $this->houseAddress;
	}

	public function setCity($city)
	{
		$this->city = $city;
	}

	public function getCity()
	{
		return $this->city == "" ? "Not Set Yet" : $this->city;
	}

	public function setDateLastEdited($date)
	{
		$this->dateLastEdited = $date;
	}

	public function getDateLastEdited()
	{
		return $this->dateLastEdited;
	}

	public function setImageUrl($url)
	{
		$this->imageUrl = $url;
	}

	public function getImageUrl()
	{
		$imgFile = 'resources/uploads/profile_photos/'.$this->userId.'.jpg';
                
        if( !file_exists( $imgFile ) ){
            if($this->gender == 'FEMALE')
                return "/resources/images/female.png";
            else
                return "/resources/images/male.png";
        }
        else{
        	return $this->imageUrl;
        }
	}

	public function setChildren()
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select("*")->from("children")->where('father_id','mother_id')->match($this->userId,$this->userId)->useOperator("OR")->getRows();

		if(count($rows) > 0){
			foreach($rows as $row){
				$this->children[] = UserProfile::createProfileFromRow($row);
			}
		}
	}

	public function getChildren()
	{
		return $this->children;
	}

	public function hasFamily()
	{
		if($this->getSpouse() == "Not Set Yet" && $this->getFather() == null && $this->getMother() == null && count($this->children) == 0)
			return false;

		return true;
	}

	public function setStatus()
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select('status')->from('users')->where('id')->match($this->userId)
		        ->getRows();

		if(count($rows)){
			$status = $rows[0]['status'];
			$this->status = $status;
		}
	}

	public function isActive()
	{
		return ($this->status == 'ACTIVE');
	}

	public function save()
	{
		$dbman = new DBInsertManager();
		if($this->loadedFromTable){
            $dbman = new DBUpdateManager();

            $dbman->in('user_profiles')->update('user_id','firstname','othernames',
            	'lastname',
            	'user_title','date_of_birth','date_joined','date_departed','gender',
            	'marital_status','spouse_name',
            	'occupation','position_in_church','home_phone','work_phone','cell_phone','emergency_contact',
            	'email','date_last_edited','address','city','image_url')->with($this->userId,$this->firstname,$this->othernames,
            	$this->lastname,$this->title,$this->dateOfBirth,$this->dateJoined,$this->dateDeparted,
            	$this->gender,$this->maritalStatus,$this->spouse,$this->occupation,$this->churchPosition,$this->homePhone,
            	$this->workPhone,$this->cellPhone,$this->emergencyContact,$this->email,$this->dateLastEdited,
            	$this->houseAddress,$this->city,$this->imageUrl)->where('user_id')->match($this->userId)->execute();

            if($this->father != null || $this->mother != null){
            	$child = Child::find($this->userId);

            	if($child != null){           		
            		$child->setUserId($this->userId);
            		$child->setFather($this->father);
            		$child->setMother($this->mother);

            		$child->save();
            	}
            	else{
            		$child = new Child();
            		$child->setUserId($this->userId);
            		$child->setFather($this->father);
            		$child->setMother($this->mother);

            		$child->save();

            	}
            }
		}
		else{
            $dbman->into('user_profiles')->insert($this->userId, $this->firstname, $this->othernames,
            	$this->lastname,$this->title, $this->dateOfBirth, $this->dateJoined, $this->dateDeparted,
            	$this->gender, $this->maritalStatus, $this->spouse, $this->occupation, $this->churchPosition,$this->homePhone,
            	$this->workPhone, $this->cellPhone, $this->emergencyContact, $this->email, $this->dateLastEdited,
            	$this->houseAddress, $this->city,$this->imageUrl)->fields('user_id','firstname','othernames',
            	'lastname',
            	'user_title','date_of_birth','date_joined','date_departed','gender',
            	'marital_status','spouse_name',
            	'occupation','position_in_church','home_phone','work_phone','cell_phone','emergency_contact',
            	'email','date_last_edited','address','city','image_url')->execute();

            if($this->father != null || $this->mother != null){
            	$child = new Child();
            	print_r("Heya");
            	exit;
                $child->setUserId($this->userId);
            	$child->setFather($this->father);
            	$child->setMother($this->mother);

            	$child->save();

            }
		}
	}


	public static function createProfileFromRow($profileRow)
	{
		$profile = new UserProfile();

		$profile->loadedFromTable = true;

		$profile->setUserId($profileRow['user_id']);

		$profile->setFirstname($profileRow['firstname']);

		$profile->setOthernames($profileRow['othernames']);

		$profile->setLastname($profileRow['lastname']);

		$profile->setTitle($profileRow['user_title']);

		$profile->setDateOfBirth($profileRow['date_of_birth']);

		$profile->setDateJoined($profileRow['date_joined']);

        $profile->setDateDeparted($profileRow['date_departed']);

        $profile->setGender($profileRow['gender']);

        $profile->setMaritalStatus($profileRow['marital_status']);

        $profile->setSpouse($profileRow['spouse_name']);

        $child = Child::find($profileRow['user_id']);

        if($child != null){
            $profile->setFather($child->getFather());
            $profile->setMother($child->getMother());
        }else{
        	$profile->setFather(null);
        	$profile->setMother(null);
        }
        

        $profile->setOccupation($profileRow['occupation']);

        $profile->setChurchPosition($profileRow['position_in_church']);

        $profile->setHomePhone($profileRow['home_phone']);

        $profile->setWorkPhone($profileRow['work_phone']);

        $profile->setCellPhone($profileRow['cell_phone']);

        $profile->setEmergencyContact($profileRow['emergency_contact']);

        $profile->setEmail($profileRow['email']);

        $profile->setDateLastEdited($profileRow['date_last_edited']);

        $profile->setHouseAddress($profileRow['address']);

        $profile->setCity($profileRow['city']);

        $profile->setImageUrl($profileRow['image_url']);

        $profile->setStatus();


        return $profile;
	}

	public static function find($id)
	{
		$dbman = new DBSelectManager();
		$rows = $dbman->select("*")->from('user_profiles')->where('user_id')->match($id)->getRows();

		if(count($rows) > 0){
            $profile = $rows[0];
            return UserProfile::createProfileFromRow($profile);
        }
        else{
        	return null;
        }
	}

	public static function findAll()
	{
		$dbman = new DBSelectManager();

		$queryObj = $dbman->select('*')->from('user_profiles')->orderBy('lastname','ASC');

		$args = func_get_args();

		if(isset($args[0]) && isset($args[1])){
			$queryObj = $queryObj->limit($args[0],$args[1]);
		}

		$rows = $queryObj->getRows();

	
		$members = [];

		if(count($rows) > 0){
			foreach($rows as $row){
				$members[] = UserProfile::createProfileFromRow($row);
			}
		}

		return $members;
	}

	public static function findByStatus($status)
	{
		$dbman = new DBSelectManager();

		$queryObj = $dbman = new DBSelectManager();
		$dbman->select('*')->from('users')->join('user_profiles')->on("users.id = user_profiles.user_id AND users.status = '$status' ")->
		orderBy('lastname','ASC');

		$args = func_get_args();

		if(isset($args[1]) && isset($args[2])){
			$queryObj = $queryObj->limit($args[1],$args[2]);
		}

		$rows = $queryObj->getRows();

	
		$members = [];

		if(count($rows) > 0){
			foreach($rows as $row){
				$members[] = UserProfile::createProfileFromRow($row);
			}
		}
        
		return $members;
	}



	public static function findByFullname($name)
	{
		$name = explode(' ',$name);

	    $firstname = $name[0];
	    $lastname =  $name[count($name) - 1];

	    $others = "";

	    for($i = 1;$i < count($name) - 1;$i++)
		$others .= $name[$i] . " ";

		$dbman = new DBSelectManager();
		$rows = $dbman->select('*')->from('user_profiles')->where('firstname','othernames','lastname')
		      ->match($firstname,$others,$lastname)->getRows();

		if(count($rows) > 0){
		    $userRow = $rows[0];
		    return UserProfile::createProfileFromRow($userRow);
		}
        
        else{
        	return null;
        }
	}

	public static function findWhereNameLike($searchStr)
	{
	    $dbman = new DBSelectManager();
	    $members = [];

	    $rows = $dbman->manual("SELECT * FROM user_profiles WHERE firstname LIKE ? OR othernames
	    	 LIKE ? OR lastname LIKE ?")->bindValues($searchStr.'%',$searchStr.'%',$searchStr.'%')->getRows();

	    if(count($rows)){
	    	foreach($rows as $row){
	    		$members[] = UserProfile::createProfileFromRow($row);
	    	}
	    }

	    return $members;
	}

	public static function getPopulation()
	{
		$dbman= new DBSelectManager();

		$rows = $dbman->select("COUNT(id) AS count")->from('user_profiles')->getRows();

		return $rows[0]['count'];
	}

	public static function getPopulationByStatus($status)
	{
		$dbman= new DBSelectManager();

		$rows = $dbman->select("COUNT(id) AS count")->from('user_profiles')->getRows();

		return $rows[0]['count'];
	}
}
?>