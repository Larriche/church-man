<?php

class Project 
{
	protected $id;

	protected $name;

	protected $estimatedCost;

	protected $dateCommenced;

	protected $dateCompleted;

	protected $description;

	protected $amountRaised;

	protected $status;

	protected $loadedFromTable = false;

    
    public function setId($id)
    {
    	$this->id = $id;
    }

    public function getId()
    {
    	return $this->id;
    }

	public function setName($name)
	{
		$this->name = $name;
	}

	public function getName()
	{
		return $this->name;
	}

	public function setEstimatedCost($cost)
	{
		$this->estimatedCost = $cost;
	}

	public function getEstimatedCost()
	{
		return $this->estimatedCost;
	}

	public function setDateCommenced($date)
	{
		$this->dateCommenced = $date;
	}

	public function getDateCommenced()
	{
        return $this->dateCommenced;
	}

	public function setDateCompleted($date)
	{
		$this->dateCompleted = $date;
	}

	public function getDateCompleted()
	{
		return $this->dateCompleted;
	}

    public function setDescription($description)
    {
    	$this->description = $description;
    }

    public function getDescription()
    {
    	return $this->description;
    }

    public function setAmountRaised($amount)
    {
    	$this->amountRaised = $amount;
    }

    public function getAmountRaised()
    {
    	return $this->amountRaised;    
    }

    public function setStatus($status)
    {
    	$this->status = $status;
    }

    public function getStatus()
    {
    	return $this->status;
    }

    public function getPledges()
    {
        $dbman = new DBSelectManager();
        $pledges = [];

        $rows = $dbman->select('*')->from('pledges')->where('project_id')->match($this->id)->getRows();

        if(count($rows) > 0){
            foreach($rows as $row){
                $pledges[] = Pledge::createFromRow($row);
            }
        }

        return $pledges;

    }

    public function save()
    {
        if($this->loadedFromTable){
            $dbman = new DBUpdateManager();
            $dbman->in('projects')->update('project_name','description','date_commenced','estimated_cost','status','amount_raised')->with($this->name,$this->description,$this->dateCommenced,$this->estimatedCost,$this->status,$this->amountRaised)->where('id')->match($this->id)->execute();	
        }
        else{
        	$this->status = "UNCOMPLETED";
        	$this->amountRaised = 0;

        	$dbman = new DBInsertManager();
        	$dbman->into('projects')->insert($this->name,$this->description,$this->dateCommenced,$this->estimatedCost,$this->status,$this->amountRaised)
        	      ->fields('project_name','description','date_commenced','estimated_cost','status','amount_raised')->execute();
        }
    }

    public static function createFromRow($row)
    {
        $project = new Project;

        $project->loadedFromTable = true;
        
        $project->setId($row['id']);
        $project->setName($row['project_name']);
        $project->setDescription($row['description']);
        $project->setDateCommenced($row['date_commenced']);
        $project->setAmountRaised($row['amount_raised']);
        $project->setEstimatedCost($row['estimated_cost']);
        $project->setStatus($row['status']);
        
        return $project;
    }

    public static function findAll()
    {
        $projectObjs = [];

        $dbman = new DBSelectManager();
        $rows = $dbman->select("*")->from('projects')->getRows();

        if(count($rows) > 0){
            foreach($rows as $row){
                $projectObjs[] = Project::createFromRow($row);
            }
        }

        return $projectObjs;
    }

    public static function find($id)
    {
        $dbman = new DBSelectManager();
        $rows = $dbman->select('*')->from('projects')->where('id')->match($id)->getRows();

        $project = Project::createFromRow($rows[0]);

        return $project;
    }

    public static function getCompleted()
    {
        $dbman = new DBSelectManager();

        $rows = $dbman->select('*')->from('projects')->where('status')->match('COMPLETED')->getRows();

        $projectObjs = [];

        if(count($rows) > 0){
            foreach($rows as $row){
                $projectObjs[] = Project::createFromRow($row);
            }
        }

        return $projectObjs;
    }

    public static function getUncompleted()
    {
        $dbman = new DBSelectManager();

        $rows = $dbman->select('*')->from('projects')->where('status')->match('UNCOMPLETED')->getRows();

        $projectObjs = [];

        if(count($rows) > 0){
            foreach($rows as $row){
                $projectObjs[] = Project::createFromRow($row);
            }
        }

        return $projectObjs;
    }

    public static function supplyFunds($id,$amount)
    {
        $project = Project::find($id);
        $project->setAmountRaised($project->getAmountRaised() + $amount);
        $project->save();
    }


}