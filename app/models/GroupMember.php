<?php
class GroupMember
{
	 /*
    |--------------------------------------------------------------------------
    | Church Group Member Model
    |--------------------------------------------------------------------------
    |
    | This class represents a model of a member of a church group
    |
    */

    // the id of the group
	protected $groupId;

    // the id of the church member
	protected $userId;

    // the date the member joined the group
	protected $memberSince;

    // the role of the church memeber in the group
	protected $role;

    // determine whether this instance was created using already existing data
	public $loadedFromTable = False;

    
    /**
     * Set the id of this group member - church member association
     *
     * @param int the id
     * @return void
     */
	public function setGroupId($id)
	{
		$this->groupId = $id;
	}

    /**
     * Return the id of the group involved in this association
     *
     * @return the id of the group
     */
	public function getGroupId()
	{
		return $this->groupId;
	}

    /**
     * Set the id of the user involved in this association
     *
     * @param int the id
     * @return void
     */
	public function setUserId($id)
	{
		$this->userId = $id;
	}

    /**
     * Get the id of the user involved in this association
     *
     * @param the user id
     * @return void
     */
	public function getUserId()
	{
		return $this->userId;
	}

    /**
     * Set the date the church member joined this church group
     *
     * @param string the date
     * @return void
     */
	public function setDateJoined($date)
	{
		$this->memberSince = $date;
	}

    /**
     * Get the date the church member joined this church group
     *
     * @return string the date
     */
	public function getDateJoined()
	{
		$date = new PrettyDate($this->memberSince);
		return $date->getReadable();
	}

    /**
     * Set the role of the church member in this group
     *
     * @param string the role of the member in this group
     * @return void
     */
	public function setRole($role)
	{
		$this->role = $role;
	}

    /**
     * Get the role of the church member in this church group
     *
     * @return string the role of the church member
     */
	public function getRole()
	{
		return $this->role;
	}
    
    /**
     * Commit changes in this model instance to the database
     *
     * @return void
     */
	public function save()
	{
		if($this->loadedFromTable){
			// do update
		}
		else{
			$dbman = new DBInsertManager();
			$dbman->into('group_members')->insert($this->userId,$this->groupId,$this->role,$this->memberSince)
			     ->fields('member_id','group_id','member_role','joined_date')->execute();
		}
	}

    /**
     * Create a model using data obtained from the database
     *
     * @param a row of data from the database
     * @return GroupMember $groupMember a group member model instance
     */
	public static function createFromRow($row)
	{
		$groupMember =  new GroupMember();
		$groupMember->setUserId($row['id']);
		$groupMember->setUserId($row['member_id']);
		$groupMember->setDateJoined($row['joined_date']);
		$groupMember->setGroupId($row['group_id']);
		$groupMember->setRole($row['member_role']);

		$groupMember->loadedFromTable = True;

		return $groupMember;
	}

    /**
     * Search a given group for a given user
     *
     * @param int $groupId the id of the group to search
     * @param int $userId the id of the church member to search for
     * @return boolean whether member with the given id exists in the given group
     */
	public static function memberExists($groupId,$userId)
	{
		$dbman = new DBSelectManager();
		$rows = $dbman->select('*')->from('group_members')->where('group_id' , 'member_id')
		    ->match($groupId,$userId)->getRows();

        if(count($rows) > 0)
        	return true;
        
        return false;
	}

    /**
     * Get a group member model with the given group id and church member id combination
     *
     * @param int $memberId the id of the church member
     * @param int $groupId the id of the group
     * @return GroupMember $groupMember the group member with the given combination
     */
	public static function find($memberId,$groupId)
	{
		$dbman = new DBSelectManager();
		$rows = $dbman->select('*')->from('group_members')->where('group_id' , 'member_id')
		    ->match($groupId,$memberId)->getRows();

        if(count($rows) > 0){
        	$groupMember = GroupMember::createFromRow($rows[0]);
        	return $groupMember;
        }

        return null;
	}	

}