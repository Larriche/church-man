<?php
class Announcement
{
	/*
    |--------------------------------------------------------------------------
    | Church Announcement Model
    |--------------------------------------------------------------------------
    |
    | This class represents a Church announcement.Through this class,you interact 
    | with announcements table and other dependent tables to get data about a 
    | church announcement
    | 
    |
    */
 
    // the id of the announcement
	protected $id;

    // title of the announcement
	protected $title;

    // message or content of the announcement
	protected $message;

    // date announcement was made
	protected $date;

    // expiry date for the announcement
	protected $expiryDate;

    // determines whether the current announcement instance was
    // created by loading already existing data from database
	protected $loadedFromTable = false;

    
    /**
     * Set the id for the announcement instance using data from database
     *
     * @return void
     */
	public function setId($id)
	{
		$this->id = $id;
	}

    /**
     * Get the id of the announcement instance
     *
     * @return int  the id of this announcement as gotten from database
     */
	public function getId()
	{
		return $this->id;
	}

    /**
     * Set the title of the announcement instance
     *
     * @return void
     */
	public function setTitle($title){
		$this->title = $title;
	}

    /**
     * Get the title of the announcement instance
     *
     * @return string the title of this announcement
     */
	public function getTitle()
	{
		return $this->title;
	}

    /**
     * Set the message content of the announcement instance
     *
     * @return void
     */
	public function setMessage($message)
	{
		$this->message = $message;
	}

    /**
     * Get the message content of the announcement instance
     *
     * @return string the message of the announcement
     */
	public function getMessage()
	{
		return $this->message;
	}

    /**
     * Set the date of creation for the announcement instance
     *
     * @return void
     */
	public function setDate($date)
	{
		$this->date = $date;
	}

    /**
     * Get the date of creation for the announcement instance
     *
     * @return string the date as obtained from the database
     */
	public function getDate()
	{
		return $this->date;
	}

    /**
     * Set the expiry date for the announcement instance
     *
     * @return void
     */
	public function setExpiryDate($date)
	{
		$this->expiryDate = $date;
	}

    /**
     * Get the expiry date for the announcement instance
     *
     * @return string the date as obtained from the database
     */
	public function getExpiryDate()
	{
		return $this->expiryDate;
	}

    /**
     * Save or update the announcement instance by persisting its 
     * properties to database
     *
     * @return void
     */
	public function save()
	{
		if($this->loadedFromTable){
            $dbman = new DBUpdateManager();

            $dbman->in('announcements')->update('title','message','date_made','expiry_date')->with($this->title,
            	$this->message,$this->date,$this->expiryDate)->where('id')->match($this->id)->execute();
		}
		else{
			$dbman = new DBInsertManager();

			$dbman->into('announcements')->insert($this->title,$this->message,$this->date,$this->expiryDate)->fields('title','message','date_made','expiry_date')->execute();
		}
	}

    /**
     * Delete the announcement from the database
     *
     * @return void
     */
    public function delete()
    {
        $dbman = new DBDeleteManager();
        $dbman->from('announcements')->where('id')->match($this->id)->delete();
    }

    /**
     * Create an announcement model object when given a row from the announcements table
     *
     * @return Announcment $announcement the announcement object
     */
	public static function createFromRow($row)
	{
		$announcement = new Announcement;
        
        $announcement->loadedFromTable = true;

        $announcement->setId($row['id']);
		$announcement->setTitle($row['title']);
		$announcement->setMessage($row['message']);
		$announcement->setDate($row['date_made']);
		$announcement->setExpiryDate($row['expiry_date']);

		return $announcement;
	}

    /**
     * Get all announcements from the database and return them as 
     * announcement objects
     *
     * @return array all announcment objects
     */
	public static function findAll()
	{
		$announcementObjs = [];

		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('announcements')->getRows();

		if(count($rows) > 0){
			foreach($rows as $row)
				$announcementObjs[] = Announcement::createFromRow($row);
		}

		return $announcementObjs;
	}

    /**
     * Get the announcement represented by the given id
     * as an announcement object
     *
     * @return Announcement the announcement object
     */
	public static function find($id)
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select("*")->from('announcements')->where('id')->match($id)->getRows();

		if(count($rows) > 0){
			return Announcement::createFromRow($rows[0]);
		}

		return null;
	}
}
?>