<?php
require('app/models/GroupRequest.php');

class Group
{
    /*
    |--------------------------------------------------------------------------
    | Church Group Model
    |--------------------------------------------------------------------------
    |
    | This class represents a Church Group.Through this class,you interact with 
    | groups table and other dependent tables to get data about a church group
    | 
    | 
    |
    */
    
    // the id of the group
	protected $id;

    // the name of the group
	protected $name;

    // the description of the group
	protected $description;

    // the date the group was created
    protected $dateCreated;

    // the date the group was dissolved
    protected $dateDissolved;

    // status of the group
    protected $status;

    // type of the group whether 'PUBLIC' or 'PRIVATE'
    protected $type;

    // determines whether our Group instance was created by loading an existing 
    // group form the database or it was created otherwise
    protected $loadedFromTable = False;

    
    /**
     * Set the id for the Group object
     *
     * @param int the id
     * @return void
     */
    public function setId($id)
    {
    	$this->id = $id;
    }

    /**
     * Get the id of this Group instance
     *
     * @return int the id
     */
    public function getId()
    {
    	return $this->id;
    }

    /**
     * Set group name for this Group instance
     *
     * @param string the name of the group
     * @return void
     */
    public function setName($name)
    {
    	$this->name = $name;
    }

    /**
     * Get the group name for this Group instance
     *
     * @return string the name
     */
    public function getName()
    {
    	return $this->name;
    }

    /**
     * Set the description for this group instance
     *
     * @param string the description
     * @return void
     */
    public function setDescription($description)
    {
    	$this->description = $description;
    }


    /**
     * Get the description for this Group instance
     *
     * @return string the description
     */
    public function getDescription()
    {
    	return $this->description;
    }

    /**
     * Set the date created for this Group instance
     *
     * @param string the date in PHP date format
     * @return void
     */
    public function setDateCreated($date)
    {
    	$this->dateCreated = $date;
    }

    /**
     * Get the date the church group represented by this
     * group instance was created
     *
     * @return string the date in human readable form
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set the date the church group represented by this
     * group instance was dissolved
     *
     * @param string the date in PHP date format
     * @return void
     */
    public function setDateDissolved($date)
    {
    	$this->dateDissolved = $date;
    }

    /**
     * Get the date the church group represented by this 
     * instance was dissolved
     *
     * @return string the date in human readable form
     */
    public function getDateDissolved()
    {
    	return $this->dateDissolved;
    }

    /**
     * Set the status of the group represented by this Group
     * instance whether active or dissolved
     *
     * @param string the status
     * @return void
     */
    public function setStatus($status)
    {
    	$this->status = $status;
    }

    /**
     * Get the status of the group represented by this Group
     * instance
     *
     * @return string the status of the group
     */
    public function getStatus()
    {
    	return $this->status;
    }

    /**
     * Set the type of group represented by this Group instance
     * whether public or private 
     *
     * @param string the type 
     * @return void
     */
    public function setType($type)
    {
    	$this->type = $type;
    }

    /**
     * Get the type of the group represented by this Group instance
     * whether public or private
     *
     * @return string the type
     */
    public function getType()
    {
    	return $this->type;
    }

    /**
     * Save the properties of this group instance to the database
     * 
     *
     * @return void
     */
    public function save()
    {
    	if($this->loadedFromTable){
            // we are updating the group
        	$dbman = new DBUpdateManager();

            $dbman->in('groups')->update('group_name','description','date_created','date_dissolved','type','status')
                ->with($this->name,$this->description,$this->dateCreated,$this->dateDissolved,$this->type,$this->status)
                ->where('id')->match($this->id)->execute();
        }
        else{
            // we are inserting the group

        	$this->status = "ACTIVE";

            // create a new instance of DBInsertManager,a class that 
            // simplifies working with databse insertions
        	$dbman = new DBInsertManager();

        	$dbman->into('groups')->insert($this->name,$this->description,$this->dateCreated,$this->type,$this->status)
        	      ->fields('group_name','description','date_created','type','status')->execute();
        }
    }

    /**
     * Delete this group instance from the database
     * 
     *
     * @return void
     */
    public function delete()
    {
        $dbman = new DBDeleteManager();
        $dbman->from('groups')->where('id')->match($this->id)->delete();
    }

    /**
     * Creates a new Group instance by using data from a row in the 
     * groups table of the database
     *
     * @param the row of data gotten from the database
     * @return Group group   a Group model created using loaded data
     */
    public static function createFromRow($groupRow)
    {
        $group = new Group();

        $group->setId($groupRow['id']);

        $group->setName($groupRow['group_name']);

        $group->setDescription($groupRow['description']);

        $group->setDateCreated($groupRow['date_created']);

        $group->setDateDissolved($groupRow['date_dissolved']);

        $group->setType($groupRow['type']);

        $group->setStatus($groupRow['status']);

        $group->loadedFromTable = True;

        return $group;
    }

    /**
     * Get an array of members belonging to the group represented
     * by this Group instance
     *
     * @return array  an array of GroupMember objects
     */
    public function getMembers()
    {
        // create a new instance of the DBSelectManager
        // a custom class for handling selections from the
        // database
        $dbman = new DBSelectManager();
        $rows = $dbman->select('*')->from('group_members')->where('group_id')->match($this->id)
             ->getRows();

        $memberObjs = [];

        foreach($rows as $row)
            $memberObjs[] = GroupMember::createFromRow($row);

        return $memberObjs;
    }

    /**
     * Get an array of leader(s) of this group 
     *
     * @return array  an array of GroupMember objects
     */
    public function getLeaders()
    {
        $members = $this->getMembers();
        $leaders = [];

        foreach($members as $member)
        {
            if($member->getRole() == 'LEADER'){
                $leaders[] = $member->getUserId();
            }
        }

        return $leaders;
    }
    
    /**
     * Search for a church member with a given id among the members of this group
     *
     * @param int $id the id of the church member
     * @return boolean whether the church member belongs to this group
     */
    public function searchMember($id)
    {
        $dbman = new DBSelectManager();

        $rows = $dbman->select("id")->from("group_members")->where('member_id','group_id')
                ->match($id,$this->id)->getRows();

        if(count($rows) > 0){
            return true;
        }
        else{
            return false;
        }
    }
    
    /**
     * Determine whether the church member with the given id has requested to join this group
     *
     * @param int the id
     * @return boolean whether the given id has requested membership
     */
    public function hasRequestFrom($userId)
    {
        $dbman = new DBSelectManager();

        $rows = $dbman->select('id')->from('group_join_requests')->where('user_id','group_id')
           ->match($userId,$this->id)->getRows();

        if(count($rows) > 0){
            return true;
        }

        return false;
    }

    /**
     * Get the membership requests for this group
     * 
     * @return array the membership requests for this group
     */
    public function getRequests()
    {
        $requests = [];

        $dbman = new DBSelectManager();

        $rows = $dbman->select('*')->from('group_join_requests')->where('group_id')
            ->match($this->id)->getRows();

        if(count($rows) > 0){
            foreach($rows as $row){
                $requests[] = GroupRequest::createFromRow($row);
            }
        }

        return $requests;
    }

    /**
     * Create a new Group instance by retrieving group with given id from the
     * database , converting it to a Group object and return it
     *
     * @param id int id of group to load from the database
     * @return Group group whose id is the given id
     */
    public static function find($id)
    {
        $dbman = new DBSelectManager();
        $rows = $dbman->select('*')->from('groups')->where('id')->match($id)->getRows();

        $group = Group::createFromRow($rows[0]);

        return $group;
    }

    /**
     * Get all group rows from the database,convert them to an array of
     * Group objects and return it
     *
     * @return array all church groups as an array of Group objects
     */
    public static function findAll()
    {
    	$groupObjs = [];

    	$dbman = new DBSelectManager();
    	$rows = $dbman->select("*")->from('groups')->getRows();

    	if(count($rows) > 0){
    		foreach($rows as $row){
    			$groupObjs[] = Group::createFromRow($row);
    		}
    	}

    	return $groupObjs;
    }

    /**
     * Get the groups the church member with th given id is part of
     *
     * @param int the id of the church member
     * @return array the collection of  the groups this member is part of
     */
    public static function getMemberGroups($id)
    {
        $dbman = new DBSelectManager();
        $memberGroups = [];
        $groupObjs = [];

        $rows = $dbman->select("*")->from('groups')->getRows();
        
        if(count($rows) > 0){
            foreach($rows as $row){
                $groupObjs[] = Group::createFromRow($row);
            }
        }

        foreach($groupObjs as $groupObj){
            if($groupObj->searchMember($id)){
                $memberGroups[] = $groupObj;
            }
        }

        return $memberGroups;
    }
}
?>