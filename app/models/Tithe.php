<?php
class Tithe
{
	protected $id;

	protected $amount;

	protected $date;

	protected $payer;

	protected $loadedFromTable = false;

	public function setId($id)
	{
		$this->id = $id;
	}

	public function getId()
	{
		return $this->id;
	}

	public function setAmount($amount)
	{
		$this->amount = $amount;
	}

	public function getAmount()
	{
		return $this->amount;
	}

	public function setDate($date)
	{
		$this->date = $date;
	}

	public function getDate()
	{
		return $this->date;
	}

	public function setPayer($id)
	{
		$this->payer = $id;
	}

	public function getPayer()
	{
		return $this->payer;
	}

	public function save()
	{
		if($this->loadedFromTable){
			$dbman = new DBUpdateManager();

			$dbman->in('tithes')->update('amount','date_paid')->with($this->amount,$this->date)
			  ->where('id')->match($this->id)->execute();
		}
		else{
			$dbman = new DBInsertManager();
		    $dbman->into("tithes")->insert($this->amount,$this->date,$this->payer)
		      ->fields('amount','date_paid','payer_id')->execute();
		}
	}

	public function delete()
	{
		$dbman = new DBDeleteManager();
		$dbman->from('tithes')->where('id')->match($this->id)->delete();
	}

	public static function createFromRow($row)
	{
		$tithe = new Tithe();

		$tithe->setId($row['id']);

		$tithe->setPayer($row['payer_id']);

		$tithe->setDate($row['date_paid']);

		$tithe->setAmount($row['amount']);

		$tithe->loadedFromTable = true;

		return $tithe;
	}

	public static function find($id)
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('tithes')->where('id')->match($id)->getRows();

		if(count($rows) > 0){
			return Tithe::createFromRow($rows[0]);
		}
	}

	public static function getMemberTitheRecords($member_id)
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select("*")->from('tithes')->where('payer_id')->match($member_id)
		 ->orderBy('date_paid','DESC')->getRows();

		$tithes = [];

		foreach($rows as $row){
			$tithes[] = Tithe::createFromRow($row);
		}

		return $tithes;
	}

	public static function getTotalsByDay()
	{
		$totals = [];

		$dbman = new DBSelectManager();

		$dateRows = $dbman->select('date_paid')->from('tithes')->orderBy('date_paid','DESC')->getRows();

		foreach($dateRows as $dateRow){
			$date = $dateRow['date_paid'];

			$totalsRows= $dbman->select('SUM(amount) as total')->from('tithes')->where('date_paid')->match($date)->getRows();
            
            if(count($totalsRows) > 0){
            	$totalAmount = $totalsRows[0]['total'];
            	$totals[$date] = $totalAmount;
            }
		}

		return $totals;
	}

	public static function getTotalsByMonth()
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('tithes')->getRows();

        $months = ['Jan','Feb','Mar','Apr','May','Jun','July','Aug','Sept','Oct','Nov','Dec'];

        $records = [];

        foreach($months as $month){
        	$records[$month] = 0;
        }
        
        foreach($months as $month){
        	foreach($rows as $row){
        		$date = new PrettyDate($row['date_paid']);

        		if($date->getMonthName() == $month && $date->getYear() == date('Y')){
        			$records[$month] += (float)$row['amount'];
        		}
        	}
        }

        return $records;
	}

	public static function getAnonymousTithes()
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('tithes')->where('payer_id')->match('NULL')->getRows();

		$tithes = [];

		if(count($rows) > 0){
			foreach($rows as $row){
				$tithes[] = Tithe::createFromRow($row);
			}
		}

		return $tithes;
	}
}
?>