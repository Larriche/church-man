<?php
class Pledge
{
	protected $id;

	protected $pledgedAmount;

	protected $amountPaid;

	protected $date;

	protected $payer;

	protected $project;

	protected $loadedFromTable = false;


    public function setId($id)
    {
    	$this->id = $id;
    }

    public function getId()
    {
    	return $this->id;
    }
    
	public function setPledgedAmount($amount)
	{
		$this->pledgedAmount = $amount;
	}

	public function getPledgedAmount()
	{
		return $this->pledgedAmount;
	}

	public function setAmountPaid($amount)
	{
		$this->amountPaid = $amount;
	}

	public function getAmountPaid()
	{
		return $this->amountPaid;
	}

	public function setDate($date)
	{
		$this->date = $date;
	}

	public function getDate()
	{
		return $this->date;
	}

	public function setPayer($id)
	{
		$this->payer = $id;
	}

	public function getPayer()
	{
		return $this->payer;
	}

	public function setProject($projectId)
	{
		$this->project = $projectId;
	}

	public function getProject()
	{
		return $this->project;
	}

	public function getPayments()
	{
		$dbman = new DBSelectManager();
		$rows = $dbman->select("*")->from("pledge_payments")->where("pledge_id")->match($this->id)
		              ->getRows();

		$pledgePayments = [];

		if(count($rows) > 0){
			foreach($rows as $row)
				$pledgePayments[] = PledgePayment::createFromRow($row);
		}

		return $pledgePayments;
	}

	

	public function save()
	{
		if($this->loadedFromTable){
			$dbman = new DBUpdateManager();

			$dbman->in("pledges")->update('amount','amount_paid','date_made','member_id','project_id')
			      ->with($this->pledgedAmount,$this->amountPaid,$this->date,$this->payer,$this->project)
			      ->where('id')->match($this->id)->execute();

		}
		else{
			$this->amountPaid = 0;

			$dbman = new DBInsertManager();
			$dbman->into("pledges")->insert($this->pledgedAmount,$this->amountPaid,$this->date,$this->payer,$this->project)
			      ->fields('amount','amount_paid','date_made','member_id','project_id')->execute();
		}
	}

	public function delete()
	{
		$dbman = new DBDeleteManager();

		$dbman->from('pledges')->where('id')->match($this->id)->delete();
	}

	public static function createFromRow($row)
	{
		$pledge = new Pledge();

        $pledge->setId($row['id']);

		$pledge->setPledgedAmount($row['amount']);

		$pledge->setAmountPaid($row['amount_paid']);

		$pledge->setDate($row['date_made']);

		$pledge->setPayer($row['member_id']);

		$pledge->setProject($row['project_id']);

		return $pledge;
	}

	public static function getMemberPledges($member_id)
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select("*")->from('pledges')->where('member_id')->match($member_id)->getRows();

		$pledges = [];

		foreach($rows as $row){
			$pledges[] = Pledge::createFromRow($row);
		}

		return $pledges;
	}

	public static function find($id)
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select("*")->from("pledges")->where("id")->match($id)->getRows();

		if(count($rows) > 0){
			$pledge = Pledge::createFromRow($rows[0]);
			$pledge->loadedFromTable = true;
			return $pledge;
		}

		return null;

	}
}
?>