<?php 
class Attendance
{
	/*
    |--------------------------------------------------------------------------
    | Church Attendance record Model
    |--------------------------------------------------------------------------
    |
    | This class represents an attendance record for an event in the church
    | 
    */

    // the id of the attendance record
	protected $id;

    // the number of people counted for this attendance
	protected $number;

    // the date the attendance was taken
	protected $date;

    // the id of the church event the attendance was recorded for
	protected $event;

    // a small note about the attendance
	protected $note;

    // determines whether this attendance instance was created by loading data
    // from database or otherwise
	protected $loadedFromTable = false;

    
    /**
     * Set the id for the attendance instance using data from database
     *
     * @return void
     */
	public function setId($id)
	{
		$this->id = $id;
	}

    /**
     * Get the id of the announcement instance
     *
     * @return int $id the id of this instance as gotten from database
     */
	public function getId()
	{
		return $this->id;
	}

    /**
     * Set the number of people counted for this attendance record
     *
     * @return void
     */
	public function setNumber($number)
	{
		$this->number = $number;
	}

    /**
     * Get the number of people counted for this attendance record
     *
     * @return int number of people counted for this attendance record
     */
	public function getNumber()
	{
		return $this->number;
	}

    /**
     * Set the date this attendance record was taken
     *
     * @return void
     */
	public function setDate($date)
	{
		$this->date = $date;
	}

    /**
     * Get the date this attendance record was taken
     *
     * @return string the date as gotten from the database
     */
	public function getDate()
	{
		return $this->date;
	}

    /**
     * Set the id of the event of this attendance record
     *
     * @return void
     */
	public function setEvent($id)
	{
		$this->event = $id;
	}

    /**
     * Get the id of the event for this attendance record
     *
     * @return int the id of the event
     */
	public function getEvent()
	{
		return $this->event;
	}

    /**
     * Set the note for this attendance record
     *
     * @return void
     */
	public function setNote($note)
	{
		$this->note = $note;
	}

    /**
     * Get the note for this attendance record
     *
     * @return string the note about this attendance record
     */
	public function getNote()
	{
		return $this->note;
	}

    /**
     * Persist the properties of this atendance to the database
     *
     * @return void
     */
	public function save()
	{
		if($this->loadedFromTable){
            // doing an update of existing attendance record
            $dbman = new DBUpdateManager();

            $dbman->in('attendance')->update('number','date_taken',
            	'note')->with($this->number,$this->date,$this->note)->
                 where('id')->match($this->id)->execute();
		}
		else{
			// saving a new attendance record for first time
			$dbman = new DBInsertManager();

			$dbman->into('attendance')->insert($this->event,$this->number,$this->date,$this->note)->fields('event_id','number','date_taken','note')->execute();
		}
	}

    /**
     * Create an attendance record object from a row in the attendance table
     *
     * @return void
     */
	public static function createFromRow($row)
	{
		$attendance = new Attendance;

		$attendance->loadedFromTable = true;

        $attendance->setId($row['id']);
        $attendance->setEvent($row['event_id']);
        $attendance->setNumber($row['number']);
        $attendance->setDate($row['date_taken']);
        $attendance->setNote($row['note']);

        return $attendance;

	}

    /**
     * Find the attendance record with the given id and return a model object created from it
     *
     * @return Attendance record model object
     */
	public static function find($id)
	{
		$dbman = new DBSelectManager();

		$rows = $dbman->select('*')->from('attendance')->where('id')->match($id)
		    ->getRows();

		if(count($rows) > 0){
			return Attendance::createFromRow($rows[0]);
		}

		return null;
	}
}
?>