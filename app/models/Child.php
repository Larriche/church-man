<?php
class Child
{
	/*
    |--------------------------------------------------------------------------
    | Child Model
    |--------------------------------------------------------------------------
    |
    | This class represents a model of a member who is a child of another member
    | 
    */

    // the id of the Child record
	protected $id;

    // the user id of the member who is linked to this Child record
	protected $userId;

    // the id of the member who is the father of this Child
	protected $father;

    // the id of the member who is the mother of this Child
	protected $mother;

    // determines whether this Child instance was created by loading
    // data from the database
	protected $loadedFromTable = false;

    /**
     * Set the id for this Child instance using data from database
     *
     * @param int the id
     * @return void
     */
	public function setId($id)
	{
		$this->id = $id;
	}

    /**
     * Get the id for this Child instance 
     *
     * @return int the id of this Child instance
     */
	public function getId()
	{
		return $this->id;
	}

    /**
     * Set the user id of member represented by this Child instance using data from database
     *
     * @param the id
     * @return void
     */
	public function setUserId($id)
	{
		$this->userId = $id;
	}

    /**
     * Get the user id of member represented by this Child instance 
     *
     * @return int the user id of this Child
     */
	public function getUserId()
	{
		return $this->userId;
	}

    /**
     * Set the id of the father of this Child instance using data from database
     *
     * @param the id of the child's father
     * @return void
     */
	public function setFather($father)
	{
		$this->father = $father;
	}

    /**
     * Get the id of the father of this Child instance 
     *
     * @return void
     */
	public function getFather()
	{
		return $this->father;
	}

    /**
     * Set the id of the mother of this Child instance using data from database
     *
     * @param int the id of the child's mother
     * @return void
     */
	public function setMother($mother)
	{
		$this->mother = $mother;
	}

    /**
     * Get the id of the mother of this Child instance 
     *
     * @return void
     */
	public function getMother()
	{
		return $this->mother;
	}


    /**
     * Persist the properties of this Child to the database
     *
     * @return void
     */
	public function save()
	{
		if($this->loadedFromTable){
			$dbman = new DBUpdateManager();

			$dbman->in('children')->update('child_id','father_id','mother_id')->with($this->userId,$this->father,
				$this->mother)->execute();
		}
		else{
			$dbman = new DBInsertManager();

			$dbman->into("children")->insert($this->userId, $this->father, $this->mother)
			      ->fields('child_id', 'father_id', 'mother_id')->execute();
		}
	}


    /**
     * Create a Child object using data gathered from database
     *
     * @param array the row of data
     * @return void
     */
    public static function createFromRow($row)
    {
    	$child = new Child();

    	$child->loadedFromTable = true;

    	$child->setId($row['id']);
    	$child->setUserId($row['child_id']);
    	$child->setFather($row['father_id']);
    	$child->setMother($row['mother_id']);

    	return $child;
    }

    /**
     * Get the Child with the given user id
     *
     * @param the id 
     * @return Child the child with the given user id
     */
	public static function find($userId)
	{
	    $dbman = new DBSelectManager();

	    $rows = $dbman->select("*")->from("children")->where("child_id")->match($userId)->getRows();

	    if(count($rows) > 0){
	    	return Child::createFromRow($rows[0]);
	    }
	    else return null;		
	}
}