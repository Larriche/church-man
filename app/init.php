<?php
session_start();

// core framework classes
require_once 'core/App.php';
require_once 'core/Controller.php';

// database manipulation libraries
require_once 'lib/db_update_manager.php';
require_once 'lib/db_insert_manager.php';
require_once 'lib/db_select_manager.php';
require_once 'lib/db_delete_manager.php';

// helper functions
require_once 'lib/functions.php';

// date manipulation library
require_once 'lib/date.php';

// library for simplifying PDF generation with DOMPDF
require_once 'lib/pdf_report.php';

// DOMPDF library
require_once "lib/dompdf/dompdf_config.inc.php";


// database connection variables
$db_host = 'localhost';
$db_user = 'root';
$db_password = '';
$db_name = 'church';

// create a PDO connection object
$conn_string = 'mysql:host='.$db_host.';dbname='.$db_name.';charset=utf8';
$connection = new PDO($conn_string,$db_user,$db_password);

$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
?>