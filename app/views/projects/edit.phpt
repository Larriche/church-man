<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="groups-menu" class="sub-menu">
      <ul>
          <li><a href="/projects/create">Add New Project</a></li>
          <li class="selected-tab"><a href="/projects/view_all">View Projects</a></li>
      </ul>
    </section>

    <section id="main-content-body">
     <section class="member-profile-header">
        <h2 class="item-heading"><?php echo $project->getName();?></h2>
        <ul>
            <li><a href="/projects/view_all" class="button-link">Back</a></li>
            <?php if(churchAdminLoggedIn()){?>
            <li><a href="/projects/edit/<?php echo $project->getId();?>" class="button-link">Edit Project</a></li>
            <?php } ?>
            
            <?php if(!memberLoggedIn()) { ?>
            <li><a href="/projects/pledges/<?php echo $project->getId();?>" class="button-link">Pledges</a></li>
            <li><a class="button-link">Income / Expenditure</a></li>
            <?php } ?>

        </ul>
       </section>
       
     <section class="item-view-section">
         <h3 class=item-view-section-header>Edit Project</h3>
         
         <?php require('app/views/includes/messages.phpt'); ?>

         <form method="POST" action="/projects/process_edit" class = "max-form">
         <p>
             <label for="name">Project Name:</label>
             <input type="text" name="project_name" value="<?php echo $project->getName();?>">
         </p>

         <p>
             <label for="description">Project Description:</label>
             <textarea name="description"><?php echo $project->getDescription();?></textarea>
         </p>

         <p>
             <label>Date Commenced:</label>
             
             <?php 
                $date = new PrettyDate($project->getDateCommenced());
                $day = $date->getDay();
                $month = $date->getMonthName();
                $year = $date->getYear();

                $fields = ["date_day","date_month","date_year"];
                
                include('app/views/includes/date_picker.phpt'); 
            ?>
          </p>

         <p>
             <label for="estimated_cost">Estimated Cost(Gh&#162;):</label>
             <input type="text" name="estimated_cost" value="<?php echo $project->getEstimatedCost();?>">
         </p>

         <input type="hidden" name="project_id" value="<?php echo $project->getId();?>">

         <p>
             <input type="submit" value="Update" name="edit_project" class="project-create-button">
         </p>
         </form>
     </section>
    </section>
</section>