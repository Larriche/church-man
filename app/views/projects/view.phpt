<?php
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt'); 
} 
elseif(memberLoggedIn()){
	require('app/views/members/menu.phpt');
}
else{
	require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
    <?php if(churchAdminLoggedIn()) { ?>
	<section id="projects-menu" class="sub-menu">
	    <ul>
	        <li><a href="/projects/create">Add Project</a></li>
	        <li class="selected-tab"><a href="/projects/view_all">View Project</a></li>
	    </ul>
    </section>
    <?php } ?>

	<section id="main-content-body">
	    <?php require('app/views/includes/messages.phpt'); ?>

	    <section class="member-profile-header">
        <h2 class="item-heading"><?php echo $project->getName();?></h2>
        <ul>
            <li><a href="/projects/view_all" class="button-link">Back</a></li>
            <?php if(churchAdminLoggedIn()){?>
            <li><a href="/projects/edit/<?php echo $project->getId();?>" class="button-link">Edit Project</a></li>
            <?php } ?>
            
            <?php if(!memberLoggedIn()) { ?>
            <li><a href="/projects/pledges/<?php echo $project->getId();?>" class="button-link">Pledges</a></li>
            <li><a class="button-link">Income / Expenditure</a></li>
            <?php } ?>

        </ul>
       </section>

	   <section class="item-view-section">
	       <section class="item-view-section">
		       <h3 class="item-view-section-header">Date Commenced</h3>
		       <p><?php $date = new PrettyDate($project->getDateCommenced()); echo $date->getReadable();?></p>
		   </section>

		   <section class="item-view-section">
		       <h2 class="item-view-section-header">Date Completed</h2>
		       <p>
		       <?php if($project->getStatus() == 'UNCOMPLETED') echo "Still in progress"; else echo $project->getDateCompleted();?>
		       </p>
		   </section>

		   <section class="item-view-section">
		       <h3 class="item-view-section-header">Funds Raised So Far</h3>
		       <p>Gh&#162; <?php echo number_format($project->getAmountRaised(),2,'.',',');?> p</p>
		   </section>

		   <section class="item-view-section">
		       <h3 class="item-view-section-header">Estimated Cost</h3>
		       <p>Gh&#162; <?php echo number_format($project->getEstimatedCost(),2,'.',',');?> p</p>
		   </section>

		   <section class="item-view-section">
		       <h2 class="item-view-section-header">Description</h2>
		       <p><?php echo $project->getDescription();?></p>
		   </section>

	   </section>

	   <?php if(memberLoggedIn()) { ?>
	   	<section class="item-view-section">
	   	  <h3 class="item-view-section-header">Make Pledge</h3>
	   	  <form method="POST" action="/members/make_pledge" class="mini-form" >
	   	  <p>
	   	    <label for="amount">Amount(Gh&#162;)</label>
	   	    <input type="text" name="amount">
	   	  </p>

          <input type="hidden" name="member_id" value="<?php echo $_SESSION['user_id'];?>">
          <input type="hidden" name="project_id" value="<?php echo $project->getId();?>">

	   	  <p>
	   	    <input type="submit" name="make_pledge" value="Submit">
	   	  </p>
	   	  </form> 
	   	</section>
	   <?php } ?>
	</section>
</section>