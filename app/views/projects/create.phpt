<?php 
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt'); 
}
else if(memberLoggedIn()){
    require('app/views/members/menu.phpt');
}
else{
    require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
    <?php if(churchAdminLoggedIn()) { ?>
    <section id="groups-menu" class="sub-menu">
      <ul>
          <li class="selected-tab"><a href="/projects/create">Add New Project</a></li>
          <li><a href="/projects/view_uncompleted">View Active Projects</a></li>
          <li><a href="/projects/view_completed">View Completed Projects</a></li>
      </ul>
    </section>
    <?php } ?>

    <section id="main-content-body">
     <section class="item-view-section">
         <h3 class=item-view-section-header>Create New Church Project</h3>
         
         <?php require('app/views/includes/messages.phpt'); ?>

         <form method="POST" action="/projects/process_create" class = "max-form">
         <p>
             <label for="name">Project Name:</label>
             <input type="text" name="project_name" required="1">
         </p>

         <p>
             <label for="description">Project Description:</label>
             <textarea name="description"></textarea>
         </p>

         <p>
             <label>Date Commenced:</label>
             <?php 
                $day = "0";
                $month = "0";
                $year = "0";

                $fields = ["start_day","start_month","start_year"];
            
                include('app/views/includes/date_picker.phpt'); 
              ?>
          </p>

         <p>
             <label for="estimated_cost">Estimated Cost:</label>
             <input type="text" name="estimated_cost">
         </p>

         <p>
             <input type="submit" value="Create" name="create_project" class="project-create-button">
         </p>
         </form>
     </section>
    </section>
</section>