<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
	<section id="projects-menu" class="sub-menu">
	    <ul>
	        <li><a href="/projects/create">Add Project</a></li>
	        <li class="selected-tab"><a href="/projects/view_all">View Project</a></li>
	    </ul>
    </section>

	<section id="main-content-body">
	    <section class="member-profile-header">
        <h2 class="item-heading"><?php echo $project->getName();?></h2>
        <ul>
            <li><a href="/projects/view_all" class="button-link">Back</a></li>
            <?php if(churchAdminLoggedIn()){?>
            <li><a href="/projects/edit/<?php echo $project->getId();?>" class="button-link">Edit Project</a></li>
            <?php } ?>
            
            <?php if(!memberLoggedIn()) { ?>
            <li><a href="/projects/pledges/<?php echo $project->getId();?>" class="button-link">Pledges</a></li>
            <li><a class="button-link">Income / Expenditure</a></li>
            <?php } ?>

        </ul>
       </section>

       <section class="item-view-section">
         <h3 class="item-view-section-header">Pledges</h3>
         <?php $pledges = $project->getPledges(); if(count($pledges) > 0){ 
          foreach($pledges as $pledge){ ?>
            <section class="item-view-section">
            <h3 class="item-view-section-header"></h3>
            <table>
               <colgroup>
                 <col class="key"/>
                 <col class="value"/>
                 <tr>
                    <td>Date Made</td>
                    <td><?php echo $pledge->getDate(); ?></td>
                 </tr>
                 <tr>
                    <td>Amount</td>
                    <td>Gh&#162; <?php echo $pledge->getPledgedAmount(); ?></td>
                 </tr>
                 <tr>
                    <td>Amount Paid</td>
                    <td>Gh&#162; <?php echo $pledge->getAmountPaid(); ?></td>
                 </tr>
                 <tr>
                    <td>Pledged By</td>
                    <td><?php echo UserProfile::find($pledge->getPayer())->getFullname(); ?></td>
                </tr>
               </colgroup>
            </table>
            </section>
         <?php } }else{ ?>
            <p>No pledges made towards this project yet</p>
         <?php } ?>
       </section>
    </section>
</section>
