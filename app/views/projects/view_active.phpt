<?php 
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt');
}
elseif(memberLoggedIn()){
    require('app/views/members/menu.phpt');
} 
else{
    require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
  <?php if(churchAdminLoggedIn()) { ?>
  <section id="groups-menu" class="sub-menu">
      <ul>
          <li><a href="/projects/create">Add Project</a></li>
          <li class="selected-tab"><a href="/projects/view_uncompleted">View Active Projects</a></li>
          <li><a href="/projects/view_completed">View Completed Projects</a></li>
      </ul>
  </section>
  <?php } ?>

  <section id="main-content-body">
     <section class="item-view-section">
         <h3 class="item-view-section-header">Church Projects</h3>
         <table>
             <colgroup>
              <col class = "counter"/>
              <col class = "name"/>
  	        <col class = "start_date"/>
  	        <col class = "amount_raised"/>
  	        <col class = "estimated_cost"/>
            
  	        </colgroup>
  	        <tr>
  	            <th></th>
  	            <th>Name</th>
  	            <th>Started</th>
  	            <th>Estimated Cost</th>
                <th>Funds Raised</th>
                <th></th>
  	        </tr>

             <?php $count = 1; foreach($projects as $project) {?>
             	<tr>
                  <td><?php echo $count++; ?></td>

                  <td><?php echo $project->getName();?></td>

                  <td><?php $date = new PrettyDate($project->getDateCommenced());
                      echo $date->getReadable()?></td>


                  <td>
                    <?php if($project->getEstimatedCost()){ ?>
                      Gh&#162;
                    <?php echo $project->getEstimatedCost(); } else {?>
                       unknown
                    <?php } ?>
                  </td>

                  <td>Gh&#162; <?php echo number_format($project->getAmountRaised(),2,'.',',');?>p</td>

                  <td><a class="item-link" href="/projects/view/<?php echo $project->getId();?>">View</td>

                  <td><a class="item-link" href="/projects/mark_completed/<?php echo $project->getId();?>">Mark as Completed</a></td>
             	</tr>
             <?php } ?>
         </table>
  </section>
</section>