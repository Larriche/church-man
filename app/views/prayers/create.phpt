<?php require('app/views/members/menu.phpt'); ?>

<section id="main-content-container">

  <section id="groups-menu" class="sub-menu">
	    <ul>
	        <li class="selected-tab"><a href="/groups/create">New Request</a></li>
	        <li><a href="/prayer_requests/view_all">My Requests</a></li>
	    </ul>
  </section>

  <section id="main-content-body">
    <?php require('app/views/includes/messages.phpt'); ?>

    <section class="item-view-section">
      <h3 class="item-view-section-header">Make Prayer Request</h3>

      <form method="POST" action="/prayer_requests/process_creation" class="max-form">
        <p>
          <label for="message">Message</label>
          <textarea name="message"></textarea>
        </p>

        <input type="hidden" name="user_id" value="<?php echo $_SESSION['user_id'];?>">

        <p>
          <input type="submit" name="create_prayer" value="Submit">
        </p>
      </form>
    </section>
  </section>
</section>