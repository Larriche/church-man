<?php
require('app/views/pastors/menu.phpt');
?>


<section id="main-content-container">
  <section id="main-content-body">
    <section class="item-view-section">
      <h3 class="item-view-section-header">Unread Prayer Requests</h3>
      <?php if(count($prayers) == 0) {?>
      	<p>No unread prayer requests</p>
      <?php } else{ ?>
      	<table>
      	   <?php foreach($prayers as $prayer) {?>
      	   	<tr>
      	   	  <td>
      	   	  Made on
      	   	  <?php
      	   	     $date = new PrettyDate($prayer->getDateMade());
      	   	     echo $date->getReadable();
      	   	  ?>
      	   	  </td>

      	   	  <td>
      	   	    <a class="item-link" href="/prayer_requests/view/<?php echo $prayer->getId();?>">
      	   	      View</a>
      	   	  </td>
      	   	</tr>
      	   <?php } ?>
      	</table>
      <?php } ?>
    </section>   
  </section>
</section>
