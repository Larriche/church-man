<?php
require('app/views/pastors/menu.phpt');
?>


<section id="main-content-container">
  <section id="main-content-body">
    <section class="item-view-section">
      <h3 class="item-view-section-header">Unread Prayer Requests</h3>
      <?php if(count($activePrayers) == 0) {?>
      	<p>No unread prayer requests</p>
      <?php } else{ ?>
      	<table>
      	   <?php foreach($activePrayers as $prayer) {?>
      	   	<tr>
      	   	  <td>
      	   	  Made by 
      	   	  <?php 
      	   	     $member = UserProfile::find($prayer->getUserId());
      	   	     echo $member->getTitle().$member->getFullname();
      	   	  ?>
      	   	  on 
      	   	  <?php
      	   	     $date = new PrettyDate($prayer->getDateMade());
      	   	     echo $date->getReadable();
      	   	  ?>
      	   	  </td>

      	   	  <td>
      	   	    <a class="item-link" href="/prayer_requests/view/<?php echo $prayer->getId();?>">
      	   	      View</a>
      	   	  </td>
      	   	</tr>
      	   <?php } ?>
      	</table>
      <?php } ?>
    </section>

    <section class="item-view-section">
      <h3 class="item-view-section-header">Seen Prayer Requests</h3>
      <?php if(count($inactivePrayers) == 0) {?>
      	<p>No prayer requests</p>
	      <?php } else{ ?>
	      	<table>
	      	   <?php foreach($inactivePrayers as $prayer) {?>
	      	   	<tr>
	      	   	  <td>
	      	   	  <?php 
	      	   	     $member = UserProfile::find($prayer->getUserId());
	      	   	     echo $member->getTitle().$member->getFullname();
	      	   	  ?>
	      	   	  on 
	      	   	  <?php
	      	   	     $date = new PrettyDate($prayer->getDateMade());
	      	   	     echo $date->getReadable();
	      	   	  ?>
	      	   	  </td>

	      	   	  <td>
	      	   	    <a class="item-link" href="/prayer_requests/view/<?php echo $prayer->getId();?>">
	      	   	      View</a>
	      	   	  </td>
	      	   	</tr>
	      	   <?php } ?>
	      	</table>
	      <?php } ?>
    </section>
     
    </section>
  </section>
</section>
