<?php
if(memberLoggedIn()){
	require('app/views/members/menu.phpt');
}
else{
	require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
  <section id="main-content-body">
    <section class="item-view-section">
      <?php 
      $member = UserProfile::find($prayer->getUserId()); 
      $date = new PrettyDate($prayer->getDateMade());
      ?>
      <h3 class="item-view-section-header">Prayer Request by <?php echo $member->getFullname();?>
      	 on <?php echo $date->getReadable(); ?>
      </h3>
      <p>
      <?php echo $prayer->getMessage(); ?>
      </p>
    </section>
  </section>
</section>