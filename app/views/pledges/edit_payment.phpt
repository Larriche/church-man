<?php
require('app/views/churchadmin/menu.phpt');
?>

<section id="main-content-container">
 <section id="main-content-body">
   <a href="/pledges/view/<?php echo $payment->getPledgeId();?>" class="button-link">Back to Pledge</a>
   <section class="item-view-section">
     <h3 class="item-view-section-header">Edit Payment Details</h3>
     <?php require('app/views/includes/messages.phpt'); ?>
     <form method="POST" action="/pledges/process_payment_edit" class="max-form">
       <p>
        <label for="amount">Amount(Gh&#162)</label>
        <input type="text" name="amount" value="<?php echo $payment->getAmount();?>">
       </p>

       <p>
         <label>Date</label>
           <?php 
            $date = new PrettyDate($payment->getDate());
            $day = $date->getDay();
            $month = $date->getMonthName();
            $year = $date->getYear();

            $fields = ["pay_day","pay_month","pay_year"];
        
            include('app/views/includes/date_picker.phpt'); 
           ?>
       </p>

       <input type="hidden" name="payment_id" value="<?php echo $payment->getId();?>">
       <input type="submit" name="update_payment" value="Update">
     </form> 
   </section>
 </section>
</section>