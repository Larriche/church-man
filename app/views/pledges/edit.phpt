<?php 
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt');
}
else{
    require('app/views/members/menu.phpt');
}
?>

<section id="main-content-container">
    <section id="main-content-body">
       <h2 class="item-heading">Pledge towards <?php echo Project::find($pledge->getProject())->getName();?></h2>
       <a href="/members/pledgebook/<?php echo $pledge->getPayer();?>" class="button-link">
            Back to Pledges</a>

       <section class="item-view-section">
         <h3 class="item-view-section-header">Edit Pledge</h3>
         <?php require('app/views/includes/messages.phpt'); ?>
         <form method="POST" action="/pledges/process_edit" class="max-form">
           <p>
             <label for="amount">Amount(Gh&#162)</label>
             <input type="text" name="amount" value="<?php echo $pledge->getPledgedAmount();?>">
           </p>

           <p>
             <label for="project">Project</label>
             <select name="project_id">
              <?php foreach ($projects as $project) { ?>
                       <option value="<?php echo $project->getId();?>" <?php 
                       echo ($project->getName() == Project::find($pledge->getProject())->getName())? "selected='1'" : "";?>><?php echo $project->getName();?></option>
              <?php } ?>
             </select>
           </p>

           <p>
               <label>Date</label>
               
               <?php 
                $date = new PrettyDate($pledge->getDate());

                $day = $date->getDay();
                $month = $date->getMonthName();
                $year = $date->getYear();

                $fields = ["pledge_day","pledge_month","pledge_year"];
            
                include('app/views/includes/date_picker.phpt'); 
                ?>
           </p>

           <input type="hidden" name="pledge_id" value="<?php echo $pledge->getId();?>">

           <p>
             <input type="submit" name="update_pledge" value="Update">
           </p>

         </form>
       </section>
    </section>
</section>