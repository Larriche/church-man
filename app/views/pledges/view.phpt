<?php 
if(churchAdminLoggedIn()){ 
    require('app/views/churchadmin/menu.phpt');
}
elseif(memberLoggedIn()){
    require('app/views/members/menu.phpt');
} 
?>

<section id="main-content-container">
    <section id="main-content-body">
       <?php require('app/views/includes/messages.phpt');?>
       <h2 class="item-heading">Pledge towards <?php echo Project::find($pledge->getProject())->getName();?></h2>
       <a href="/members/pledgebook/<?php echo $pledge->getPayer();?>" class="button-link">
            Back to Pledges</a>
       <section class = "item-view-section">
           <h3 class="item-view-section-header">Pledge Info</h3>
           <table>
               <tr>
                   <td>Project:</td>
                   <td><?php echo Project::find($pledge->getProject())->getName();?></td>
               </tr>

               <tr>
                   <td>Date Made:</td>
                   <td><?php $date = new PrettyDate($pledge->getDate()); echo $date->getReadable();?></td>
               </tr>

               <tr>
                   <td>Amount Pledged:</td>
                   <td>Gh&#162; <?php echo number_format($pledge->getPledgedAmount(),2,'.',',');?> p</td>
               </tr>

               <tr>
                   <td>Amount Paid:</td>
                   <td>Gh&#162; <?php echo number_format($pledge->getAmountPaid(),2,'.',',');?> p</td>
               </tr>

               <tr>
                   <td>% Paid:</td>
                   <td>
                   <?php 
                      if($pledge->getPledgedAmount() != 0)
                          $percent = $pledge->getAmountPaid() / $pledge->getPledgedAmount() * 100; 
                      else
                          $percent = 100;
                      echo round($percent , 2) . '%';
                    ;?>
                   </td>
               </tr>

               <tr>
                   <td>Pledged By:</td>
                   <td><?php echo UserProfile::find($pledge->getPayer())->getFullname();?></td>
               </tr>
           </table>
       </section>

       <section class="item-view-section">
           <h3 class="item-view-section-header">Recent Payments</h3>
           <table>
               <tr>
                   <th>Amount</th>
                   <th>Date</th>
               </tr>

               <?php foreach($pledgePayments as $pledgePayment) { ?>
               <tr>
                   <td>Gh&#162; <?php echo number_format($pledgePayment->getAmount(),2,'.',',');?> p</td>
                   <td><?php $date = new PrettyDate($pledgePayment->getDate()); echo $date->getReadable();?></td>
                   <td><a href="/pledges/edit_payment/<?php echo $pledgePayment->getId();?>" class="item-link">Edit</a></td>
               </tr>
               <?php } ?>
           </table>
       </section>

       <?php if(churchAdminLoggedIn()) { ?>
       <section class="item-view-section">
          <h3 class="item-view-section-header">Add Payment</h3>
          <form method="POST" action="/pledges/record_payment" class="max-form">
              <p>
	               <label for="amount">Amount:</label>
	               <input type="text" name="amount">
               </p>

               <p>
                   <label>Date</label>
                   <?php 
                    $day = "0";
                    $month = "0";
                    $year = "0";

                    $fields = ["pay_day","pay_month","pay_year"];
                
                    include('app/views/includes/date_picker.phpt'); 
                    ?>
               </p>

               <input type="hidden" name="pledge_id" value="<?php echo $pledge->getId();?>">
               <input type="hidden" name="project_id" value="<?php echo $pledge->getProject();?>">

               <p>
                   <input type="submit" name="make_payment" value="Add">
               </p>
          </form>
       </section>
       <?php } ?>

    </section>
</section>