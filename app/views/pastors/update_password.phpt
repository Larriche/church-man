<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
     <section id="main-content-body">
           <?php require('app/views/includes/messages.phpt'); ?>
           
           <section class="item-view-section">
	          <h2 class="item-view-section-header">Update Password</h2>
	          <form method="POST" action = "/pastors/process_password_update" class="mini-form">
	          
	          <p>
	          <label for="password">New Password:</label>
	          <input type="password" name="new_password">
	          </p>

	          <input type="hidden" name="id" value="<?php echo $id;?>">

	          <p>
	          <input type="submit" name="update_password" value="Submit">
	          </p>
	          </form>
	        </section>
     </section>
</section>