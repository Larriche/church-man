<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="groups-menu" class="sub-menu">
      <ul>
          <li  class="selected-tab"><a href="/pastors/register">Add new Pastor</a></li>
          <li><a href="/pastors/view">View All Pastors</a></li>
      </ul>
    </section>
    
    <section id="main-content-body">
        <section class="item-view-section">
           <h3 class="item-view-section-header">Pastors</h3>
           <table>
           <?php  foreach($pastors as $pastor) { ?>
              <tr>
                 <td><?php echo $pastor->getUsername(); ?></td>
                 <td><a class="item-link" href="/pastors/update_password/<?php echo $pastor->getId();?>">Update Password</a></td>
                 <td><a href="/pastors/delete/<?php echo $pastor->getId();?>" class="danger">Delete</a></td>
              </tr>
           <?php } ?> 
           </table> 
        </section>
    </section>

    <?php
    $dialogMessage = 'Are you sure you want to delete this user?'; 
    require('app/views/includes/dialog_box.phpt'); 
    ?>
</section>

<script>
window.addEventListener('load',initialize);

function initialize()
{
    initializeDialogComponents();
}
</script>