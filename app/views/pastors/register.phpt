<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="groups-menu" class="sub-menu">
	    <ul>
	        <li  class="selected-tab"><a href="/pastors/register">Add new Pastor</a></li>
	        <li><a href="/pastors/view">View All Pastors</a></li>
	    </ul>
    </section>

    <section id="main-content-body">
            <section class="item-view-section">

              <h2 class="item-view-section-header">Pastor Registration</h2>
              <form method="POST" action = "/pastors/process_registration" class = "mini-form">
                  <?php require('app/views/includes/messages.phpt'); ?>

	              <p>
	              <label for="username">Username:</label>
	              <input type="text" name="username">
	              </p>

	              <p>
	              <label for="password">Password:</label>
	              <input type="password" name="password">
	              </p>

	              <p>
	              <input type="submit" name="pastor_regis" value="Register">
	              </p>
              </form>
            </section>
    </section>
</section>