<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="main-content-body">
       <section class="member-profile-header">
        <h2 class="item-heading"><?php echo $event->getName();?></h2>
        <ul>
            <?php if(churchAdminLoggedIn()) {?>
            <li><a href="/events/edit/<?php echo $event->getId();?>" class="button-link">Edit Event</a></li>
            <?php } ?>

            <li><a href="/events/view/<?php echo $event->getId();?>" class="button-link">View Event</a></li>

            <?php if(churchAdminLoggedIn() || pastorLoggedIn()){ ?>
            <li><a href="/events/attendance/<?php echo $event->getId();?>" class="button-link">
              Attendance</a></li>

            <li><a href="/events/finances/<?php echo $event->getId();?>" class="button-link">
              Finances</a></li>
            <?php } ?>

             <?php if(churchAdminLoggedIn()){ ?>
              <li><a href="/events/delete/<?php echo $event->getId();?>" class="danger">Delete</a></li>
            <?php } ?>

            <li><a href="/events/view_all" class="button-link">
              Back to Events</a></li>
        </ul>
       </section>

       <?php require('app/views/includes/messages.phpt');?>

       <section class="item-view-section">
          <h3 class="item-view-section-header">Income</h3>
          <?php $rows = $event->getIncome(); if(count($rows) > 0){ ?>
          <table>
             <tr>
                 <th>Amount</th>
                 <th>Date</th>
                 <th>Description</th>
             </tr>
             <?php foreach($rows as $row) {?>
             <tr>
                 <td>Gh&#162; <?php echo number_format($row['amount'],2,'.',',');?></td>
                 <td><?php $date = new PrettyDate($row['date_recorded']); echo $date->getReadable(); ?></td>
                 <td><?php echo $row['description']; ?></td>
             </tr>
             <?php } ?>
          </table>
          <?php } else {?>
          	<p>No income recorded for this event</p>
          <?php } ?>
       </section>

       <section class="item-view-section">
          <h3 class="item-view-section-header">Expenses</h3>
          <?php $rows = $event->getExpenses(); if(count($rows) > 0){ ?>
          <table>
             <tr>
                 <th>Amount</th>
                 <th>Date</th>
                 <th>Description</th>
             </tr>
             <?php foreach($rows as $row) {?>
             <tr>
                 <td>Gh&#162; <?php echo number_format($row['amount'],2,'.',',');?></td>
                 <td><?php $date = new PrettyDate($row['date_recorded']); echo $date->getReadable(); ?></td>
                 <td><?php echo $row['description']; ?></td>
             </tr>
             <?php } ?>
          </table>
          <?php } else {?>
          	<p>No expenses recorded for this event</p>
          <?php } ?>
       </section>

       <section class = "item-view-section">
          <h3 class='item-view-section-header'>Record New Transaction</h3>

          <form method="POST" action="/events/record_finance" class='max-form'>
             <p>
             <label for="amount">Amount:</label>
             <input type="text" name="amount">
             </p>

             <p>
             <label for="date">Date:</label>

             <?php 
                    $day = "0";
                    $month = "0";
                    $year = "0";

                    $fields = ["date_day","date_month","date_year"];
                
                    include('app/views/includes/date_picker.phpt'); 
             ?>
             </p>

             <p>
               <label for="type">Transaction type:</label>
               <select name="type">
               	 <option value='INCOME'>Income</option>
               	 <option value='EXPENSE'>Expenditure</option>
               </select>
             </p>

             <p>
               <label for="description">Description:</label>
               <textarea name="description"></textarea>
             </p>

             <input type="hidden" name="event_id" value="<?php echo  $event->getId();?>">

             <p>
               <input type="submit" name="add_transaction" value="Add">
             </p>
          </form>
       </section>
    </section>
</section>

<?php 
   $dialogMessage = 'Are you sure you want to carry out this action?';
   require('app/views/includes/dialog_box.phpt'); 
?>

<script>
window.addEventListener('load',initialize);

function initialize()
{
  initializeDialogComponents();
}
</script>