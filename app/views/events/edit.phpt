<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
	<section id="main-content-body">
	   <section class="member-profile-header">
        <h2 class="item-heading"><?php echo $event->getName();?></h2>
        <ul>
            <?php if(churchAdminLoggedIn()) {?>
            <li><a href="/events/edit/<?php echo $event->getId();?>" class="button-link">Edit Event</a></li>
            <?php } ?>

            <li><a href="/events/view/<?php echo $event->getId();?>" class="button-link">View Event</a></li>

            <?php if(churchAdminLoggedIn() || pastorLoggedIn()){ ?>
            <li><a href="/events/attendance/<?php echo $event->getId();?>" class="button-link">
              Attendance</a></li>

            <li><a href="/events/finances/<?php echo $event->getId();?>" class="button-link">
              Finances</a></li>
            <?php } ?>

             <?php if(churchAdminLoggedIn()){ ?>
              <li><a href="/events/delete/<?php echo $event->getId();?>" class="danger">Delete</a></li>
            <?php } ?>

            <li><a href="/events/view_all" class="button-link">
              Back to Events</a></li>
        </ul>
       </section>
	   <?php require('app/views/includes/messages.phpt'); ?>
        
        <section class = "item-view-section">
           <h3 class="item-view-section-header">Edit Event</h3>
           <form method = "POST" action = "/events/process_edit" class="max-form">
               <p>
                   <label for="attendance">Event Name:</label>
                   <input type="text" name="event_name" value="<?php echo $event->getName();?>">
               </p>

               <p>
                   <label for="date">Event Start Date:</label>
                   
                   <?php 
                    $date = new PrettyDate($event->getDateStarted());
                    $day = $date->getDay();
                    $month = $date->getMonthName();
                    $year = $date->getYear();

                    $fields = ["start_day","start_month","start_year"];
                
                    include('app/views/includes/date_picker.phpt'); 
                  ?>
               </p>

               <p>
                   <label for="event">Event Close Date:</label>
                   
                   <?php 
                    $date = new PrettyDate($event->getDateCompleted());
                    $day = $date->getDay();
                    $month = $date->getMonthName();
                    $year = $date->getYear();

                    $fields = ["end_day","end_month","end_year"];
                
                    include('app/views/includes/date_picker.phpt'); 
                  ?>
               </p>

               <p>
                   <label for="type">Attendance Type:</label>
                   <select name="type">
                       <option value = "CHURCH">All Church Members</option>
                       <option value = "GROUP">Group</option>
                       <option value = "INDIVIDUALS">Individuals</option>
                   </select>
               </p>

               <p>
                  <label for="description">Description:</label>
                  <textarea name="description"><?php echo $event->getDescription();?></textarea>
               </p>

               <input type="hidden" name="event_id" value="<?php echo $event->getId();?>">

               <p>
                   <input type="submit" name="process_event" value="Update">
               </p>
           </form>
        </section>
	</section>
</section>

<?php 
   $dialogMessage = 'Are you sure you want to carry out this action?';
   require('app/views/includes/dialog_box.phpt'); 
?>

<script>
window.addEventListener('load',initialize);

function initialize()
{
  initializeDialogComponents();
}
</script>