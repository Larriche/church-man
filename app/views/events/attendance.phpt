<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="main-content-body">
       <section class="member-profile-header">
        <h2 class="item-heading"><?php echo $event->getName();?></h2>
        <ul>
            <?php if(churchAdminLoggedIn()) {?>
            <li><a href="/events/edit/<?php echo $event->getId();?>" class="button-link">Edit Event</a></li>
            <?php } ?>

            <li><a href="/events/view/<?php echo $event->getId();?>" class="button-link">View Event</a></li>

            <?php if(churchAdminLoggedIn() || pastorLoggedIn()){ ?>
            <li><a href="/events/attendance/<?php echo $event->getId();?>" class="button-link">
              Attendance</a></li>

            <li><a href="/events/finances/<?php echo $event->getId();?>" class="button-link">
              Finances</a></li>
            <?php } ?>

             <?php if(churchAdminLoggedIn()){ ?>
              <li><a href="/events/delete/<?php echo $event->getId();?>" class="danger">Delete</a></li>
            <?php } ?>

            <li><a href="/events/view_all" class="button-link">
              Back to Events</a></li>
        </ul>
       </section>

       <?php require('app/views/includes/messages.phpt');?>

       <section class="item-view-section">
          <h3 class="item-view-section-header">Attendance Records</h3>
          <?php $attendanceRecs = $event->getAttendanceRecord(); if(count($attendanceRecs) > 0){ ?>
          <table>
             <tr>
                 <th>Attendance</th>
                 <th>Date</th>
             </tr>
             <?php foreach($attendanceRecs as $row) {?>
             <tr>
                 <td><?php echo $row->getNumber();?></td>
                 <td><?php $date = new PrettyDate($row->getDate()); echo $date->getReadable();?></td>
                 <td><a class="item-link" href="/attendance_records/view/<?php echo $row->getId();?>">View detail</a></td>
                 <td><a class="item-link" href="/attendance_records/edit/<?php echo $row->getId();?>">Edit</a></td>
             </tr>
             <?php } ?>
          </table>
          <?php } else {?>
          	<p>No attendance recorded for this event yet</p>
          <?php } ?>
       </section>

       <?php if(churchAdminLoggedIn()){ ?>
       <section class = "item-view-section">
          <h3 class='item-view-section-header'>Record New Attendance</h3>

          <form method="POST" action="/events/record_attendance" class='max-form'>
             <p>
             <label for="amount">Attendance:</label>
             <input type="text" name="attendance">
             </p>

             <p>
             <label for="date">Date:</label>

             <?php 
                    $day = "0";
                    $month = "0";
                    $year = "0";

                    $fields = ["date_day","date_month","date_year"];
                
                    include('app/views/includes/date_picker.phpt'); 
             ?>
             </p>

             <p>
                  <label for="time">Time:</label>

                  <?php 

                   $hour = null;
                   $minute = null;
                   $fields = ['date_hour','date_minute'];
                  include('app/views/includes/time_picker.phpt');
                  ?>
                </p>

             <p>
               <label for="note">Note:</label>
               <textarea name="note"></textarea>
             </p>

             <input type="hidden" name="event_id" value="<?php echo  $event->getId();?>">

             <p>
               <input type="submit" name="record_attendance" value="Add">
             </p>
          </form>
       </section>
       <?php } ?>
    </section>
</section>

<?php 
   $dialogMessage = 'Are you sure you want to carry out this action?';
   require('app/views/includes/dialog_box.phpt'); 
?>

<script>
window.addEventListener('load',initialize);

function initialize()
{
  initializeDialogComponents();
}
</script>