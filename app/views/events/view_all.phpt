<?php 
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt');
}
elseif(memberLoggedIn()){
    require('app/views/members/menu.phpt');
}
else{
    require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
    <?php if(churchAdminLoggedIn()) { ?>
    <section id="groups-menu" class="sub-menu">
      <ul>
          <li><a href="/events/create">Add New Church Event</a></li>
          <li class="selected-tab"><a href="/events/view_all">View Church Events</a></li>
      </ul>
    </section>
    <?php } ?>

    <section id="main-content-body">
        <section class="item-view-section">
            <h3 class="item-view-section-header">Church Events</h3>

            <?php if(count($events) == 0) { ?>
                <p>There are no church events</p>
            <?php }else { ?>
                <table>
                <?php foreach($events as $event) { ?>
                    <tr>
                    <td><?php echo $event->getName(); ?></td>
                    <td><a class="item-link" href="/events/view/<?php echo $event->getId();?>">View</a></td>
                    </tr>
                <?php } ?>
                </table>
            <?php } ?>

        </section>


    </section>
   
</section>