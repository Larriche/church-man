<?php 
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt');
}
elseif(memberLoggedIn()){
    require('app/views/members/menu.phpt');
}
else{
    require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
    <?php if(churchAdminLoggedIn()) { ?>
    <section id="events-menu" class="sub-menu">
      <ul>
          <li><a href="/events/create">Add New Church Event</a></li>
          <li class="selected-tab"><a href="/events/view">View Church Events</a></li>
      </ul>
    </section>
    <?php } ?>

    <section id="main-content-body">
      <?php require('app/views/includes/messages.phpt'); ?>

      <section class="member-profile-header">
        <h2 class="item-heading"><?php echo $event->getName();?></h2>
        <ul>
            <?php if(churchAdminLoggedIn()) {?>
            <li><a href="/events/edit/<?php echo $event->getId();?>" class="button-link">Edit Event</a></li>
            <?php } ?>

            <li><a href="/events/view/<?php echo $event->getId();?>" class="button-link">View Event</a></li>

            <?php if(churchAdminLoggedIn() || pastorLoggedIn()){ ?>
            <li><a href="/events/attendance/<?php echo $event->getId();?>" class="button-link">
              Attendance</a></li>

            <li><a href="/events/finances/<?php echo $event->getId();?>" class="button-link">
              Finances</a></li>
            <?php } ?>

            <?php if(churchAdminLoggedIn()){ ?>
              <li><a href="/events/delete/<?php echo $event->getId();?>" class="danger">Delete</a></li>
            <?php } ?>

            <li><a href="/events/view_all" class="button-link">
              Back to Events</a></li>
        </ul>
       </section>

        <section class="item-view-section">
            <h3 class="item-view-section-header">Event Info:</h3>

            <table>
               <colgroup>
                   <col class="field">
                   <col class="value">
               </colgroup>
               <tr>
                   <td>Date Started:</td>
                   <td><?php $date = new PrettyDate($event->getDateStarted()); echo $date->getReadable();?></td>
               </tr>

               <tr>
                   <td>Date Completed:</td>
                   <td><?php $date = new PrettyDate($event->getDateCompleted()); echo $date->getReadable();?></td>
               </tr>

               <tr>
                   <td>Attending:</td>
                    <td>
                      <ul>
                      <?php if(count($event->getAttendance())) { ?>
                        <?php foreach($event->getAttendance() as $attendee) { 
                             if($event->getEventType() == 'GROUP') { ?>
                              <li><?php echo Group::find($attendee)->getName();?></li>
                        <?php } else if($event->getEventType() == 'INDIVIDUALS'){ ?>
                              <li><?php echo UserProfile::find($attendee)->getFullname();?></li>
                        <?php }}}?>

                      <?php if($event->getEventType() == 'CHURCH') { ?>
                             <li>All Church Members</li>
                      <?php } ?>
                      </ul>
                    </td>
               </tr>
            </table>
        </section>

        <?php if(churchAdminLoggedIn()) { ?>
        <section class="item-view-section">
            <?php if($event->getEventType() == "GROUP") {?>
            <h3 class="item-view-section-header">Add new attending group:</h3>
            <form method="POST" action="/events/add_attending_group">
                <p>
                    <label for="group">Group:</label>
                    <select name="group">
                        <?php $groups = Group::findAll(); ?>
                        <?php foreach ($groups as $group){ ?>
                        	<option value="<?php echo $group->getId(); ?>"><?php echo $group->getName();?></option>
                        <?php } ?>
                    </select>
                </p>

                <input type="hidden" name="event_id" value="<?php echo $event->getId();?>">
                <p>
                    <input type="submit" name="add_group" value="Add" class="add-button">
                </p>
            </form>
            <?php } else if($event->getEventType() == "INDIVIDUALS") {?>
            <h3 class="item-view-section-header">Add new attending individual</h3>
            <form method="POST" action="/events/add_attending_member">
                <p>
                    <label for="individual">Member:</label>
                    <input type="text" name="member_name"
                </p>
                <input type="hidden" name="event_id" value="<?php echo $event->getId();?>">

                <p>
                    <input type="submit" name="add_member" value="Add" class="add-button">
                </p>
            </form>
            <?php } ?>
        </section>
        <?php } ?>

        <section class= "item-view-section">
            <h3 class="item-view-section-header">Attendance Graph</h3>
            <?php if(count($event->getAttendanceRecord()) > 0){?>
            <section class="chart-container" id="attendance-graph">
            </section>
            <?php } else {?>
            <p>No attendance records yet</p>
            <?php } ?>
        </section>
    </section>
</section>

<?php 
   $dialogMessage = 'Are you sure you want to carry out this action?';
   require('app/views/includes/dialog_box.phpt'); 
?>

<script>
window.addEventListener('load',initialize);

function initialize()
{
  initializeDialogComponents();
}
</script>

<?php
$graphJs  = '<script>window.onload = function () {
  if(document.getElementById("attendance-graph")){
  var chart = new CanvasJS.Chart("attendance-graph", {
    title:{
      text: "",
      fontColor: "rgba(100,149,237,.8)"              
    },
    axisY :{
        lineColor: "#3CB371",
        gridColor: "#F0FFFF", 
        },
        axisX :{
        lineColor: "#3CB371",
        gridColor: "#F0FFFF", 
        },
    data: [              
    {
      // Change type to "doughnut", "line", "splineArea", etc.
      type: "bar",
      color: "rgba(152,251,152,0.3)",
      dataPoints: [';

$count = 0;
$attendanceRecords = $event->getAttendanceRecord();


foreach($attendanceRecords as $attendance){
    if($count == 10)
        break;

    $date = new PrettyDate($attendance->getDate());
    $graphJs .= '{ label: '."'".$date->getReadable()."'".',y:'.$attendance->getNumber().'},';
        $count++;
}

 
$graphJs = rtrim($graphJs,',');      

$graphJs .='
      ]
    }
    ]
  });
  chart.render();
}
  
}</script>';
echo $graphJs;
?>