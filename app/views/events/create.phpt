<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">

    <section id="groups-menu" class="sub-menu">
      <ul>
          <li class="selected-tab"><a href="/events/create">Add New Church Event</a></li>
          <li><a href="/events/view_all">View Church Events</a></li>
      </ul>
    </section>
    <section id="main-content-body">
        
        <?php require('app/views/includes/messages.phpt'); ?>
        
        <section class = "item-view-section">
           <h3 class="item-view-section-header">Add new Event</h3>
           <form method = "POST" action = "/events/add_event" class="max-form">
               <p>
                   <label for="attendance">Event Name:</label>
                   <input type="text" name="event_name">
               </p>

               <p>
                   <label for="date">Event Start Date:</label>
                   
                   <?php 
                    $day = "0";
                    $month = "0";
                    $year = "0";

                    $fields = ["start_day","start_month","start_year"];
                
                    include('app/views/includes/date_picker.phpt'); 
                  ?>
               </p>

               <p>
                   <label for="event">Event Close Date:</label>
                   
                   <?php 
                    $day = "0";
                    $month = "0";
                    $year = "0";

                    $fields = ["end_day","end_month","end_year"];
                
                    include('app/views/includes/date_picker.phpt'); 
                  ?>
               </p>

               <p>
                   <label for="type">Attendance Type:</label>
                   <select name="type">
                       <option value = "CHURCH">All Church Members</option>
                       <option value = "GROUP">Group</option>
                       <option value = "INDIVIDUALS">Individuals</option>
                   </select>
               </p>

               <p>
                  <label for="description">Description:</label>
                  <textarea name="description"></textarea>
               </p>

               <p>
                   <input type="submit" name="add_event" value="Add" class="add-button">
               </p>
           </form>
        </section>
    </section>
</section>