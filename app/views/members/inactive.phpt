<?php
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt');
}
else{ // pastor logged in
    require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
  <section id="main-content-body">
     <section id="members-view">
         <h3 class="table-heading">Deactivated Church Members</h3>
         <table>
             <colgroup>
              <col class = "counter"/>
              <col class = "image"/>
  	        <col class = "name"/>
  	        <col class = "date"/>
  	        </colgroup>

             <?php $index = $start; foreach($memberProfiles as $profile) { ?>
             	<tr>
             	   <td><?php echo $index++;?></td>
                 <td><img src='<?php echo $profile->getImageUrl();?>' height="60" width="60"></td>
             	   <td><a href = "/members/view/<?php echo $profile->getUserId();?>">
                  <?php echo $profile->getFullname();?>
                  </a></td>
             	   <td>member since <?php $date = new PrettyDate($profile->getDateJoined());
                     echo $date->getReadable();?></td>
                 <td><a href="/members/restore/<?php echo $profile->getUserId()?>" class="item-link">Restore</td>
             	</tr>
             <?php } ?>
         </table>

         <?php if(($start + $offset) <= $count) { ?>
         <section class="paginator-links">
           <p>
           <?php if( ($start - $offset) > 0) { ?>
           <a href="/members/view_all/<?php echo $start - $offset;?>" class="item-link">Prev</a>
           <?php } ?>

           <?php displayPaginatorLinks($count,$offset,14,(int)($start / $offset) + 1,'href="/members/view_all/');?>

           <?php if( ($start + $offset) <= $count) { ?>
           <a href="/members/view_all/<?php echo $start + $offset;?>" class="item-link">Next</a>
           <?php } ?>
           </p>
         </section>
         <?php } ?>
     </section>
  </section>
</section>