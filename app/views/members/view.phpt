<?php
if(churchAdminLoggedIn()){ 
    require('app/views/churchadmin/menu.phpt');
}
if(memberLoggedIn()){
    require('app/views/members/menu.phpt');
} 
?>

<section id="main-content-container">
  <?php if(churchAdminLoggedIn() || pastorLoggedIn()) { ?>
  <section id="member-menu" class="sub-menu">
      <ul>
          <li class="selected-tab"><a href="/members/view/<?php echo $member->getUserId();?>">Profile</a></li>
          <li><a href="/members/tithebook/<?php echo $member->getUserId();?>">Tithe Book</a></li>
          <li><a href="/members/pledgebook/<?php echo $member->getUserId();?>">Pledges</a></li>
      </ul>
  </section>
  <?php } ?>

  <section id="main-content-body">
      <section class="member-profile-header">
        <img src="<?php echo $member->getImageUrl();?>" width="150" height="150">
        <h2 class="item-heading"><?php echo $member->getTitle().' '.$member->getFullname();?></h2>
        <ul>
          <?php if(churchAdminLoggedIn()) {?>
            <li><a href="/members/edit/<?php echo $member->getUserId();?>" class="button-link">Edit Profile</a></li>
            <li><a href="/members/update_password/<?php echo $member->getUserId();?>" class="button-link">
              Update Password</a></li>
             <?php } ?>
            <?php if(churchAdminLoggedIn()) { ?>
              <li><a href="/members/delete/<?php echo $member->getUserId();?>" class="danger">Delete Member</a></li>
            <?php } ?>

        </ul>
      </section>
    
      <section class="item-view-section">
       <h3 class="item-view-section-header">Log In Information</h3>
        <table>
          <colgroup>
            <col class="field"/>
            <col class="value"/>
          </colgroup>

          <tr>
            <td>Username:</td>
            <td><?php echo User::find($member->getUserId())->getUsername();?></td>
          </tr>
        </table>

      </section>

      <section class="item-view-section">
         <h3 class="item-view-section-header">Personal Info</h3>
         <table>
              <colgroup>
  	            <col class = "field"/>
  	            <col class = "value"/>
  	        </colgroup>
  	        
             	<tr>
             	   <td>Surname:</td>
             	   <td><?php echo $member->getLastname();?></td>
             	</tr>

             	<tr>
             	   <td>First Name:</td>
             	   <td><?php echo $member->getFirstname(); ?></td>
             	</tr>

             	<tr>
             	   <td>Other Names:</td>
             	   <td><?php echo $member->getOthernames();?></td>
             	</tr>

             	<tr>
             	   <td>Birthday:</td>
             	   <td><?php $date = new PrettyDate($member->getDateOfBirth());
                     echo $date->getReadable();?></td>
             	</tr>

             	<tr>
             	   <td>Marital Status:</td>
             	   <td><?php echo $member->getMaritalStatus();?></td>
             	</tr>

              <tr>
                 <td>Occupation:</td>
                 <td><?php echo $member->getOccupation();?></td>
              </tr>

              <tr>
                 <td>Location:</td>
                 <td><?php echo $member->getHouseAddress();?></td>
              </tr>

              <tr>
                <td>City:</td>
                <td><?php echo $member->getCity();?></td>
              </tr>

         </table>
      </section>	

      <section class="item-view-section">
          <h3 class="item-view-section-header">Family Info</h3>
          <?php if($member->hasFamily()){ ?>
          <table>
              <colgroup>
              	<col class="field"/>
              	<col class="value"/>
              </colgroup>

              <?php if($member->getSpouse() != "Not Set Yet") {?>
              <tr>
                  
                  <td>Spouse:</td>
                  <td><?php echo $member->getSpouse(); ?></td>
                  
              </tr>
              <?php } ?>

              <?php if($member->getFather() != null) {?>
              <tr>
                  <td>Father:</td>
                  <td><?php echo UserProfile::find($member->getFather())->getFullname();?></td>
              </tr>
              <?php } ?>

              <?php if($member->getMother() != null){?>
              <tr>
                  <td>Mother:</td>
                  <td><?php echo UserProfile::find($member->getMother())->getFullname();?></td>
              </tr>
              <?php } ?>

              <?php if(count($member->getChildren()) > 0){ ?>
              <tr>
                  <td>Children:</td>
                  <td></td>
              </tr>
              <?php } ?>
          </table>
          <?php } else { ?>
          <p>No family records in our database</p>
          <?php } ?>
      </section>

      <section class="item-view-section">
          <h3 class="item-view-section-header">Contact Info:</h3>
          <table>
              <colgroup>
              	<col class="field">
              	<col class="value">
              </colgroup>

              <tr>
                  <td>Personal Contact:</td>
                  <td><?php echo $member->getCellphone();?></td>
              </tr>

              <tr>
                  <td>Home Phone:</td>
                  <td><?php echo $member->getHomePhone();?></td>
              </tr>

              <tr>
                  <td>Work Phone:</td>
                  <td><?php echo $member->getWorkPhone();?></td>
              </tr>

              <tr>
                  <td>Emergency Contact:</td>
                  <td><?php echo $member->getEmergencyContact();?></td>
              </tr>

              <tr>
                  <td>Email:</td>
                  <td><?php echo $member->getEmail(); ?></td>
              </tr>
          </table>
      </section>

      <section class="item-view-section">
          <h3 class="item-view-section-header">Membership Info</h3>
          <table>
              <colgroup>
                <col class = "field">
                <col class = "value">
              </colgroup>

              <tr>
                  <td>Date Joined: </td>
                  <td><?php $date = new PrettyDate($member->getDateJoined());
                      echo $date->getReadable();?></td>
              </tr>

              <tr>
                  <td>Position held in church:</td>
                  <td><?php echo $member->getChurchPosition(); ?></td>
              </tr>
          </table>
      </section>
   </section>
</section>

<?php require('app/views/includes/dialog_box.phpt'); ?>