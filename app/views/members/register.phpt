<?php require('app/views/churchadmin/menu.phpt'); ?>


<section id="main-content-container">
    <section id="main-content-body">
            <section id="member-regis-form">

              <?php require('app/views/includes/messages.phpt'); ?>

              <h2 class="member-regis-header">New Member Registration</h2>
              <form method="POST" action = "/members/process_registration" class = "mini-form">
              <p>
              <label for="username">Username:</label>
              <input type="text" name="username">
              </p>

              <p>
              <label for="password">Password:</label>
              <input type="password" name="password">
              </p>

              <p>
              <input type="submit" name="member_regis" value="Next">
              </p>
              </form>
            </section>
    </section>
</section>