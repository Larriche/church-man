<?php
if(churchAdminLoggedin()){
    require('/app/views/churchadmin/menu.phpt');
}
else if(memberLoggedIn()){
	require('/app/views/members/menu.phpt');
}

?>

<section id="main-content-container">
    <?php if(churchAdminLoggedin() || pastorLoggedIn()) { ?>
    <section id="member-menu" class="sub-menu">
      <ul>
          <li class="selected-tab"><a href="/members/view/<?php echo $member->getUserId();?>">Profile</a></li>
          <li><a href="/members/tithebook/<?php echo $member->getUserId();?>">Tithe Book</a></li>
          <li><a href="/members/pledgebook/<?php echo $member->getUserId();?>">Pledges</a></li>
      </ul>
    </section>
    <?php } ?>

	<section id="main-content-body">

	    <section class="member-profile-header">
	        <img src="<?php echo $member->getImageUrl();?>" width="100" height="100">
	        <h2 class="item-heading"><?php echo $member->getFullname();?></h2>
	        <ul>
            <li><a href="/members/edit/<?php echo $member->getUserId();?>" class="button-link">Edit Profile</a></li>
            <li><a href="/members/update_password/<?php echo $member->getUserId();?>" class="button-link">
              Update Password</a></li>
            </ul>
        </section>

	    <?php require('app/views/includes/messages.phpt');?>

        <section class="item-view-section">
	        <h3 class="item-view-section-header">Edit Member Profile</h3>
	        <form method="POST" action="/members/process_edit" enctype="multipart/form-data" class="max-form">
	        	<p>
					<label for="firstname">
					    Firstname:
					</label>
					<input type="text" name="firstname" value="<?php echo $member->getFirstname();?>">
				</p>

				<p>
					<label for="middlename">
					    Other Names:
					</label>
					<input type="text" name="other_names" value="<?php echo $member->getOthernames();?>">
				</p>

				<p>
	                <label for="surname">
	                    Surname:
	                </label>
	                <input type="text" name="surname" value="<?php echo $member->getLastname();?>">
	            </p>

	            <p>
					<label for="title">
					Title:
					</label>
					<input type="text" name="member_title" value="<?php echo $member->getTitle();?>">
				</p>

				<p>
					<label>
					Gender:
					</label>
					<select name="gender">
					<option value = "NOT SET YET">None Selected</option>

					<option value="MALE" <?php echo $member->getGender() == 'Male' ? "selected='1'" : "";?>>Male</option>
					<option value="FEMALE" <?php echo $member->getGender() == 'Female' ? "selected='1'" : "";?>>Female</option>
					</select>
				</p>

				<p>
					<label for="marital_status">
					Marital Status:
					</label>
					<select name="marital_status">
					<option value = "NOT SET YET">None Selected</option>

					<option value="SINGLE" <?php echo $member->getMaritalStatus() == 'Single' ? "selected='1'" : "";?>>Single</option>

					<option value="MARRIED" <?php echo $member->getMaritalStatus() == 'Married' ? "selected='1'" : "";?>>Married</option>
					</select>
				</p>

				<p>
					<label for="date_of_birth">
					Date of Birth:
					</label>
	                
	                <?php 
	                $date = new PrettyDate($member->getDateOfBirth());
	                $day = $date->getDay();
	                $month = $date->getMonthName();

	                $fields = ["birth_day","birth_month","birth_year"];
	                $year = $date->getYear();
	                include('app/views/includes/date_picker.phpt'); 
	                ?>
				</p>

				<p>
					<label for="date_of_birth">
					Date Joined:
					</label>
	                
	                <?php 
	                $date = new PrettyDate($member->getDateJoined());
	                $day = $date->getDay();
	                $month = $date->getMonthName();

	                $fields = ["joined_day", "joined_month", "joined_year"];
	                $year = $date->getYear();
	                include('app/views/includes/date_picker.phpt'); 
	                ?>
	                
				</p>

				<p>
		  			<label for="spouse">
					Spouse(if married):
					</label>

					<input type="text" name="spouse" 
					   value="<?php echo $member->getSpouse() == "Not Set Yet" ? '' : $member->getSpouse();?>">
				</p>

				<p>
					<label>
					Father:
					</label>

					<input type="text" name="father" 
					<?php if($member->getFather() != null){ ?>
					    value="<?php echo UserProfile::find($member->getFather())->getFullname();?>">
					<?php } ?>
					
									
				</p>

				<div id="father-select-div">
	            </div>

				<p>
					<label>Mother:</label>
					<input type="text" name="mother"
					<?php if($member->getFather() != null){ ?> 
					   value="<?php echo UserProfile::find($member->getMother())->getFullname();?>">
                    <?php } ?>
					
				</p>

				
				<p>
					<label for="occupation">
					Occupation:
					</label>
					<input type="text" name="occupation" value="<?php echo $member->getOccupation();?>">
				</p>

				<p>
					<label for="position">
					Postion in Church:
					</label>
					<input type="text" name="position" 
					 value="<?php echo $member->getChurchPosition() == 'None' ? '': $member->getChurchPosition();?>">
				</p>

				<p>
					<label for="home_phone">
					Home Phone:
					</label>
					<input type="text" name="home_phone" 
					  value="<?php echo $member->getHomePhone() == 'Not Set Yet' ? "": $member->getHomePhone();?>">
				</p>

				<p>
					<label for="home_phone">
					Work Phone:
					</label>
					<input type="text" name="work_phone" 
					  value="<?php echo $member->getWorkPhone() == 'Not Set Yet' ? "" : $member->getWorkPhone();?>">
				</p>

				<p>
					<label for="home_phone">
					Cell Phone:
					</label>
					<input type="text" name="cell_phone" 
					 value="<?php echo $member->getCellPhone() == 'Not Set Yet' ? "" : $member->getCellPhone();?>">
				</p>

				<p>
					<label for="home_phone">
					Emergency Contact:
					</label>
					<input type="text" name="emergency_phone" 
					  value="<?php echo $member->getEmergencyContact() == 'Not Set Yet' ? "" : $member->getEmergencyContact();?>">
				</p>

				<p>
					<label for="email">
					Email:
					</label>
					<input type="text" name="email" 
					  value="<?php echo $member->getEmail() == 'Not Set Yet' ? "" : $member->getEmail();?>">
				</p>

				<p>
					<label for="house_address">
					House Address:
					</label>
					<input type="text" name="house_address" 
					  value="<?php echo $member->getHouseAddress() == 'Not Set Yet' ? "" : $member->getHouseAddress();?>">
				</p>

				<p>
					<label for="city">
					City:
					</label>
					<input type="text" name="city" 
					  value="<?php echo $member->getCity() == 'Not Set Yet' ? "" : $member->getCity();?>">
				</p>

				<p>
				    <label for="profile_photo">New Profile Photo:</label>
				    <input type="file" name="profile_photo" id="image-input">
				</p>

				<input type="hidden" name="user_id" value="<?php echo $member->getUserId();?>">

				<p>
				<input type="submit" name="member_update" value="Update">
				</p>
	        </form>
	    </section>
	</section>
</section>