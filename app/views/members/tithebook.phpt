 <?php
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt'); 
}
else if(memberLoggedIn()){
    require('app/views/members/menu.phpt');
}
?>

<section id="main-content-container">
    <?php if(!memberLoggedIn()) { ?>
    <section id="member-menu" class="sub-menu">
      <ul>
          <li><a href="/members/view/<?php echo $member->getUserId();?>">Profile</a></li>
          <li class="selected-tab"><a href="/members/tithebook/<?php echo $member->getUserId();?>">Tithe Book</a></li>
          <li><a href="/members/pledgebook/<?php echo $member->getUserId();?>">Pledges</a></li>
      </ul>
    </section>
    <?php } ?>

    <section id="main-content-body">
       <section class="member-profile-header">
        <img src="<?php echo $member->getImageUrl();?>" width="100" height="100">
        <h2 class="item-heading"><?php echo $member->getFullname();?></h2>
        </section>

        <?php if(churchAdminLoggedIn()) { require('app/views/includes/messages.phpt');}?>

        <?php if(churchAdminLoggedIn()) { ?>
        <section class = "item-view-section">
           <h3 class="item-view-section-header">Record New Tithe Payment</h3>
           <form method="POST" action="/members/record_tithe">
               <p>
	               <label for="amount">Tithe Amount:</label>
	               <input type="text" name="amount">
               </p>

               <p>
                   <label for="date">Date</label>
                   
                  
                  <?php 
                  $day = "0";
                  $month = "0";
                  $year = "0";

                  $fields = ["pay_day","pay_month","pay_year"];
              
                  include('app/views/includes/date_picker.phpt'); 
                  ?>
                      
               </p>

               <input type="hidden" name = "payer_id" value="<?php echo $member->getUserId(); ?>">

               <p>
                  <input type="submit" name="tithe_add" value="Add" class="add-button">
               </p>

           </form>
        </section>
        <?php } ?>

        <section class="item-view-section">
            <h3 class="item-view-section-header">Tithe Records</h3>
            <table>
               <colgroup>
                   <col style="width: 200px"/>
                   <col style="width: 120px"/>
               </colgroup>

               <tr>
                  <th>Amount</th>
                  <th>Date Paid</th>
               </tr>

               <?php foreach($tithes as $tithe) { ?>
               <tr>
                  <td>Gh&#162; <?php echo number_format($tithe->getAmount(),2,".",","); ?> p</td>
                  <td><?php $date = new PrettyDate($tithe->getDate()); echo $date->getReadable(); ?></td>
                  <td><a class="item-link" href="/finances/edit_tithe/<?php echo $tithe->getId();?>">Edit</a></td>
                  <td><a class="danger" href="/finances/delete_tithe/<?php echo $tithe->getId();?>">Delete</a></td>
               </tr>
               <?php } ?>
            </table>
        </section>
    </section>
</section>