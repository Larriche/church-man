<section id="menu">
    <ul>
        <li>
        <a href="/authentication/password_update" 
           style="background-image:url(/resources/images/security_small.jpg)">
             Change Password
         </a>
         </li>

        <li>
        <a href="/members/view/<?php echo $_SESSION['user_id']; ?>" 
           style="background-image:url(/resources/images/male_small.png)">
             My Profile
         </a>
        </li>

        <li>
        <a href="/groups/view_all" 
           style="background-image:url(/resources/images/members_small.png)">
             My Groups
         </a>
        </li>

        
        <li>
        <a href="/prayer_requests/create" 
           style="background-image:url(/resources/images/pastor_small.png)">
             My Prayer Requests
         </a>
        </li>

        <li>
        <a href="/members/pledgebook/<?php echo $_SESSION['user_id'];?>" 
           style="background-image:url(/resources/images/pledges_small.jpg)">
             My Pledges
         </a>
        </li>

        <li>
        <a href="/members/tithebook/<?php echo $_SESSION['user_id'];?>" 
           style="background-image:url(/resources/images/tithe_small.jpg)">
             My Tithebook
        </a>
        </li>

        <li>
        <a href="/projects/view_all" 
           style="background-image:url(/resources/images/projects_small.png)">
             Projects
         </a>
        </li>

        <li>
        <a href="/events/view_all" 
           style="background-image:url(/resources/images/events_small.png)">
             Events
         </a>
        </li>

        <li>
        <a href="/announcements/view" 
           style="background-image:url(/resources/images/announce_small.png)">
             Announcements
         </a>
        </li>

    </ul>
</section>
