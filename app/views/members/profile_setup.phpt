<?php require('app/views/churchadmin/menu.phpt');?>

<section id="main-content-container">
	<section id="main-content-body">
	    
	    <?php require('app/views/includes/messages.phpt'); ?>

        <section class="item-view-section">
	        <h3 class="item-view-section-header">Setup Member Profile</h3>
	        <form method="POST" action="/members/process_profile_setup" enctype="multipart/form-data"
	            class="max-form">
	        	<p>
					<label for="firstname">
					    Firstname:
					</label>
					<input type="text" name="firstname">
				</p>

				<p>
					<label for="middlename">
					    Other Names:
					</label>
					<input type="text" name="other_names">
				</p>

				<p>
	                <label for="surname">
	                    Surname:
	                </label>
	                <input type="text" name="surname">
	            </p>

	            <p>
					<label for="title">
					Title:
					</label>
					<input type="text" name="member_title">
				</p>

				<p>
					<label>
					Gender:
					</label>
					<select name="gender">
					<option value = "NOT SET YET">None Selected</option>
					<option value="MALE">Male</option>
					<option value="FEMALE">Female</option>
					</select>
				</p>

				<p>
					<label for="marital_status">
					Marital Status:
					</label>
					<select name="marital_status">
					<option value = "NOT SET YET">None Selected</option>
					<option value="SINGLE">Single</option>
					<option value="MARRIED">Married</option>
					</select>
				</p>

				<p>
					<label for="date_of_birth">
					Date of Birth:
					</label>
	                
	                <?php 
	                $day = "0";
	                $month = "0";
	                $year = "0";

	                $fields = ["birth_day","birth_month","birth_year"];
	            
	                include('app/views/includes/date_picker.phpt'); 
	                ?>
				</p>

				<p>
					<label for="date_of_birth">
					Date Joined:
					</label>
	                
	                <?php 
                    $day = "0";
	                $month = "0";
	                $year = "0";

	                $fields = ["joined_day", "joined_month", "joined_year"];
	               
	                include('app/views/includes/date_picker.phpt'); 
	                ?>
	                
				</p>

				<p>
		  			<label for="spouse">
					Spouse(if married):
					</label>
					<input type="text" name="spouse" id="spouse">
				</p>

				<p>
					<label>
					Father:
					</label>
					<input type="text" name="father" id="father">
					
					
				</p>

				<div id="father-select-div">
	            </div>

				<p>
					<label>Mother:</label>
					<input type="text" name="mother" id="mother">
					
				</p>

				<p>
					<label for="occupation">
					Occupation:
					</label>
					<input type="text" name="occupation">
				</p>

				<p>
					<label for="occupation">
					Postion in Church:
					</label>
					<input type="text" name="position">
				</p>

				<p>
					<label for="home_phone">
					Home Phone:
					</label>
					<input type="text" name="home_phone">
				</p>

				<p>
					<label for="home_phone">
					Work Phone:
					</label>
					<input type="text" name="work_phone">
				</p>

				<p>
					<label for="home_phone">
					Cell Phone:
					</label>
					<input type="text" name="cell_phone">
				</p>

				<p>
					<label for="home_phone">
					Emergency Contact:
					</label>
					<input type="text" name="emergency_phone">
				</p>

				<p>
					<label for="email">
					Email:
					</label>
					<input type="text" name="email">
				</p>

				<p>
					<label for="house_address">
					House Address:
					</label>
					<input type="text" name="house_address">
				</p>

				<p>
					<label for="city">
					City:
					</label>
					<input type="text" name="city">
				</p>

				<p>
				    <label for="profile_photo">Profile Photo:</label>
				    <input type="file" name="profile_photo" id="image-input">
				</p>

				<input type="hidden" name="user_id" value="<?php echo $user_id;?>">

				<p>
				<input type="submit" name="profile_submit" value="Submit" class="member-regis-submit">
				</p>
	        </form>
	    </section>
	</section>
</section>

<script>
window.addEventListener('load',initialize);

function initialize()
{
	$(document).ready(function() {
		// load similar member names for autocomplete
	    $('#spouse').autocomplete({
	      source: function(request,response){
	      	$.getJSON("/ajax/members",{ name : $('#spouse').val()},response);
	      }
	    });

	    $('#father').autocomplete({
	      source: function(request,response){
	      	$.getJSON("/ajax/members",{ name : $('#father').val()},response);
	      }
	    });

	    $('#mother').autocomplete({
	      source: function(request,response){
	      	$.getJSON("/ajax/members",{ name : $('#mother').val()},response);
	      }
	    });
	});
}
</script>