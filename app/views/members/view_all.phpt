<?php
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt');
}
else{ // pastor logged in
    require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
  <section id="main-content-body">
     <section id="members-view">
         <h3 class="table-heading">Church Members</h3>
         <?php if(count($memberProfiles)) {?>

          <form method="POST" action="/members/search" style="margin-top: 10px; margin-bottom: 10px">
              <input type="text" name="name" placeholder="Member name">
              <input type="submit" value="Search">
          </form>

         <table>
             <colgroup>
              <col class = "counter"/>
              <col class = "image"/>
  	        <col class = "name"/>
  	        <col class = "date"/>
  	        </colgroup>

             <?php $index = $start; foreach($memberProfiles as $profile) {
              if($profile->isActive()) {?>
             	<tr>
             	   <td><?php echo $index++;?></td>
                 <td><img src='<?php echo $profile->getImageUrl();?>' height="60" width="60"></td>
             	   <td><a href = "/members/view/<?php echo $profile->getUserId();?>">
                  <?php echo $profile->getFullname();?>
                  </a></td>
             	   <td>member since <?php $date = new PrettyDate($profile->getDateJoined());
                     echo $date->getReadable();?></td>
             	</tr>
             <?php }} ?>
         </table>
         <?php } else {?>
          <hr />
          <p>No church members added yet</p>
         <?php } ?>

         <?php if(($start + $offset) <= $count) { ?>
         <section class="paginator-links">
           <p>
           <?php if( ($start - $offset) > 0) { ?>
           <a href="/members/view_all/<?php echo $start - $offset;?>" class="item-link">Prev</a>
           <?php } ?>

           <?php displayPaginatorLinks($count,$offset,14,(int)($start / $offset) + 1,'href="/members/view_all/');?>

           <?php if( ($start + $offset) <= $count) { ?>
           <a href="/members/view_all/<?php echo $start + $offset;?>" class="item-link">Next</a>
           <?php } ?>
           </p>
         </section>
         <?php } ?>
     </section>
  </section>
</section>