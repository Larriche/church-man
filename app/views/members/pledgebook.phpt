<?php 
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt');
}
else if(memberLoggedIn()){
    require('app/views/members/menu.phpt');
}
 
?>

<section id="main-content-container">
    <?php if(!memberLoggedIn()) { ?>
    <section id="member-menu" class="sub-menu">
      <ul>
          <li><a href="/members/view/<?php echo $member->getUserId();?>">Profile</a></li>
          <li><a href="/members/tithebook/<?php echo $member->getUserId();?>">Tithe Book</a></li>
          <li class="selected-tab"><a href="/members/pledgebook/<?php echo $member->getUserId();?>">Pledges</a></li>
      </ul>
    </section>
    <?php } ?>

    <section id="main-content-body">
        <section class="member-profile-header">
        <img src="<?php echo $member->getImageUrl();?>" width="100" height="100">
        <h2 class="item-heading"><?php echo $member->getFullname();?></h2>
        </section>

        <?php if(churchAdminLoggedIn()) require('app/views/includes/messages.phpt'); ?>

        <?php if(churchAdminLoggedIn()) { ?>
        <section class = "item-view-section">
           <h3 class="item-view-section-header">Record New Pledge</h3>
           <form method="POST" action="/members/add_pledge">
               <p>
                 <label for="amount">Amount:</label>
                 <input type="text" name="amount">
               </p>

               <p>
                   <label for="date">Project</label>
                   <select name="project">
                       <?php foreach ($projects as $project) { ?>
                       <option value="<?php echo $project->getId();?>"><?php echo $project->getName();?></option>
                       <?php } ?>
                   </select>                 
               </p>
                 
               <p>
                   <label>Date</label>
                   
                   <?php 
                    $day = "0";
                    $month = "0";
                    $year = "0";

                    $fields = ["pay_day","pay_month","pay_year"];
                
                    include('app/views/includes/date_picker.phpt'); 
                    ?>
               </p>

               <input type="hidden" name = "member_id" value="<?php echo $member->getUserId(); ?>">

               <p>
                  <input type="submit" name="add_pledge" value="Add" class="add-button">
               </p>

           </form>
        </section>
        <?php } ?>

        <section class="item-view-section">
            <h3 class="item-view-section-header">Pledges</h3>
            <table>
               <colgroup>
                 <col style="width:150px;"/>
                 <col style="width:120px;"/>
                 <col style="width:200px;"/>
                 <col style="width:50px;"/>
               </colgroup>

               <tr>
                  <th>Amount Pledged</th>
                  <th>Amount Paid</th>
                  <th>Project</th>
                  <th></th>
               </tr>

               <?php foreach($pledges as $pledge) { ?>
               <tr>
                  <td>Gh&#162; <?php echo $pledge->getPledgedAmount(); ?> p</td>
                  <td>Gh&#162; <?php echo $pledge->getAmountPaid(); ?> p</td>
                  <td><?php echo Project::find($pledge->getProject())->getName(); ?></td>
                  <td><a href = "/pledges/view/<?php echo $pledge->getId();?>" class="item-link">View</a></td>
                  <td><a href="/pledges/edit/<?php echo $pledge->getId();?>"
                    class="item-link">Edit</a></td>
                  <td><a href="/pledges/delete/<?php echo $pledge->getId();?>" class="danger">
                    Delete</a></td>
               </tr>
               <?php } ?>
            </table>
        </section>
    </section>
</section>

<?php 
   $dialogMessage = 'Are you sure you want to carry out this action?';
   require('app/views/includes/dialog_box.phpt'); 
?>

<script>
window.addEventListener('load',initialize);

function initialize()
{
  initializeDialogComponents();
}
</script>