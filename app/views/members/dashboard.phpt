<section id="dashboard-container">
    <section id="dashboard">
       <a href="/authentication/password_update">
	    <section class="dashboard-item">
	        <img src="/resources/images/security.jpg">
	        <div class="dashboard-text">Change Password</div>
	    </section>  
	   </a>
       
       <a href="/members/view/<?php echo $_SESSION['user_id'];?>">
	    <section class="dashboard-item">
	        <img src="/resources/images/add.png">
	        <div class="dashboard-text">My Profile</div>
	    </section>
	   </a>

       <a href="/groups/view_all">
	    <section class="dashboard-item">
	        <img src="/resources/images/search.png">
	        <div class="dashboard-text">My Groups</div>
	    </section>
	   </a>

       <a href="/prayer_requests/create">
	    <section class="dashboard-item">
            <img src="/resources/images/projects.png">
	    	<div class="dashboard-text">My Prayer Requests</div>
	    </section>
	   </a>

	    <a href="#">
		    <section class="dashboard-item">
	            <img src="/resources/images/money.png">
		    	<div class="dashboard-text">My Pledges</div>
		    </section>
	   </a>

	    <a href="#">
		    <section class="dashboard-item">
	            <img src="/resources/images/attendance.jpg">
		    	<div class="dashboard-text">My Tithebook</div>
		    </section>
	    </a>

	    <a href="/groups/create">
		    <section class="dashboard-item">
	            <img src="/resources/images/member.png">
		    	<div class="dashboard-text">Projects</div>
		    </section>
	    </a>

	    <a href="#">
		    <section class="dashboard-item">
	            <img src="/resources/images/pledges.jpg">
		    	<div class="dashboard-text">Events</div>
		    </section>
	    </a>

	    <a href="#r">
		    <section class="dashboard-item">
	            <img src="/resources/images/announce.png">
		    	<div class="dashboard-text">Announcements</div>
		    </section>
	    </a>
	</section>
</section>