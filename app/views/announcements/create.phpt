<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
	<section id="announcements-menu" class="sub-menu">
	    <ul>
	        <li class="selected-tab"><a href="/announcements/add_new">Add Announcements</a></li>
	        <li><a href="/announcements/view">View Announcements</a></li>
	    </ul>
    </section>

	<section id="main-content-body">
	   <section id="messages-container">
              <?php if(!empty($messages)) { ?>
                  <section class="messages" id="announcement-create-messages">
                  <span id = "close-button" onclick = "closeMessageBox();">X</span>
                  <?php foreach($messages as $message){ 
                      echo $message;
                  }?>

                  </section>

              <?php } ?>
        </section>
	   <section class="item-view-section">
	       <h3 class="item-view-section-header">Add New Announcement</h3>
	       <form method="POST" action = "/announcements/add_announcement" class="max-form">
	       <p>
	           <label for="title">Title:</label>
	           <input type="text" name="title">
	       </p>
	       
	        <p>
	           <label for="message">Message:</label>
	           <textarea name="message"></textarea>
	        </p>

	        <p>
	           <label for="date">Expiry Date:</label>
	           <?php 
                    $day = "0";
                    $month = "0";
                    $year = "0";

                    $fields = ["date_day","date_month","date_year"];
                
                    include('app/views/includes/date_picker.phpt'); 
                ?>
	        </p>

	        <p>
	           <input type = "submit" name="add_announcement" value="Add">
	        </p>
	       </form>
	   </section>
	</section>
</section>