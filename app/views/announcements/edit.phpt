<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="announcements-menu" class="sub-menu">
	    <ul>
	        <li><a href="/announcements/add_new">Add Announcements</a></li>
	        <li  class="selected-tab"><a href="/announcements/view">View Announcements</a></li>
	    </ul>
    </section>

	<section id="main-content-body">
	   <a class="button-link" href="/announcements/view">Back</a>
	   <section id="messages-container">
              <?php if(!empty($messages)) { ?>
                  <section class="messages" id="announcement-create-messages">
                  <span id = "close-button" onclick = "closeMessageBox();">X</span>
                  <?php foreach($messages as $message){ 
                      echo $message;
                  }?>

                  </section>

              <?php } ?>
        </section>
	   <section class="item-view-section">
	       <h3 class="item-view-section-header">Edit Announcement</h3>
	       <form method="POST" action = "/announcements/update_announcement" class="max-form">
	       <p>
	           <label for="title">Title:</label>
	           <input type="text" name="title" value="<?php echo $announcement->getTitle();?>">
	       </p>
	       
	        <p>
	           <label for="message">Message:</label>
	           <textarea name="message"><?php echo $announcement->getMessage();?></textarea>
	        </p>

	         <p>
	           <label for="date">Expiry Date:</label>
	           <?php 
	                $date = new PrettyDate($announcement->getExpiryDate());
                    $day = $date->getDay();
                    $month = $date->getMonthName();
                    $year = $date->getYear();

                    $fields = ["date_day","date_month","date_year"];
                
                    include('app/views/includes/date_picker.phpt'); 
                ?>
	        </p>

	        <input type="hidden" name="id" value="<?php echo $announcement->getId();?>">

	        <p>
	           <input type = "submit" name="update_announcement" value="Update">
	        </p>
	       </form>
	   </section>
	</section>
</section>