<?php 
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt');
}
elseif(memberLoggedIn()){
    require('app/views/members/menu.phpt');
}
else{// pastors
	require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
    <?php if(churchAdminLoggedIn()) { ?>
	<section id="announcements-menu" class="sub-menu">
	    <ul>
	        <li><a href="/announcements/add_new">Add Announcements</a></li>
	        <li class="selected-tab"><a href="/announcements/view">View Announcements</a></li>
	    </ul>
    </section>
    <?php } ?>

	<section id="main-content-body">
	   <h2 class="item-heading">Announcements</h3> 
	   <?php if(count($announcements) == 0) {?>
	   <p>There are no announcements to display</p>
	   <?php } else { foreach($announcements as $announcement){ ?>
	   <section class = 'item-view-section'>
	         <?php if(churchAdminLoggedIn()) { ?>
	         <section class="announcement-links">
		         <a href="/announcements/edit/<?php echo $announcement->getId();?>" class="button-link">Edit</a>
		         <a href="/announcements/delete/<?php echo $announcement->getId();?>" class="danger">Delete</a>
		     </section>
	         <?php } ?>
	         <h3 class="item-view-section-header"><?php echo $announcement->getTitle();?></h3>
	         <p><?php echo $announcement->getMessage();?></p>
	   </section>
	   <?php } } ?>

	</section>
</section>

<?php 
   require('app/views/includes/dialog_box.phpt'); 
?>

