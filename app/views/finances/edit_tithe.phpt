<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
  <section id="member-menu" class="sub-menu">
    <ul>
        <li><a href="/finances/offerings">Offerings</a></li>
        <li class="selected-tab"><a href="/finances/tithes">Tithes</a></li>
        <li><a href="/finances/totals">Other Income/Expenditure</a></li>
    </ul>
  </section>

  <section id="main-content-body">
    <a href="/finances/tithes" class="button-link">Back to tithes</a>
    <?php require('app/views/includes/messages.phpt');?>
    <section class="item-view-section">
      <h3 class="item-view-section-header">Edit tithe</h3>
      <form method="POST" action="/finances/process_edit_tithe" class="max-form">
        <p>
          <label for="amount">Amount(Gh&#162;)</label>
          <input type="text" name="amount" value="<?php echo $tithe->getAmount();?>"/>
        </p>

        <p>
          <label for="date">Date</label>

          <?php 
            $date = new PrettyDate($tithe->getDate());

            $day = $date->getDay();
            $month = $date->getMonthName();
            $year = $date->getYear();

            $fields = ["date_day","date_month","date_year"];
        
            include('app/views/includes/date_picker.phpt'); 
           ?>
        </p>

        <input type="hidden" name="tithe_id" value="<?php echo $tithe->getId();?>" />

        <p>
          <input type="submit" name="edit_tithe" value="Update">
        </p>
      </form>
    </section>
  </section>
</section>