<?php
if(churchAdminLoggedIn()) { 
    require('app/views/churchadmin/menu.phpt'); 
}
else{
    require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
    <section id="finance-menu" class="sub-menu">
      <ul>
          <li><a href="/finances/offerings">Offerings</a></li>
          <li><a href="/finances/tithes">Tithes</a></li>
          <li class="selected-tab"><a href="/finances/totals">Other Income/Expenditure</a></li>
      </ul>
    </section>


    <section id="main-content-body">   
        <section class="member-profile-header">
            <h2 class="item-heading">Incomes and Expenditures</h2>
            <ul>
              <?php if(churchAdminLoggedIn()) { ?>
              <li><a href="/finances/record_new" class="button-link">Record New</a></li>
              <?php } ?>
              <li><a href="/finances/details" class="button-link">View Details</a></li>
              <li><a href="/finances/totals" class="button-link">Stats</a></li>
            </ul>
        </section>

        <section class= "item-view-section">
            <h3 class="item-view-section-header">Income</h3>
            <section id="offeringsChartContainer" class="chart-container scrollable">
            </section>
        </section>

        <section class= "item-view-section">
            <h3 class="item-view-section-header">Expenditure</h3>
            <section id="tithesChartContainer" class="chart-container scrollable">
            </section>
        </section>
    </section>
</section>



<?php
$graphJs  = '<script>window.onload = function () {
  var chart = new CanvasJS.Chart("offeringsChartContainer", {
    title:{
      text: "",
      fontColor: "rgba(100,149,237,.8)"              
    },
    axisY :{
        lineColor: "#3CB371",
        gridColor: "#F0FFFF", 
        },
        axisX :{
        lineColor: "#3CB371",
        gridColor: "#F0FFFF", 
        },
    data: [              
    {
      // Change type to "doughnut", "line", "splineArea", etc.
      type: "spline",
      color: "rgba(152,251,152,0.8)",
      dataPoints: [';

foreach($income as $month => $incomeTotal){
      $graphJs .= '{ label: '."'".$month."'".',y:'.$incomeTotal.'},';
}
 
$graphJs = rtrim($graphJs,',');      

$graphJs .='
      ]
    }
    ]
  });
  chart.render();
  

  var chart = new CanvasJS.Chart("tithesChartContainer", {
    title:{
      text: "",
      fontColor: "rgba(100,149,237,.8)"              
    },
    axisY :{
        lineColor: "#3CB371",
        gridColor: "#F0FFFF", 
        },
        axisX :{
        lineColor: "#3CB371",
        gridColor: "#F0FFFF", 
        },
    data: [              
    {
      // Change type to "doughnut", "line", "splineArea", etc.
      type: "spline",
      color: "rgba(152,251,152,0.8)",
      dataPoints: [';

foreach($expenses as $month => $expenseTotal){
      $graphJs .= '{ label: '."'".$month."'".',y:'.$expenseTotal.'},'."\n";
}
 
$graphJs = rtrim($graphJs,',');      

$graphJs .='
      ]
    }
    ]
  });
  chart.render();
  
};</script>';
echo $graphJs;
?>