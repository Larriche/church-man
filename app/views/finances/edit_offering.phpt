<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
  <section id="main-content-body">
    <a href="/finances/offerings" class="button-link">Back to Offerings</a>
    <?php require('app/views/includes/messages.phpt');?>
    <section class="item-view-section">
      <h3 class="item-view-section-header">Edit Offering</h3>
      <form method="POST" action="/finances/process_edit_offering" class="max-form">
        <p>
          <label for="amount">Amount(Gh&#162;)</label>
          <input type="text" name="amount" value="<?php echo $offering->getAmount();?>"/>
        </p>

        <p>
          <label for="date">Date</label>

          <?php 
            $date = new PrettyDate($offering->getDate());

            $day = $date->getDay();
            $month = $date->getMonthName();
            $year = $date->getYear();

            $fields = ["date_day","date_month","date_year"];
        
            include('app/views/includes/date_picker.phpt'); 
           ?>
        </p>

        <input type="hidden" name="offering_id" value="<?php echo $offering->getId();?>" />

        <p>
          <input type="submit" name="edit_offering" value="Update">
        </p>
      </form>
    </section>
  </section>
</section>