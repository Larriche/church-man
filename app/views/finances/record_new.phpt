<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="finance-menu" class="sub-menu">
      <ul>
          <li><a href="/finances/offerings">Offerings</a></li>
          <li><a href="/finances/tithes">Tithes</a></li>
          <li class="selected-tab"><a href="/finances/totals">Other Income/Expenditure</a></li>
      </ul>
    </section>


    <section id="main-content-body">   

       <section class="member-profile-header">
            <h2 class="item-heading">Incomes and Expenditures</h2>
            <ul>
              <?php if(churchAdminLoggedIn()) { ?>
              <li><a href="/finances/record_new" class="button-link">Record New</a></li>
              <?php } ?>
              <li><a href="/finances/details" class="button-link">View Details</a></li>
              <li><a href="/finances/totals" class="button-link">Stats</a></li>
            </ul>
        </section>

        <?php include('app/views/includes/messages.phpt');?>
        
        <section class="item-view-section">
          <h3 class="item-view-section-header">Record New Transaction</h3>

          <form method="POST" action="/finances/add_new" class="max-form">
            <p>
              <label for="amount">Amount(Gh&#162;):</label>
              <input type="text" name="amount">
            </p>

            <p>
              <label for="type">Type:</label>
              <select name="type">
                <option value="INCOME">Income</option>
                <option value="EXPENSE">Expenditure</option>
              </select>
            </p>

            <p>
              <label for="date_made">Date</label>

              <?php 
                $day = "0";
                $month = "0";
                $year = "0";

                $fields = ["date_day","date_month","date_year"];
                
                include('app/views/includes/date_picker.phpt'); 
            ?>
            </p>

            <p>
              <label for="description">Description</label>
              <textarea name="description"></textarea>
            </p>

            <input type="submit" name="record_transaction" value="Save">
          </form>
        </section>
    </section>
</section>
