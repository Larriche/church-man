<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="member-menu" class="sub-menu">
      <ul>
          <li><a href="/finances/offerings">Offerings</a></li>
          <li class="selected-tab"><a href="/finances/tithes">Tithes</a></li>
          <li><a href="/finances/totals">Other Income/Expenditure</a></li>
      </ul>
    </section>

    <section id="main-content-body">   
        <?php require('app/views/includes/messages.phpt');?>

        <section class= "item-view-section">
            <h3 class="item-view-section-header">Recent Tithes</h3>
            <?php if(count($tithes) > 0) {?>
            <section id="tithes-chart" class="chart-container">
            </section>
            <?php } else{?>
            <p>No tithes recorded yet</p>
            <?php } ?>
        </section>

        <section class="item-view-section">
           <h3 class="item-view-section-header">Tithes By Month</h3>
           <?php if(count($monthlyTithes) > 0) {?>
            <section id="tithes-for-year-chart" class="chart-container">
            </section>
            <?php } else{?>
            <p>No tithes recorded yet</p>
            <?php } ?>
        </section>

        <?php if(churchAdminLoggedIn()) {?>
        <section class="item-view-section">
          <h3 class="item-view-section-header">Record Anonymous Tithes(Tithes by unregistered members)</h3>
          <form method="POST" action="/finances/record_tithes" class="max-form">
            <p>
              <label for="amount">Amount(Gh&#162;)</label>
              <input type="text" name="amount"/>
            </p>

            <p>
              <label for="date_paid">Payment Date</label>

              <?php
                $day = "00";
                $month = "00";
                $year = "00";

                $fields = ["date_day","date_month","date_year"];
            
                include('app/views/includes/date_picker.phpt');
              ?>
            </p>

            <p>
              <input type="submit" name="record_tithe" value="Save"/>
            </p>
          </form>
        </section>
        <?php } ?>

        <?php if(count($anonTithes) > 0){ ?>
          <section class="item-view-section">
           <h3 class="item-view-section-header">Anonymous Tithes</h3>
          <table>
            <colgroup>
              <col style="width: 150px"/>
              <col style="width: 150px"/>
              <col style="width: 50px"/>
            </colgroup>
            <tr>
              <th>Amount</th>
              <th>Date</th>
              <th></th>
            </tr>
            <?php foreach($anonTithes as $tithe){ ?>
              <tr>
                <td>Gh&#162; <?php echo number_format($tithe->getAmount(),2,'.',',');?> p</td>
                <td><?php $date = new PrettyDate($tithe->getDate()); echo $date->getReadable();?></td>

                <?php if(churchAdminLoggedIn()) { ?>
                <td><a href="/finances/edit_tithe/<?php echo $tithe->getId(); ?>" class="item-link">Edit</a></td>
                <td><a href="/finances/delete_tithe/<?php echo $tithe->getId();?>" class="danger">Delete</a></td>
                <?php } ?>
              </tr>
            <?php } ?>
          </table>
          </section>
        <?php } ?>

        
    </section>
</section>

<?php 
   $dialogMessage = 'Are you sure you want to carry out this action?';
   require('app/views/includes/dialog_box.phpt'); 
?>

<script>
window.addEventListener('load',initialize);

function initialize()
{
  initializeDialogComponents();
}
</script>

<?php
$graphJs  = '<script>
  window.onload = function () {
  if(document.getElementById("tithes-chart")){
  var chart = new CanvasJS.Chart("tithes-chart", {
    title:{
      text: "",
      fontColor: "rgba(100,149,237,.8)"              
    },
    axisY :{
        lineColor: "#3CB371",
        gridColor: "#F0FFFF", 
        },
        axisX :{
        lineColor: "#3CB371",
        gridColor: "#F0FFFF", 
        },
    data: [              
    {
      // Change type to "doughnut", "line", "splineArea", etc.
      type: "bar",
      color: "rgba(152,251,152,0.3)",
      dataPoints: [';

$tithes = array_reverse($tithes);
foreach($tithes as $day => $total){
    $date = new PrettyDate($day);
    $graphJs .= '{ label: '."'".$date->getReadable()."'".',y:'.$total.'},';
}
 
$graphJs = rtrim($graphJs,',');      

$graphJs .='
      ]
    }
    ]
  });
  chart.render();
}

  if(document.getElementById("tithes-for-year-chart")){
    var chart = new CanvasJS.Chart("tithes-for-year-chart", {
    title:{
      text: "",
      fontColor: "rgba(100,149,237,.8)"              
    },
    axisY :{
        lineColor: "#3CB371",
        gridColor: "#F0FFFF", 
        },
        axisX :{
        lineColor: "#3CB371",
        gridColor: "#F0FFFF", 
        },
    data: [              
    {
      // Change type to "doughnut", "line", "splineArea", etc.
      type: "spline",
      color: "rgba(152,251,152,0.8)",
      dataPoints: [';

foreach($monthlyTithes as $month => $expenseTotal){
      $graphJs .= '{ label: '."'".$month."'".',y:'.$expenseTotal.'},'."\n";
}
 
$graphJs = rtrim($graphJs,',');      

$graphJs .='
      ]
    }
    ]
  });
  chart.render();

  }
  
}
</script>';
echo $graphJs;
?>

