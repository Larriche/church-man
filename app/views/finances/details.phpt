<?php
if(churchAdminLoggedIn()){
	require('app/views/churchadmin/menu.phpt');
}
else if(pastorLoggedIn()){
	require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
   <section id="finance-menu" class="sub-menu">
      <ul>
          <li><a href="/finances/offerings">Offerings</a></li>
          <li><a href="/finances/tithes">Tithes</a></li>
          <li class="selected-tab"><a href="/finances/totals">Other Income/Expenditure</a></li>
      </ul>
    </section>

  <section id="main-content-body">
    <section class="member-profile-header">
        <h2 class="item-heading">Incomes and Expenditures</h2>
        <ul>
          <?php if(churchAdminLoggedIn()) { ?>
          <li><a href="/finances/record_new" class="button-link">Record New</a></li>
          <?php } ?>
          <li><a href="/finances/details" class="button-link">View Details</a></li>
          <li><a href="/finances/totals" class="button-link">Stats</a></li>
        </ul>
    </section>

    <section class="item-view-section">
      <h3 class="item-view-section-header">Income</h3>
      <table>
        <colgroup>
        	<col style="width: 150px"/>
        	<col style="width: 150px"/>
        	<col style="width: 250px"/>
        </colgroup>
        <tr>
          <td>Amount</td>
          <td>Date</td>
          <td>Note</td>
        </tr>
        <?php if(count($income) > 0) {
        	foreach($income as $inc){?>
        <tr>
          <td>Gh&#162;<?php echo number_format($inc->getAmount(),2,'.',',');?>p</td>
          <td><?php $date = new PrettyDate($inc->getDateRecorded()); echo $date->getReadable();?></td>
          <td><?php echo $inc->getDescription();?></td>
          <td><a href="/finances/edit_others/<?php echo $inc->getId();?>" class="item-link">Edit</a></td>
          <td><a href="/finances/delete_others/<?php echo $inc->getId();?>" class="danger">Delete</a></td>
        </tr>
        <?php } } ?>
      </table>
    </section>

    <section class="item-view-section">
      <h3 class="item-view-section-header">Expenses</h3>
      <table>
        <colgroup>
        	<col style="width: 150px"/>
        	<col style="width: 150px"/>
        	<col style="width: 250px"/>
        </colgroup>
        <tr>
          <td>Amount</td>
          <td>Date</td>
          <td>Note</td>
        </tr>
        <?php if(count($income) > 0) {
        	foreach($expenses as $expense){?>
        <tr>
          <td>Gh&#162;<?php echo number_format($expense->getAmount(),2,'.',',');?>p</td>
          <td><?php $date = new PrettyDate($expense->getDateRecorded()); echo $date->getReadable();?></td>
          <td><?php echo $expense->getDescription();?></td>
          <td><a href="/finances/edit_others/<?php echo $expense->getId();?>" class="item-link">Edit</a></td>
          <td><a href="/finances/delete_others/<?php echo $expense->getId();?>" class="danger">Delete</a></td>
        </tr>
        <?php } } ?>
      </table>
    </section>
  </section>
</section>

<?php 
   $dialogMessage = 'Are you sure you want to carry out this action?';
   require('app/views/includes/dialog_box.phpt'); 
?>

<script>
window.addEventListener('load',initialize);

function initialize()
{
  initializeDialogComponents();
}
</script>