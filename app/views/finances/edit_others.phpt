<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
  <section id="finance-menu" class="sub-menu">
    <ul>
        <li><a href="/finances/offerings">Offerings</a></li>
        <li><a href="/finances/tithes">Tithes</a></li>
        <li class="selected-tab"><a href="/finances/totals">Other Income/Expenditure</a></li>
    </ul>
  </section>

  <section id="main-content-body">
    <a href="/finances/details" class="button-link">Back to Financial Records</a>
    <?php require('app/views/includes/messages.phpt');?>
    <section class="item-view-section">
      <h3 class="item-view-section-header">Edit Financial Record</h3>
      <form method="POST" action="/finances/process_edit_others" class="max-form">
        <p>
          <label for="amount">Amount(Gh&#162;)</label>
          <input type="text" name="amount" value="<?php echo $financialRecord->getAmount();?>"/>
        </p>

        <p>
          <label for="date">Date</label>

          <?php 
            $date = new PrettyDate($financialRecord->getDateRecorded());

            $day = $date->getDay();
            $month = $date->getMonthName();
            $year = $date->getYear();

            $fields = ["date_day","date_month","date_year"];
        
            include('app/views/includes/date_picker.phpt'); 
           ?>
        </p>

        <input type="hidden" name="financialRecord_id" value="<?php echo $financialRecord->getId();?>" />

        <p>
          <input type="submit" name="process_edit" value="Update">
        </p>
      </form>
    </section>
  </section>
</section>