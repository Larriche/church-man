<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="member-menu" class="sub-menu">
      <ul>
          <li class='selected-tab'><a href="/finances/offerings">Offerings</a></li>
          <li><a href="/finances/tithes">Tithes</a></li>
          <li><a href="/finances/totals">Other Income/Expenditure</a></li>
      </ul>
    </section>

    <section id="main-content-body">
        <?php if(churchAdminLoggedIn()) { ?>
        <section id="messages-container">
        <?php if(!empty($messages)) { ?>
            <section class="messages" id="login-messages">
            <span id = "close-button" onclick = "closeMessageBox();">X</span>
            <?php foreach($messages as $message){ 
                echo $message;
            }?>
            </section>
        <?php } ?>
        </section>

        <section class = "item-view-section">
           <h3 class="item-view-section-header">Record New Offering</h3>
           <form method="POST" action="/finances/record_offering" class="max-form">
               <p>
	               <label for="amount">Amount:</label>
	               <input type="text" name="amount">
               </p>

               <p>
                   <label for="date">Date Taken:</label>

                   <?php 
                    $day = "0";
                    $month = "0";
                    $year = "0";

                    $fields = ["date_day","date_month","date_year"];
                
                    include('app/views/includes/date_picker.phpt'); 
                  ?>
                                  
               </p>

               <p>
                  <input type="submit" name="add_offering" value="Add" class="add-button">
               </p>

           </form>
        </section>
        <?php } ?>

        <section class= "item-view-section">
            <h3 class="item-view-section-header">Offerings</h3>
            <?php if(count($offerings) > 0) { ?>
            <section id="offerings-chart" class="chart-container">
            </section>
            <?php } else{ ?>
            <p>No offerings recorded yet</p>
            <?php } ?>

            <?php if(count($offerings) > 0){ ?>
              <section class="item-view-section">
              <table>
                <colgroup>
                  <col style="width: 150px"/>
                  <col style="width: 150px"/>
                  <col style="width: 100px"/>
                </colgroup>
                <tr>
                  <th>Amount</th>
                  <th>Date</th>
                  <th></th>
                </tr>
                <?php foreach($offerings as $offering){ ?>
                  <tr>
                    <td>Gh&#162; <?php echo number_format($offering->getAmount(),2,'.',',');?> p</td>
                    <td><?php $date = new PrettyDate($offering->getDate()); echo $date->getReadable();?></td>

                    <?php if(churchAdminLoggedIn()) {?>
                    <td><a href="/finances/edit_offering/<?php echo $offering->getId(); ?>" class="item-link">Edit</td>
                    <?php } ?>
                  </tr>
                <?php } ?>
              </table>
              </section>
            <?php } ?>
        </section>
    </section>
</section>

<?php
$graphJs  = '<script>window.onload = function () {
  if(document.getElementById("offerings-chart")){
  var chart = new CanvasJS.Chart("offerings-chart", {
    title:{
      text: "",
      fontColor: "rgba(100,149,237,.8)"              
    },
    axisY :{
        lineColor: "#3CB371",
        gridColor: "#F0FFFF", 
        },
        axisX :{
        lineColor: "#3CB371",
        gridColor: "#F0FFFF", 
        },
    data: [              
    {
      // Change type to "doughnut", "line", "splineArea", etc.
      type: "bar",
      color: "rgba(152,251,152,0.3)",
      dataPoints: [';

$offerings = array_reverse($offerings);
foreach($offerings as $offering){
      $date = new PrettyDate($offering->getDate());
      $graphJs .= '{ label: '."'".$date->getReadable()."'".',y:'.$offering->getAmount().'},';
}
 
$graphJs = rtrim($graphJs,',');      

$graphJs .='
      ]
    }
    ]
  });
  chart.render();
  
}}</script>';
echo $graphJs;
?>