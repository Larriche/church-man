<!DOCTYPE html>
<html lang="en">
  <head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="/public/css/browser_reset.css">
  <link rel="stylesheet" href="/public/css/style.css" type="text/css">
  <!-- <link rel="stylesheet" href="/public/css/auto-complete.css" type="text/css"> -->
  <link rel="stylesheet" href="/public/css/jquery-ui.min.css" type="text/css">
  <?php if(isset($styles))foreach ($styles as $style) {?>
  <link rel="stylesheet" href="/public/css/<?php echo $style; ?>" type = "text/css">
  <?php } ?>
  </head>
  <body>
   <header class="site-header">
       <h3 class="site-logo">churchMan+</h3>
       <?php if(userLoggedIn()) {?>
       <section id="user-menu">
           <p><span class='username'><?php echo User::find($_SESSION['user_id'])->getUsername();?> </span>logged in as <?php echo User::readableRole($_SESSION['user_role']);?></p>
           <a href="/authentication/logout">Log Out</a>
       </section>
       <?php } ?>
   </header>
