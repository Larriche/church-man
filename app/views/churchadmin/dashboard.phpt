<section id="dashboard-container">
    <section id="dashboard">
       <a href="/authentication/password_update">
	    <section class="dashboard-item">
	        <img src="/resources/images/security.jpg">
	        <div class="dashboard-text">Change Password</div>
	    </section>  
	   </a>
       
       <a href="/members/register">
	    <section class="dashboard-item">
	        <img src="/resources/images/add.png">
	        <div class="dashboard-text">Add Church Member</div>
	    </section>
	   </a>

       <a href="/members/view_all">
	    <section class="dashboard-item">
	        <img src="/resources/images/search.png">
	        <div class="dashboard-text">View Church Members</div>
	    </section>
	   </a>

       <a href="/projects/create">
	    <section class="dashboard-item">
            <img src="/resources/images/projects.png">
	    	<div class="dashboard-text">Church Projects</div>
	    </section>
	   </a>

	    <a href="#">
		    <section class="dashboard-item">
	            <img src="/resources/images/money.png">
		    	<div class="dashboard-text">Finances</div>
		    </section>
	   </a>

	    <a href="#">
		    <section class="dashboard-item">
	            <img src="/resources/images/attendance.jpg">
		    	<div class="dashboard-text">Attendance</div>
		    </section>
	    </a>

	    <a href="/groups/create">
		    <section class="dashboard-item">
	            <img src="/resources/images/member.png">
		    	<div class="dashboard-text">Groups</div>
		    </section>
	    </a>

	    <a href="/events/view_all">
		    <section class="dashboard-item">
	            <img src="/resources/images/calendar.png">
		    	<div class="dashboard-text">Events</div>
		    </section>
	    </a>

	    <a href="announcements/view">
		    <section class="dashboard-item">
	            <img src="/resources/images/announce.png">
		    	<div class="dashboard-text">Announcements</div>
		    </section>
	    </a>
	</section>
</section>