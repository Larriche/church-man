<?php require('app/views/sysadmin/menu.phpt'); ?>

<section id="main-content-container" class="new-admin-container">
    <section id="main-content-body">
      <section class="item-view-section">
        <?php require('app/views/includes/messages.phpt'); ?>
        <h3 class="item-view-section-header">Register new Church Administrator</h3>
        <p>
        <form method="POST" action="/churchadmin/process_registration" id="church-admin-regis-form"
            class="mini-form">
        <label>Username:</label>
        <input type="text" name="username" onfocus="clearErrorMessages();">
        </p>

        <p>
        <label>Password:</label>
        <input type="password" name="password" onfocus="clearErrorMessages();">
        </p>


        <p>
        <input type="submit" value="Submit" name="churchadmin_regis">
        </p>
        </form>
      </section>
    </section>
</section>