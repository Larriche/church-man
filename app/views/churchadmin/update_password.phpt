<div id="dashboard-container">
        <section id="member-regis-form">
          <section id="messages-container">
            <?php if(!empty($messages)) { ?>
                <section class="messages" id="church-admin-regis-messages">
                <?php foreach($messages as $message){ 
                    echo $message;
                }?>
                </section>
            <?php } ?>
            </section>
          <h2 class="member-regis-header">Update Password</h2>
          <form method="POST" action = "/churchadmin/process_update_password">
          <p>
          <label for="username">Current Password:</label>
          <input type="password" name="current_password">
          </p>

          <p>
          <label for="password">New Password:</label>
          <input type="password" name="new_password">
          </p>

          <p>
          <input type="submit" name="churchadmin_pass_change" value="Submit">
          </p>
          </form>
        </section>
</div>