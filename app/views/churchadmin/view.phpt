<?php require('app/views/sysadmin/menu.phpt'); ?>

<section id="main-content-container">    
    <section id="main-content-body">
        <section class="item-view-section">
           <h3 class="item-view-section-header">Church Admins</h3>
           <table>
           <?php  foreach($churchadmins as $churchadmin) { ?>
              <tr>
                 <td><?php echo $churchadmin->getUsername(); ?></td>
                 <td><a class="item-link" href="/churchadmin/update_password/<?php echo $churchadmin->getId();?>">Update Password</a></td>
                 <td><a href="/churchadmin/delete/<?php echo $churchadmin->getId();?>" class="danger">Delete</a></td>
              </tr>
           <?php } ?> 
           </table> 
        </section>
    </section>

    <?php
    $dialogMessage = 'Are you sure you want to delete this user?'; 
    require('app/views/includes/dialog_box.phpt'); 
    ?>
</section>

<script>
window.addEventListener('load',initialize);

function initialize()
{
    initializeDialogComponents();
}
</script>