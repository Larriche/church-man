<section id="menu">
    <ul>
        <li>
        <a href="/authentication/password_update" 
           style="background-image:url(/resources/images/security_small.jpg)">
             Change Password
         </a>
         </li>

        <li>
        <a href="/members/register" 
           style="background-image:url(/resources/images/add_small.png)">
             Add Member
         </a>
        </li>

        <li>
        <a href="/members/view_all" 
           style="background-image:url(/resources/images/search_small.png)">
             View Members
         </a>
        </li>

        <li>
        <a href="/pastors/register" 
           style="background-image:url(/resources/images/pastor_small.png)">
             Pastors
         </a>
        </li>

        <li>
        <a href="/groups/create" 
           style="background-image:url(/resources/images/members_small.png)">
             Groups
         </a>
        </li>

        <li>
        <a href="/projects/create" 
           style="background-image:url(/resources/images/projects_small.png)">
             Church Projects
         </a>
        </li>

        <li>
        <a href="/finances/offerings" 
           style="background-image:url(/resources/images/money_small.png)">
             Finances
         </a>
        </li>

        <li>
        <a href="/events/view_all" 
           style="background-image:url(/resources/images/events_small.png)">
             Events
         </a>
        </li>
        
        <li>
        <a href="/announcements/view" 
           style="background-image:url(/resources/images/announce_small.png)">
             Announcements
         </a>
        </li>

    </ul>
</section>