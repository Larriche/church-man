<section id="main-content-body" class="centered">
    <section class="centered-form">
        <form method="POST" action="/authentication/process_login" id="login-form" class="mini-form">
        
        <?php require('app/views/includes/messages.phpt'); ?>
        
        <h3 class="centered-form-header">Log In</h3>
        <p>
        <label>Username:</label>
        <input required type="text" name="username">
        </p>

        <p>
        <label>Password:</label>
        <input required type="password" name="password">
        </p>

        <p>
        <input type="hidden" name="role" value="<?php echo $role;?>">
        </p>

        <p>
        <input type="submit" value="Log In" name="login_submit">
        </p>
        </form>
    </section>
</section>