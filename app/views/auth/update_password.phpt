<section id="main-content-container">
    <section id="auth-form">
        <form method="POST" action="/authentication/process_password_update" id="login-form"
         onsubmit="return validateLoginForm();" class="mini-form">
            
            <?php require('app/views/includes/messages.phpt'); ?>

            <h3 class="auth-form-header">Update Password</h3>

            <p>
            <label>Current Password:</label>
            <input type="password" name="current_password" onfocus="clearErrorMessages();">
            </p>

            <p>
            <label for="password">New Password:</label>
            <input type="password" name="new_password">
            </p>

            <p>
            <input type="submit" value="Update" name="update_password">
            </p>

        </form>
    </section>
</section>
