<section id="control-panel-container">
    <section id="control-panel">
       <a href="/authentication/login/sys_admin">
	    <section class="control-panel-item">
	        <img src="/resources/images/sysadmin.png">
	        <div class="control-panel-text">System Admin</div>
	    </section>  
	   </a>
       
       <a href="/authentication/login/church_admin">
	    <section class="control-panel-item">
	        <img src="/resources/images/churchadmin.png">
	        <div class="control-panel-text">Church Admin</div>
	    </section>
	   </a>

       <a href="/authentication/login/pastor">
	    <section class="control-panel-item">
	        <img src="/resources/images/pastor.png">
	        <div class="control-panel-text">Pastor</div>
	    </section>
	   </a>

       <a href="/authentication/login/member">
	    <section class="control-panel-item">
            <img src="/resources/images/member.png">
	    	<div class="control-panel-text">Members</div>
	    </section>
	   </a>
	</section>
</section>
