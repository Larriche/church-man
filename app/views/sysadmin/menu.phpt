<section id="menu">
    <ul>
        <li>
        <a href="/authentication/password_update" 
           style="background-image:url(/resources/images/security_small.jpg)">
             Change Password
         </a>
         </li>

        <li>
        <a href="/churchadmin/register" 
           style="background-image:url(/resources/images/add_small.png)">
             Add Church Admins
         </a>
        </li>

        <li>
        <a href="/churchadmin/view" 
           style="background-image:url(/resources/images/search_small.png)">
             View Church Admins
         </a>
        </li>
    </ul>
</section>
