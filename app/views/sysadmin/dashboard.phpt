<section id="dashboard-container">
    <section id="dashboard" style="margin-left: 100px">
       <a href="/authentication/password_update">
	    <section class="dashboard-item">
	        <img src="/resources/images/security.jpg">
	        <div class="dashboard-text">Change Password</div>
	    </section>  
	   </a>
       
       <a href="/churchadmin/register">
	    <section class="dashboard-item">
	        <img src="/resources/images/add.png">
	        <div class="dashboard-text">Add Church Admin</div>
	    </section>
	   </a>

       <a href="/churchadmin/view">
	    <section class="dashboard-item">
            <img src="/resources/images/search.png">
	    	<div class="dashboard-text">View Church Admins</div>
	    </section>
	   </a>
	</section>
</section>