<section id="dialog-box">
  <p>Are you sure you want to carry out this action?</p>
   
  <section id="dialog-links">
    <a class="item-link" id="yes">Yes</a>
    <a class="item-link" id="no">No</a>
  </section>
</section>

<script>
window.addEventListener('load',initialize);

function initialize()
{
  initializeDialogComponents();
}
</script>