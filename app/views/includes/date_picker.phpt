
<select class="date_picker" name="<?php echo $fields[0];?>">
    <option value = "00">Day</option>
    <?php for($i = 1;$i <= 31;$i++) { ?>
    <option value="<?php echo $i;?>" <?php echo $day == $i ? "selected='1'" : ""?>><?php echo $i;?></option>
    <?php } ?>
</select>

<select class="date_picker" name="<?php echo $fields[1];?>">
    <option value="00">Month</option>
    <option value = "01" <?php echo $month == 'Jan' ? "selected='1'" : ""?>>January</option>
    <option value = "02" <?php echo $month == 'Feb' ? "selected='1'" : ""?>>February</option>
    <option value = "03" <?php echo $month == 'Mar' ? "selected='1'" : ""?>>March</option>
    <option value = "04" <?php echo $month == 'Apr' ? "selected='1'" : ""?>>April</option>
    <option value = "05" <?php echo $month == 'May' ? "selected='1'" : ""?>>May</option>
    <option value = "06" <?php echo $month == 'Jun' ? "selected='1'" : ""?>>June</option>
    <option value = "07" <?php echo $month == 'July' ? "selected='1'" : ""?>>July</option>
    <option value = "08" <?php echo $month == 'Aug' ? "selected='1'" : ""?>>August</option>
    <option value = "09" <?php echo $month == 'Sept' ? "selected='1'" : ""?>>September</option>
    <option value = "10" <?php echo $month == 'Oct' ? "selected='1'" : ""?>>October</option>
    <option value = "11" <?php echo $month == 'Nov' ? "selected='1'" : ""?>>November</option>
    <option value = "12" <?php echo $month == 'Dec' ? "selected='1'" : ""?>>December</option>
</select>

<select class="date_picker" name="<?php echo $fields[2];?>">
    <option value="0000">Year</option>
    <?php for($i = 1900;$i <= date('Y');$i++) { ?>
    <option value="<?php echo $i;?>" <?php echo $year == $i ? "selected='1'" : ""?>><?php echo $i;?></option>
    <?php } ?>
</select>