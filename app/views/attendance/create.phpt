<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="attendance-menu" class="sub-menu">
      <ul>
          <li class="selected-tab"><a href="/attendance_records/add_new">Add New Record</a></li>
          <li ><a href="/attendance_records/view">View All Records</a></li>
      </ul>
    </section>
    
    <section id="main-content-body">
        <section class = "item-view-section">
           <h3 class="item-view-section-header">Add Attendance Record</h3>
           <form method="POST" action="/attendance_records/process_add_new">
               <p>
                   <label for="attendance">Attendance:</label>
                   <input type="text" name="attendance">
               </p>

               <p>
                   <label for="date">Event Date:</label>
                   
                 <?php 
                    $day = "0";
                    $month = "0";
                    $year = "0";

                    $fields = ["date_day","date_month","date_year"];
                
                    include('app/views/includes/date_picker.phpt'); 
                  ?>
               </p>

               <p>
                  <label for="time">Time:</label>

                  <?php 

                   $hour = null;
                   $minute = null;
                   $fields = ['date_hour','date_minute'];
                  include('app/views/includes/time_picker.phpt');
                  ?>
                </p>

               <p>
                   <label for="event">Event:</label>
                   <select name="event">
                       <?php foreach($events as $event){ ?>
                       <option value="<?php echo $event->getId();?>"><?php echo $event->getName();?></option>
                       <?php } ?>
                   </select>
               </p>

               <p>
                   <input type="submit" name="record_attendance" value="Add" class="add-button">
               </p>
           </form>
        </section>
    </section>
</section>