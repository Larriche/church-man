<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="main-content-body">
        <section class="member-profile-header">
        <?php $event = Event::find($attendance->getEvent());?>
        <?php if($event != null) { ?>
          <h2 class="item-heading"><?php echo $event->getName(); ?> Attendance Records</h2>
        <?php } ?>
        <a class="button-link" href="/events/attendance/<?php echo $attendance->getEvent();?>">Back to event</a>
        </section>
        
        <section class ="item-view-section">
           <h3 class="item-view-section-header">Edit Attendance</h3>
           <?php require('app/views/includes/messages.phpt');?>
           <form method="POST" action="/attendance_records/process_edit" class='max-form'>

             <p>
             <label for="amount">Attendance:</label>
             <input type="text" name="attendance" value="<?php echo $attendance->getNumber();?>">
             </p>

             <p>
             <label for="date">Date:</label>

             <?php 
                  $date = new PrettyDate($attendance->getDate());
                  $day = $date->getDay();
                  $month = $date->getMonthName();
                  $year = $date->getYear();

                  $fields = ["date_day","date_month","date_year"];
              
                  include('app/views/includes/date_picker.phpt'); 
             ?>
             </p>

             <p>
                  <label for="time">Time:</label>

                  <?php 

                   $hour = $date->getHour();
                   $minute = $date->getMinute();
                   $fields = ['date_hour','date_minute'];
                  include('app/views/includes/time_picker.phpt');
                  ?>
                </p>

             <p>
               <label for="note">Note:</label>
               <textarea name="note"><?php echo $attendance->getNote();?></textarea>
             </p>

             <input type="hidden" name="event_id" value="<?php echo $event->getId();?>">

             <p>
               <input type="submit" name="update_attendance" value="Update">
             </p>
          </form>
        </section>
    </section>
</section>