<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="main-content-body">
        <section class="member-profile-header">
        <a class="button-link" href="/events/attendance/<?php echo $attendance->getEvent();?>">Back to event</a>
        </section>

        <section class = "item-view-section">
           <h3 class="item-view-section-header">View attendance details</h3>

           <table>
             <tr>
               <td>Attendance Count:</td>
               <td><?php echo number_format($attendance->getNumber());?></td>
             </tr>

             <tr>
               <td>Date:</td>
               <td><?php $date = new PrettyDate($attendance->getDate()); echo $date->getReadable();?></td>
             </tr>

             <tr>
               <td>Note:</td>
               <td><?php echo $attendance->getNote();?></td>
             </tr>
           </table>
           
        </section>
    </section>
</section>