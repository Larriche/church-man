<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
    <section id="attendance-menu" class="sub-menu">
      <ul>
          <li><a href="/attendance_records/add_new">Add New Record</a></li>
          <li class="selected-tab"><a href="/attendance/view">View All Records</a></li>
      </ul>
    </section>
    
    <section id="main-content-body">
       <p><?php echo $message; ?></p>
    </section>
</section>