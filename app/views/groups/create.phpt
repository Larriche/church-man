<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
  <section id="groups-menu" class="sub-menu">
      <ul>
          <li class="selected-tab"><a href="/groups/create">Add New Group</a></li>
          <li><a href="/groups/view_all">View Groups</a></li>
      </ul>
  </section>

  <section id="main-content-body">
     <section class="item-view-section">
         <h3 class="item-view-section-header">Create New Church Group</h3>
         
         <?php require('app/views/includes/messages.phpt'); ?>

         <form method="POST" action="/groups/process_create" class = "max-form">
         <p>
             <label for="name">Group Name:</label>
             <input type="text" name="group_name">
         </p>

         <p>
             <label for="description">Group Description:</label>
             <textarea name="description"></textarea>
         </p>

         <p>
             <label>Date Formed:</label>
            
            <?php 
              $day = "0";
              $month = "0";
              $year = "0";

              $fields = ["date_day","date_month","date_year"];
          
              include('app/views/includes/date_picker.phpt'); 
            ?>
          </p>

          <p>
             <label>Privacy:</label>
             <select name="privacy">
                 <option value="PRIVATE">Private Group ( eg.a committee )</option>
                 <option value="PUBLIC">Open to All</option>
             </select>
          </p>



         <p>
             <input type="submit" value="Create" name="create_group" class="group-create-button">
         </p>
         </form>
     </section>
  </section>
</section>