<?php 
require('app/views/members/menu.phpt');
?>

<section id="main-content-container">
  <section id="main-content-body">
     <section class="item-view-section">
         <h3 class="item-view-section-header">Church Groups</h3>
           <section class="item-view-section">
             <h3 class="item-view-section-header">Your Groups</h3>
             <?php if(count($memberGroups) > 0) { ?>
               <table>
                  <colgroup>
                    <col style="width: 10px;"/>
                    <col style="width: 300px;"/>
        	          <col style="width: 100px;"/>
        	          <col style="width: 150px;"/>
        	          <col style="width: 100px;"/>
                    <col class = "view"/>
        	        </colgroup>
        	        <tr>
        	            <th></th>
        	            <th>Name</th>
        	            <th>Members</th>
        	            <th>Date Created</th>
        	            <th>Privacy</th>
                      <th></th>
        	        </tr>

                   <?php $count = 1; foreach($memberGroups as $group) {?>
                   	<tr>
                   	   <td><?php echo $count++;?></td>
                       <td><?php echo $group->getName();?></td>
                       <td><?php echo count($group->getMembers());?></td>
                       <td><?php $date = new PrettyDate($group->getDateCreated()); echo $date->getReadable();?></td>
                       <td><?php echo capitalise(($group->getType()));?></td>
                       <td><a class="item-link" href="/groups/view/<?php echo $group->getId();?>">View</a></td>
                   	</tr>
                   <?php } ?>
               </table>
            <?php } else { ?>
              <p>You are not in any groups</p>
            <?php } ?>
          </section>

          <section class="item-view-section">
            <h3 class="item-view-section-header">Other Groups</h3>
            <?php if(count($activeGroups) > 0){ ?>
              <table>
                  <colgroup>
                    <col style="width: 10px;"/>
                    <col style="width: 300px;"/>
                    <col style="width: 100px;"/>
                    <col style="width: 100px;"/>
                    <col style="width: 150px;"/>
                    <col style="width: 100px;"/>
                    <col class = "view"/>
                  </colgroup>
                  <tr>
                      <th></th>
                      <th>Name</th>
                      <th>Members</th>
                      <th>Date Created</th>
                      <th>Date Dissolved</th>
                      <th>Privacy</th>
                      <th></th>
                  </tr>

                   <?php $count = 1; foreach($activeGroups as $group) {?>
                    <tr>
                       <td><?php echo $count++;?></td>
                       <td><?php echo $group->getName();?></td>
                       <td><?php echo count($group->getMembers());?></td>
                       <td><?php $date = new PrettyDate($group->getDateCreated()); echo $date->getReadable();?></td>
                       <td><?php $date = new PrettyDate($group->getDateDissolved()); echo $date->getReadable();?></td>
                       <td><?php echo capitalise(($group->getType()));?></td>
                       <td><a class="item-link" href="/groups/view/<?php echo $group->getId();?>">View</a></td>
                    </tr>
                   <?php } ?>
               </table>
            <?php } else {?>
              <p>No groups created yet</p>
            <?php } ?>
          </section>
  </section>
</section>