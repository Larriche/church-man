<?php require('app/views/churchadmin/menu.phpt'); ?>

<section id="main-content-container">
  <section id="groups-menu" class="sub-menu">
      <ul>
          <li><a href="/groups/create">Add New Group</a></li>
          <li class="selected-tab"><a href="/groups/view_all">View Groups</a></li>
      </ul>
  </section>

  <section id="main-content-body">
     <section class="member-profile-header">
        <h2 class="item-heading"><?php echo $group->getName();?></h2>
        <ul>
            <li><a href="/groups/view_all" class="button-link">Back</a></li>
            <?php if(churchAdminLoggedIn()) { ?>
            <li><a href="/groups/edit/<?php echo $group->getId();?>" class="button-link">Edit Group</a></li>

            <?php if($group->getStatus() == 'ACTIVE') { ?>
            <li><a href="/groups/dissolve/<?php echo $group->getId();?>" class="button-link">Dissolve Group</a>
            <?php }else {?>
            <li><a href="/groups/activate/<?php echo $group->getId();?>" class="button-link">Make Active</a>
            <?php } ?>

            <li><a href="/groups/delete/<?php echo $group->getId();?>" class="danger">Delete Group</a></li>
            <?php } ?>

            <?php if($group->searchMember($_SESSION['user_id'])) { ?>
            <li><a class="button-link">Message Board</a></li>
            <?php } ?>

            <?php if(memberLoggedIn() && !$group->searchMember($_SESSION['user_id'])) { 
              if(!$group->hasRequestFrom($_SESSION['user_id'])) {?>
            <li><a href="/groups/join_requests/<?php echo $group->getId();?>" class="button-link">Join Group</a></li>
            <?php } else { ?>
              <span class="long-term-notif">Membership Request Sent</span>
            <?php } } ?>

            
            <?php if($group->searchMember($_SESSION['user_id'])){
              if(GroupMember::find($_SESSION['user_id'],$group->getId())->getRole() == 'LEADER'){?>
                 <li><a href="/groups/requests/<?php echo $group->getId();?>" class="button-link">Join Requests</a></li>
            <?php
              }
            }
            ?>
        </ul>
      </section>
      
     <section class="item-view-section">
         <h3 class="item-view-section-header">Edit Church Group</h3>
         
         <?php require('app/views/includes/messages.phpt'); ?>

         <form method="POST" action="/groups/process_edit" class = "max-form">
         <p>
             <label for="name">Group Name:</label>
             <input type="text" name="group_name" value="<?php echo $group->getName();?>">
         </p>

         <p>
             <label for="description">Group Description:</label>
             <textarea name="description"><?php echo $group->getDescription();?></textarea>
         </p>

         <p>
             <label>Date Formed:</label>
            
            <?php 
              $date = new PrettyDate($group->getDateCreated());
              $day = $date->getDay();
              $month = $date->getMonthName();
              $year = $date->getYear();

              $fields = ["date_day","date_month","date_year"];
          
              include('app/views/includes/date_picker.phpt'); 
            ?>
          </p>

          <p>
             <label>Privacy:</label>
             <select name="privacy">
                 <option value="PRIVATE" <?php if($group->getType() == 'PRIVATE') echo 'selected="1"';?>>
                   Private Group ( eg.a committee )
                 </option>
                 <option value="PUBLIC" <?php if($group->getType() == 'PUBLIC') echo 'selected="1"';?>>
                   Open to All
                 </option>
             </select>
          </p>
          
          <input type="hidden" name="group_id" value="<?php echo $group->getId();?>">

         <p>
             <input type="submit" value="Update" name="edit_group">
         </p>
         </form>
     </section>
  </section>
</section>