<?php 
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt'); 
}
else if(memberLoggedIn()){
    require('app/views/members/menu.phpt');
}
else{ // pastors
    require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
  <section id="groups-menu" class="sub-menu">
      <ul>
          <li><a href="/groups/create">Add New Group</a></li>
          <li class="selected-tab"><a href="/groups/view_all">View Groups</a></li>
      </ul>
  </section>

  <section id="main-content-body">
     <section class="item-view-section">
         <h3 class="item-view-section-header">Church Groups</h3>
           <section class="item-view-section">
             <h3 class="item-view-section-header">Active Groups</h3>
             <?php if(count($activeGroups) > 0) { ?>
               <table>
                  <colgroup>
                    <col style="width: 10px;"/>
                    <col style="width: 300px;"/>
        	          <col style="width: 100px;"/>
        	          <col style="width: 150px;"/>
        	          <col style="width: 100px;"/>
                    <col class = "view"/>
        	        </colgroup>
        	        <tr>
        	            <th></th>
        	            <th>Name</th>
        	            <th>Members</th>
        	            <th>Date Created</th>
        	            <th>Privacy</th>
                      <th></th>
        	        </tr>

                   <?php $count = 1; foreach($activeGroups as $group) {?>
                   	<tr>
                   	   <td><?php echo $count++;?></td>
                       <td><?php echo $group->getName();?></td>
                       <td><?php echo count($group->getMembers());?></td>
                       <td><?php $date = new PrettyDate($group->getDateCreated()); echo $date->getReadable();?></td>
                       <td><?php echo capitalise(($group->getType()));?></td>
                       <td><a class="item-link" href="/groups/view/<?php echo $group->getId();?>">View</a></td>
                   	</tr>
                   <?php } ?>
               </table>
            <?php } else { ?>
              <p>No active church groups</p>
            <?php } ?>
          </section>

          <section class="item-view-section">
            <h3 class="item-view-section-header">Dissolved Groups</h3>
            <?php if(count($inactiveGroups) > 0){ ?>
              <table>
                  <colgroup>
                    <col style="width: 10px;"/>
                    <col style="width: 300px;"/>
                    <col style="width: 100px;"/>
                    <col style="width: 100px;"/>
                    <col style="width: 150px;"/>
                    <col style="width: 100px;"/>
                    <col class = "view"/>
                  </colgroup>
                  <tr>
                      <th></th>
                      <th>Name</th>
                      <th>Members</th>
                      <th>Date Created</th>
                      <th>Date Dissolved</th>
                      <th>Privacy</th>
                      <th></th>
                  </tr>

                   <?php $count = 1; foreach($inactiveGroups as $group) {?>
                    <tr>
                       <td><?php echo $count++;?></td>
                       <td><?php echo $group->getName();?></td>
                       <td><?php echo count($group->getMembers());?></td>
                       <td><?php $date = new PrettyDate($group->getDateCreated()); echo $date->getReadable();?></td>
                       <td><?php $date = new PrettyDate($group->getDateDissolved()); echo $date->getReadable();?></td>
                       <td><?php echo capitalise(($group->getType()));?></td>
                       <td><a class="item-link" href="/groups/view/<?php echo $group->getId();?>">View</a></td>
                    </tr>
                   <?php } ?>
               </table>
            <?php } else {?>
              <p>No dissolved groups</p>
            <?php } ?>
          </section>
  </section>
</section>