<?php 
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt'); 
}
elseif(memberLoggedIn()){
	require('app/views/members/menu.phpt');
}
else{
	require('app/views/pastors/menu.phpt');
}
?>

<section id="main-content-container">
    <?php if(churchAdminLoggedIn()) { ?>
	<section id="groups-menu" class="sub-menu">
	    <ul>
	        <li><a href="/groups/create">Add New Group</a></li>
	        <li class="selected-tab"><a href="/groups/view_all">View Groups</a></li>
	    </ul>
    </section>
    <?php } ?>
    
	<section id="main-content-body">
	   <section class="member-profile-header">
        <h2 class="item-heading"><?php echo $group->getName();?></h2>
        <ul>
            <li><a href="/groups/view_all" class="button-link">Back</a></li>
            <?php if(churchAdminLoggedIn()) { ?>
            <li><a href="/groups/edit/<?php echo $group->getId();?>" class="button-link">Edit Group</a></li>

            <?php if($group->getStatus() == 'ACTIVE') { ?>
            <li><a href="/groups/dissolve/<?php echo $group->getId();?>" class="button-link">Dissolve Group</a>
            <?php }else {?>
            <li><a href="/groups/activate/<?php echo $group->getId();?>" class="button-link">Make Active</a>
            <?php } ?>

            <li><a href="/groups/delete/<?php echo $group->getId();?>" class="danger">Delete Group</a></li>
            <?php } ?>

            <?php if($group->searchMember($_SESSION['user_id'])) { ?>
            <li><a class="button-link">Message Board</a></li>
            <?php } ?>

            <?php if(memberLoggedIn() && !$group->searchMember($_SESSION['user_id'])) { 
            	if(!$group->hasRequestFrom($_SESSION['user_id'])) {?>
            <li><a href="/groups/join_requests/<?php echo $group->getId();?>" class="button-link">Join Group</a></li>
            <?php } else { ?>
            	<span class="long-term-notif">Membership Request Sent</span>
            <?php } } ?>

            
            <?php if($group->searchMember($_SESSION['user_id'])){
            	if(GroupMember::find($_SESSION['user_id'],$group->getId())->getRole() == 'LEADER'){?>
                 <li><a href="/groups/requests/<?php echo $group->getId();?>" class="button-link">Join Requests</a></li>
            <?php
            	}
            }
            ?>
        </ul>
      </section>

	   <?php require('app/views/includes/messages.phpt');?>
	   <section id="groups-view">
	       <section class="item-view-section">
		       <h3 class="item-view-section-header">Members</h3>
		       <table>
		           <colgroup>
		            <col class = "counter"/>
		            <col class = "image"/>
		            <col class = "name"/>
			        <col class = "date"/>
			        <col class = "role"/>
			        </colgroup>
			        

		           <?php $count = 1; foreach($members as $member) {?>
		           	<tr>
		           	   <td><?php echo $count++;?></td>
		           	   <td><img src='<?php echo UserProfile::find($member->getUserId())->getImageUrl();?>' height="60" width="60"></td>
		               <td><?php echo UserProfile::find($member->getUserId())->getFullname();?></td>
		               <td>member since <?php echo $member->getDateJoined();?></td>
		               <td><?php echo $member->getRole();?></td>
		           	</tr>
		           <?php } ?>
		       </table>
		   </section>

		   <section class="item-view-section">
		       <h2 class="item-view-section-header">Date Created</h2>
		       <p><?php $date = new PrettyDate($group->getDateCreated()); echo $date->getReadable();?></p>
		   </section>

		   <?php if($group->getStatus() == 'CLOSED'){ ?>
		   	<section class="item-view-section">
		   	  <h2 class="item-view-section-header">Date Dissolved</h2>
		   	  <p><?php $date = new PrettyDate($group->getDateDissolved()); echo $date->getReadable();?></p>
		   	</section>
		   <?php } ?>

		   <section class="item-view-section">
		       <h2 class="item-view-section-header">Description</h2>
		       <p><?php echo $group->getDescription();?></p>
		   </section>

           <?php if( (churchAdminLoggedIn() || 
           	   ( memberLoggedIn() && in_array( $_SESSION['user_id'] , $group->getLeaders() ))) && $group->getStatus() == 'ACTIVE'){ ?>
		   <section class="item-view-section">
		       <h2 class="item-view-section-header">Add Member</h2>
		       <form method="POST" action="/groups/add_member" class="max-form">
		           <p>
		               <label for="name">Member Name:</label>
		               <input type="text" name="name" id="new-member-name">
		           </p>

		           <p>
		              <label for="date_joined">Date Joined</label>
		              <?php 
	                    $day = "0";
	                    $month = "0";
	                    $year = "0";

	                    $fields = ["joined_day","joined_month","joined_year"];
	                
	                    include('app/views/includes/date_picker.phpt'); 
	                  ?>
		           </p>

		           <p>
		              <label for="role">Member Position</label>
		              <select name="role">
		              	<option value="MEMBER">Member</option>
		              	<option value="LEADER">Leader</option>
		              	<option value="SECRETARY">Secretary</option>
		              	<option value="TREASURER">Treasurer</option>
		              </select>

		           <input type="hidden" name="user_id">
		           <input type="hidden" name='group_id' value="<?php echo $group->getId();?>">
		           <p>
                        <input class="member-add-button" type="submit" name="add_group_member" value="Add">
		           </p>
		       </form>		       
		   </section>
		   <?php } ?>
	   </section>
	</section>

	<?php 
	   $dialogMessage = 'Are you sure you want to carry out this action?';
	   require('app/views/includes/dialog_box.phpt'); 
	?>
</section>

<script>
window.addEventListener('load',initialize);

function initialize()
{
	initializeDialogComponents();
	initializeAutocomplete();
}

function initializeAutocomplete()
{
	$(document).ready(function() {
		// load similar member names for autocomplete
	    $('#new-member-name').autocomplete({
	      source: function(request,response){
	      	$.getJSON("/ajax/members",{ name : $('#new-member-name').val()},response);
	      }
	    });

	});
}
</script>