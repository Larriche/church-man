<?php 
if(churchAdminLoggedIn()){
    require('app/views/churchadmin/menu.phpt'); 
}
elseif(memberLoggedIn()){
	require('app/views/members/menu.phpt');
}
?>

<section id="main-content-container">
  <section id="main-content-body">
    <section class="member-profile-header">
        <h2 class="item-heading"><?php echo $group->getName();?></h2>
        <ul>
          <li><a href="/groups/view/<?php echo $group->getId();?>" class="button-link">Back</a></li>
          <li><a class="button-link">Message Board</a></li>
          <li><a href="/groups/join_requests/<?php echo $group->getId();?>" class="button-link">Join Requests</a></li>
        </ul>
    </section>

    <section class="item-view-section">
      <h3 class="item-view-section-header">Membership Requests</h3>
      <?php if(count($group->getRequests())  > 0) { ?>
      <table>
        <colgroup>
          <col class="field"/>
          <col class="value"/>
        </colgroup>
        <?php foreach($group->getRequests() as $request) {?>
        	<tr>
        	  <td><?php echo UserProfile::find($request->getUserId())->getFullname(); ?></td>
        	  <td><a href="/groups/approve_request/<?php echo $request->getId();?>" class="item-link">Approve</a></td>
        	</tr>
        <?php } ?>
      </table>
      <?php } else {?>
      	<p>No membership requests</p>
      <?php } ?>
    </section>
  </section>
</section>