<?php
$dbhost  = 'localhost';    
$dbname  = 'church';   
$dbuser  = 'root';  
$dbpass  = '';   

$connection = new mysqli($dbhost, $dbuser, $dbpass, $dbname);

// a quick function cooked up to reduce the amount of typing
// required to create the tables
function queryMysql($query)
{
    global $connection;
    $result = $connection->query($query);
    if (!$result) 
    	echo $connection->error.'<br />';
    else 
    	echo "Query ran Ok"."<br />";

    return $result;
}

queryMysql(
	"CREATE TABLE users
	(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	username VARCHAR(100),
	password VARCHAR(255),
	role enum('MEMBER','SYS_ADMIN','CHURCH_ADMIN','PASTOR'),
	status enum('ACTIVE','INACTIVE'),
	date_last_edited DATETIME
	);"
);

queryMysql(
	"CREATE TABLE user_profiles(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	user_id INT UNSIGNED,
	firstname VARCHAR(255),
	othernames VARCHAR(255),
	lastname VARCHAR(255),
	user_title VARCHAR(10),
	date_of_birth DATE,
	date_joined DATE,
	date_departed DATE,
	gender enum('MALE','FEMALE','NOT SET YET'),
	marital_status enum('SINGLE','MARRIED','NOT SET YET'),
	spouse_name VARCHAR(255),
	occupation VARCHAR(255),
	position_in_church VARCHAR(255),
	home_phone VARCHAR(30),
	work_phone VARCHAR(30),
	cell_phone VARCHAR(30),
	emergency_contact VARCHAR(30), 
	date_last_edited DATE,
	email VARCHAR(50),
	address VARCHAR(255),
	city VARCHAR(100),
	image_url VARCHAR(100),
	FOREIGN KEY (user_id)REFERENCES users(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE groups
	(
	id INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	group_name VARCHAR(255),
	description TEXT,
	date_created DATE,
	date_dissolved DATE,
	status enum('ACTIVE','CLOSED'),
	type enum('PRIVATE','PUBLIC')
	)ENGINE=InnoDB;"
);


queryMysql(
	"CREATE TABLE group_members
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	member_id INT UNSIGNED,
	member_role enum('LEADER','MEMBER','SECRETARY','TREASURER'),
	group_id INT UNSIGNED,
	joined_date DATE,
	FOREIGN KEY(member_id) REFERENCES users(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE,
	FOREIGN KEY(group_id) REFERENCES groups(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE families
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	family_name VARCHAR(255)
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE family_members
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	user_id INT UNSIGNED,
	family_id INT UNSIGNED,
	FOREIGN KEY(user_id) REFERENCES users(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE,
	FOREIGN KEY(family_id) REFERENCES families(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE
	)ENGINE=InnoDB;"
);

queryMysql(
"CREATE TABLE couples(
id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
husband_id INT UNSIGNED,
wife_id INT UNSIGNED,
FOREIGN KEY(husband_id) REFERENCES users(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
FOREIGN KEY(wife_id) REFERENCES users(id)
    ON DELETE CASCADE
    ON UPDATE CASCADE
)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE children
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	child_id INT UNSIGNED,
	father_id INT UNSIGNED,
	mother_id INT UNSIGNED
	)ENGINE=InnoDB;
	"
);

queryMysql(
	"CREATE TABLE projects
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	project_name VARCHAR(255),
	date_commenced DATE,
	date_completed DATE,
	description TEXT,
	estimated_cost FLOAT,
	amount_raised FLOAT,
	status enum('UNCOMPLETED','COMPLETED','ABANDONED')
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE pledges
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	member_id INT UNSIGNED,
	giver_name VARCHAR(255),
	giver_contact VARCHAR(255),
	date_made DATE,
	amount FLOAT,
	amount_paid FLOAT,
	project_id INT UNSIGNED
	)ENGINE=InnoDB;
	"
	// pledges can be made by people who are not church members
	// so we omit foreign relationship between pledges and users
	// for pledgers who are not members,we capture their names
	// and contacts
);

queryMysql(
	"CREATE TABLE tithes
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	payer_id INT UNSIGNED,
	amount FLOAT,
	date_paid DATE
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE events(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	event_name VARCHAR(255),
	description TEXT,
	date_of_commencement DATE,
	date_ended DATE,
	event_type enum('CHURCH','GROUP','INDIVIDUALS'),
	income FLOAT,
	expenses FLOAT
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE attendance
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	date_taken DATETIME,
	number INT UNSIGNED,
	note TEXT,
	event_id INT UNSIGNED,
	FOREIGN KEY(event_id) REFERENCES events(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE offerings
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	date_paid DATE,
	amount FLOAT
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE groups_to_events
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	group_id INT UNSIGNED,
	event_id INT UNSIGNED,
	FOREIGN KEY(group_id) REFERENCES groups(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE,
	FOREIGN KEY(event_id) REFERENCES events(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE members_to_events
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	event_id INT UNSIGNED,
	member_id INT UNSIGNED,
	FOREIGN KEY(event_id) REFERENCES events(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE,
	FOREIGN KEY(member_id) REFERENCES users(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE event_finances
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	event_id INT UNSIGNED,
	amount FLOAT,
	type enum('INCOME','EXPENSE'),
	date_recorded DATE,
	description TEXT,
	FOREIGN KEY(event_id) REFERENCES events(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE project_finances
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	project_id INT UNSIGNED,
	amount FLOAT,
	type enum('INCOME','EXPENSE'),
	date_recorded DATE,
	description TEXT,
	FOREIGN KEY(project_id) REFERENCES projects(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE announcements
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	title VARCHAR(255),
	date_made DATE,
	message TEXT,
	expiry_date DATE
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE prayer_requests
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	member_id INT UNSIGNED,
	description TEXT,
	date_made DATE,
	status enum('ACTIVE','INACTIVE'),
	FOREIGN KEY(member_id) REFERENCES users(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE
	)ENGINE=InnoDB;
	"
);

queryMysql(
	"CREATE TABLE pledge_payments
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	pledge_id INT UNSIGNED,
	amount FLOAT,
	date_paid DATE,
	FOREIGN KEY(pledge_id) REFERENCES pledges(id)
	    ON DELETE CASCADE
	    ON UPDATE CASCADE
	)ENGINE = InnoDB;
	"
);

queryMysql(
	// other church finances
	"CREATE TABLE church_finances
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	amount FLOAT,
	type enum('INCOME','EXPENSE'),
	date_recorded DATE,
	description TEXT
	)ENGINE=InnoDB;"
);

queryMysql(
	"CREATE TABLE group_join_requests
	(
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	group_id INT UNSIGNED,
	user_id INT UNSIGNED,
	FOREIGN KEY (group_id) REFERENCES groups(id)
	  ON DELETE CASCADE
	  ON UPDATE CASCADE,
	FOREIGN KEY (user_id) REFERENCES users(id)
	  ON DELETE CASCADE
	  ON UPDATE CASCADE
	)ENGINE=InnoDB;"
);


// setting up default credentials for the system admin
$username = "admin";
$password = password_hash("admin",PASSWORD_DEFAULT);
$role = 'SYS_ADMIN';
$date = date("Y-m-d");
$result = $connection->query("INSERT INTO users(username,password,role,date_last_edited,status)
 	                      VALUES('$username','$password','$role','$date','ACTIVE')");

if(!$result)
	echo $connection->error;
else
	echo "Done";
?>